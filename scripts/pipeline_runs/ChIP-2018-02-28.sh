#!/bin/bash
# Exit immediately on error
set -eu -o pipefail

#-------------------------------------------------------------------------------
# ChipSeq PBSScheduler Job Submission Bash script
# Version: 2.2.0
# Created on: 2018-02-28T15:43:08
# Steps:
#   picard_sam_to_fastq: 0 job... skipping
#   trimmomatic: 0 job... skipping
#   merge_trimmomatic_stats: 0 job... skipping
#   bwa_mem_picard_sort_sam: 0 job... skipping
#   samtools_view_filter: 2 jobs
#   picard_merge_sam_files: 1 job
#   picard_mark_duplicates: 2 jobs
#   metrics: 2 jobs
#   homer_make_tag_directory: 30 jobs
#   qc_metrics: 1 job
#   homer_make_ucsc_file: 31 jobs
#   macs2_callpeak: 6 jobs
#   TOTAL: 75 jobs
#-------------------------------------------------------------------------------

OUTPUT_DIR=/gs/scratch/efournier/A549_Michele/output/chip-pipeline-GRCh38
JOB_OUTPUT_DIR=$OUTPUT_DIR/job_output
TIMESTAMP=`date +%FT%H.%M.%S`
JOB_LIST=$JOB_OUTPUT_DIR/ChipSeq_job_list_$TIMESTAMP
mkdir -p $OUTPUT_DIR
cd $OUTPUT_DIR


#-------------------------------------------------------------------------------
# STEP: samtools_view_filter
#-------------------------------------------------------------------------------
STEP=samtools_view_filter
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_1_JOB_ID: samtools_view_filter.HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1
JOB_DEPENDENCIES=
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1.622b22fa2d6019a30fa670ceea179314.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1.622b22fa2d6019a30fa670ceea179314.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -b -F4 -q 20 \
  alignment/WCE_shNIPBL-3_EtOH_Rep1/HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1/HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1.sorted.bam \
  > alignment/WCE_shNIPBL-3_EtOH_Rep1/HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1/HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1.sorted.filtered.bam
samtools_view_filter.HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1.622b22fa2d6019a30fa670ceea179314.mugqic.done
)
samtools_view_filter_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$samtools_view_filter_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_2_JOB_ID: samtools_view_filter_report
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter_report
JOB_DEPENDENCIES=$samtools_view_filter_1_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter_report.b5cac64c2a4ab579900138ddfbed49a6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter_report.b5cac64c2a4ab579900138ddfbed49a6.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p report && \
pandoc --to=markdown \
  --template /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.samtools_view_filter.md \
  --variable min_mapq="20" \
  /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.samtools_view_filter.md \
  > report/ChipSeq.samtools_view_filter.md
samtools_view_filter_report.b5cac64c2a4ab579900138ddfbed49a6.mugqic.done
)
samtools_view_filter_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$samtools_view_filter_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: picard_merge_sam_files
#-------------------------------------------------------------------------------
STEP=picard_merge_sam_files
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_1_JOB_ID: symlink_readset_sample_bam.WCE_shNIPBL-3_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.WCE_shNIPBL-3_EtOH_Rep1
JOB_DEPENDENCIES=$samtools_view_filter_1_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.WCE_shNIPBL-3_EtOH_Rep1.b6e6416c5715b70a16e3f524a86d5dc1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.WCE_shNIPBL-3_EtOH_Rep1.b6e6416c5715b70a16e3f524a86d5dc1.mugqic.done'
mkdir -p alignment/WCE_shNIPBL-3_EtOH_Rep1 && \
ln -s -f HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1/HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1.sorted.filtered.bam alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.merged.bam
symlink_readset_sample_bam.WCE_shNIPBL-3_EtOH_Rep1.b6e6416c5715b70a16e3f524a86d5dc1.mugqic.done
)
picard_merge_sam_files_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_merge_sam_files_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: picard_mark_duplicates
#-------------------------------------------------------------------------------
STEP=picard_mark_duplicates
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_1_JOB_ID: picard_mark_duplicates.WCE_shNIPBL-3_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.WCE_shNIPBL-3_EtOH_Rep1
JOB_DEPENDENCIES=$picard_merge_sam_files_1_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.WCE_shNIPBL-3_EtOH_Rep1.6bcd0686f9bf1bc436a4c81aece7ea7f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.WCE_shNIPBL-3_EtOH_Rep1.6bcd0686f9bf1bc436a4c81aece7ea7f.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/localscratch/ -XX:ParallelGCThreads=1 -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/localscratch/ \
  INPUT=alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.merged.bam \
  OUTPUT=alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  METRICS_FILE=alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.metrics \
  MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.WCE_shNIPBL-3_EtOH_Rep1.6bcd0686f9bf1bc436a4c81aece7ea7f.mugqic.done
)
picard_mark_duplicates_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=2 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_2_JOB_ID: picard_mark_duplicates_report
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates_report
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates_report.c80ab57aaab7999422d809ae0583b4b5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates_report.c80ab57aaab7999422d809ae0583b4b5.mugqic.done'
mkdir -p report && \
cp \
  /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.picard_mark_duplicates.md \
  report/ChipSeq.picard_mark_duplicates.md
picard_mark_duplicates_report.c80ab57aaab7999422d809ae0583b4b5.mugqic.done
)
picard_mark_duplicates_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: metrics
#-------------------------------------------------------------------------------
STEP=metrics
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: metrics_1_JOB_ID: metrics.flagstat
#-------------------------------------------------------------------------------
JOB_NAME=metrics.flagstat
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/metrics/metrics.flagstat.96355fdef215f074305d48d156d6efa0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'metrics.flagstat.96355fdef215f074305d48d156d6efa0.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools flagstat \
  alignment/POL2_shCRTL-1_Dex_Rep1/POL2_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  > alignment/POL2_shCRTL-1_Dex_Rep1/POL2_shCRTL-1_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2_shCTRL-2_Dex_Rep1/POL2_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  > alignment/POL2_shCTRL-2_Dex_Rep1/POL2_shCTRL-2_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/WCE_shCTRL-2_EtOH_Rep1/WCE_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  > alignment/WCE_shCTRL-2_EtOH_Rep1/WCE_shCTRL-2_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2_shNIPBL-3_EtOH_Rep1/POL2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  > alignment/POL2_shNIPBL-3_EtOH_Rep1/POL2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  > alignment/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2_shNIPBL-5_EtOH_Rep1/POL2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  > alignment/POL2_shNIPBL-5_EtOH_Rep1/POL2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2_shCTRL-2_EtOH_Rep1/POL2_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  > alignment/POL2_shCTRL-2_EtOH_Rep1/POL2_shCTRL-2_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/WCE_shCRTL-1_Dex_Rep1/WCE_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  > alignment/WCE_shCRTL-1_Dex_Rep1/WCE_shCRTL-1_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2_shNIPBL-3_Dex_Rep1/POL2_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  > alignment/POL2_shNIPBL-3_Dex_Rep1/POL2_shNIPBL-3_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2_shNIPBL-5_Dex_Rep1/POL2_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  > alignment/POL2_shNIPBL-5_Dex_Rep1/POL2_shNIPBL-5_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/WCE_shCTRL-1_EtOH_Rep1/WCE_shCTRL-1_EtOH_Rep1.sorted.dup.bam \
  > alignment/WCE_shCTRL-1_EtOH_Rep1/WCE_shCTRL-1_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  > alignment/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  > alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2_shCRTL-1_EtOH_Rep1/POL2_shCRTL-1_EtOH_Rep1.sorted.dup.bam \
  > alignment/POL2_shCRTL-1_EtOH_Rep1/POL2_shCRTL-1_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/WCE_shNIPBL-5_Dex_Rep1/WCE_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  > alignment/WCE_shNIPBL-5_Dex_Rep1/WCE_shNIPBL-5_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/WCE_shNIPBL-3_Dex_Rep1/WCE_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  > alignment/WCE_shNIPBL-3_Dex_Rep1/WCE_shNIPBL-3_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2-ser2_shNIPBL-3_EtOH_Rep1/POL2-ser2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  > alignment/POL2-ser2_shNIPBL-3_EtOH_Rep1/POL2-ser2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/BRD4_NA_EtOH_Rep2/BRD4_NA_EtOH_Rep2.sorted.dup.bam \
  > alignment/BRD4_NA_EtOH_Rep2/BRD4_NA_EtOH_Rep2.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/WCE_NA_Dex_Rep2/WCE_NA_Dex_Rep2.sorted.dup.bam \
  > alignment/WCE_NA_Dex_Rep2/WCE_NA_Dex_Rep2.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/CDK9_NA_Dex_Rep2/CDK9_NA_Dex_Rep2.sorted.dup.bam \
  > alignment/CDK9_NA_Dex_Rep2/CDK9_NA_Dex_Rep2.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2-ser2_shNIPBL-5_EtOH_Rep1/POL2-ser2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  > alignment/POL2-ser2_shNIPBL-5_EtOH_Rep1/POL2-ser2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2-ser2_shCTRL-2_Dex_Rep1/POL2-ser2_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  > alignment/POL2-ser2_shCTRL-2_Dex_Rep1/POL2-ser2_shCTRL-2_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2-ser2_shNIPBL-3_Dex_Rep1/POL2-ser2_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  > alignment/POL2-ser2_shNIPBL-3_Dex_Rep1/POL2-ser2_shNIPBL-3_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2-ser2_shCRTL-1_Dex_Rep1/POL2-ser2_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  > alignment/POL2-ser2_shCRTL-1_Dex_Rep1/POL2-ser2_shCRTL-1_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2-ser2_shNIPBL-5_Dex_Rep1/POL2-ser2_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  > alignment/POL2-ser2_shNIPBL-5_Dex_Rep1/POL2-ser2_shNIPBL-5_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/WCE_NA_EtOH_Rep2/WCE_NA_EtOH_Rep2.sorted.dup.bam \
  > alignment/WCE_NA_EtOH_Rep2/WCE_NA_EtOH_Rep2.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2-ser2_shCRTL-1_EtOH_Rep1/POL2-ser2_shCRTL-1_EtOH_Rep1.sorted.dup.bam \
  > alignment/POL2-ser2_shCRTL-1_EtOH_Rep1/POL2-ser2_shCRTL-1_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2-ser2_shCTRL-2_EtOH_Rep1/POL2-ser2_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  > alignment/POL2-ser2_shCTRL-2_EtOH_Rep1/POL2-ser2_shCTRL-2_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/CDK9_NA_EtOH_Rep2/CDK9_NA_EtOH_Rep2.sorted.dup.bam \
  > alignment/CDK9_NA_EtOH_Rep2/CDK9_NA_EtOH_Rep2.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/BRD4_NA_Dex_Rep2/BRD4_NA_Dex_Rep2.sorted.dup.bam \
  > alignment/BRD4_NA_Dex_Rep2/BRD4_NA_Dex_Rep2.sorted.dup.bam.flagstat
metrics.flagstat.96355fdef215f074305d48d156d6efa0.mugqic.done
)
metrics_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$metrics_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: metrics_2_JOB_ID: metrics_report
#-------------------------------------------------------------------------------
JOB_NAME=metrics_report
JOB_DEPENDENCIES=$metrics_1_JOB_ID
JOB_DONE=job_output/metrics/metrics_report.505a0d8303c0b2fa1256b6cfe93c7f8a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'metrics_report.505a0d8303c0b2fa1256b6cfe93c7f8a.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
for sample in POL2_shCRTL-1_Dex_Rep1 POL2_shCTRL-2_Dex_Rep1 WCE_shCTRL-2_EtOH_Rep1 POL2_shNIPBL-3_EtOH_Rep1 WCE_shNIPBL-5_EtOH_Rep1 POL2_shNIPBL-5_EtOH_Rep1 POL2_shCTRL-2_EtOH_Rep1 WCE_shCRTL-1_Dex_Rep1 POL2_shNIPBL-3_Dex_Rep1 POL2_shNIPBL-5_Dex_Rep1 WCE_shCTRL-1_EtOH_Rep1 WCE_shCTRL-2_Dex_Rep1 WCE_shNIPBL-3_EtOH_Rep1 POL2_shCRTL-1_EtOH_Rep1 WCE_shNIPBL-5_Dex_Rep1 WCE_shNIPBL-3_Dex_Rep1 POL2-ser2_shNIPBL-3_EtOH_Rep1 BRD4_NA_EtOH_Rep2 WCE_NA_Dex_Rep2 CDK9_NA_Dex_Rep2 POL2-ser2_shNIPBL-5_EtOH_Rep1 POL2-ser2_shCTRL-2_Dex_Rep1 POL2-ser2_shNIPBL-3_Dex_Rep1 POL2-ser2_shCRTL-1_Dex_Rep1 POL2-ser2_shNIPBL-5_Dex_Rep1 WCE_NA_EtOH_Rep2 POL2-ser2_shCRTL-1_EtOH_Rep1 POL2-ser2_shCTRL-2_EtOH_Rep1 CDK9_NA_EtOH_Rep2 BRD4_NA_Dex_Rep2
do
  flagstat_file=alignment/$sample/$sample.sorted.dup.bam.flagstat
  echo -e "$sample	`grep -P '^\d+ \+ \d+ mapped' $flagstat_file | grep -Po '^\d+'`	`grep -P '^\d+ \+ \d+ duplicate' $flagstat_file | grep -Po '^\d+'`"
done | \
awk -F"	" '{OFS="	"; print $0, $3 / $2 * 100}' | sed '1iSample	Aligned Filtered Reads	Duplicate Reads	Duplicate %' \
  > metrics/SampleMetrics.stats && \
mkdir -p report && \
if [[ -f metrics/trimSampleTable.tsv ]]
then
  awk -F "	" 'FNR==NR{trim_line[$1]=$0; surviving[$1]=$3; next}{OFS="	"; if ($1=="Sample") {print trim_line[$1], $2, "Aligned Filtered %", $3, $4} else {print trim_line[$1], $2, $2 / surviving[$1] * 100, $3, $4}}' metrics/trimSampleTable.tsv metrics/SampleMetrics.stats \
  > report/trimMemSampleTable.tsv
else
  cp metrics/SampleMetrics.stats report/trimMemSampleTable.tsv
fi && \
trim_mem_sample_table=`if [[ -f metrics/trimSampleTable.tsv ]] ; then LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----:|-----:|-----:|-----:|-----:|-----:|-----:"} else {print $1, sprintf("%\47d", $2), sprintf("%\47d", $3), sprintf("%.1f", $4), sprintf("%\47d", $5), sprintf("%.1f", $6), sprintf("%\47d", $7), sprintf("%.1f", $8)}}' report/trimMemSampleTable.tsv ; else LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----:|-----:|-----:"} else {print $1, sprintf("%\47d", $2), sprintf("%\47d", $3), sprintf("%.1f", $4)}}' report/trimMemSampleTable.tsv ; fi` && \
pandoc --to=markdown \
  --template /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.metrics.md \
  --variable trim_mem_sample_table="$trim_mem_sample_table" \
  /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.metrics.md \
  > report/ChipSeq.metrics.md

metrics_report.505a0d8303c0b2fa1256b6cfe93c7f8a.mugqic.done
)
metrics_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$metrics_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: homer_make_tag_directory
#-------------------------------------------------------------------------------
STEP=homer_make_tag_directory
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_1_JOB_ID: homer_make_tag_directory.POL2_shCRTL-1_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2_shCRTL-1_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2_shCRTL-1_Dex_Rep1.ad205b089d8ff4ba09c028ca35b2544c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2_shCRTL-1_Dex_Rep1.ad205b089d8ff4ba09c028ca35b2544c.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2_shCRTL-1_Dex_Rep1 \
  alignment/POL2_shCRTL-1_Dex_Rep1/POL2_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2_shCRTL-1_Dex_Rep1.ad205b089d8ff4ba09c028ca35b2544c.mugqic.done
)
homer_make_tag_directory_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_2_JOB_ID: homer_make_tag_directory.POL2_shCTRL-2_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2_shCTRL-2_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2_shCTRL-2_Dex_Rep1.c5a5ae41f4a89ebed0d91e2691ceb227.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2_shCTRL-2_Dex_Rep1.c5a5ae41f4a89ebed0d91e2691ceb227.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2_shCTRL-2_Dex_Rep1 \
  alignment/POL2_shCTRL-2_Dex_Rep1/POL2_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2_shCTRL-2_Dex_Rep1.c5a5ae41f4a89ebed0d91e2691ceb227.mugqic.done
)
homer_make_tag_directory_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_3_JOB_ID: homer_make_tag_directory.WCE_shCTRL-2_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.WCE_shCTRL-2_EtOH_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.WCE_shCTRL-2_EtOH_Rep1.fa27febe95e428dfafcf3ab46703282d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.WCE_shCTRL-2_EtOH_Rep1.fa27febe95e428dfafcf3ab46703282d.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/WCE_shCTRL-2_EtOH_Rep1 \
  alignment/WCE_shCTRL-2_EtOH_Rep1/WCE_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.WCE_shCTRL-2_EtOH_Rep1.fa27febe95e428dfafcf3ab46703282d.mugqic.done
)
homer_make_tag_directory_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_4_JOB_ID: homer_make_tag_directory.POL2_shNIPBL-3_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2_shNIPBL-3_EtOH_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2_shNIPBL-3_EtOH_Rep1.7834d68ebee894ad2bd92c311928145c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2_shNIPBL-3_EtOH_Rep1.7834d68ebee894ad2bd92c311928145c.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2_shNIPBL-3_EtOH_Rep1 \
  alignment/POL2_shNIPBL-3_EtOH_Rep1/POL2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2_shNIPBL-3_EtOH_Rep1.7834d68ebee894ad2bd92c311928145c.mugqic.done
)
homer_make_tag_directory_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_5_JOB_ID: homer_make_tag_directory.WCE_shNIPBL-5_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.WCE_shNIPBL-5_EtOH_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.WCE_shNIPBL-5_EtOH_Rep1.e54ed40d860735275f0d6b0c7450d5c5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.WCE_shNIPBL-5_EtOH_Rep1.e54ed40d860735275f0d6b0c7450d5c5.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/WCE_shNIPBL-5_EtOH_Rep1 \
  alignment/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.WCE_shNIPBL-5_EtOH_Rep1.e54ed40d860735275f0d6b0c7450d5c5.mugqic.done
)
homer_make_tag_directory_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_6_JOB_ID: homer_make_tag_directory.POL2_shNIPBL-5_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2_shNIPBL-5_EtOH_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2_shNIPBL-5_EtOH_Rep1.b91f4a9dcf773b111615c1a9ce9a9d22.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2_shNIPBL-5_EtOH_Rep1.b91f4a9dcf773b111615c1a9ce9a9d22.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2_shNIPBL-5_EtOH_Rep1 \
  alignment/POL2_shNIPBL-5_EtOH_Rep1/POL2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2_shNIPBL-5_EtOH_Rep1.b91f4a9dcf773b111615c1a9ce9a9d22.mugqic.done
)
homer_make_tag_directory_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_7_JOB_ID: homer_make_tag_directory.POL2_shCTRL-2_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2_shCTRL-2_EtOH_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2_shCTRL-2_EtOH_Rep1.5dae483bcc86ef9bbe80b69a85e2c43e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2_shCTRL-2_EtOH_Rep1.5dae483bcc86ef9bbe80b69a85e2c43e.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2_shCTRL-2_EtOH_Rep1 \
  alignment/POL2_shCTRL-2_EtOH_Rep1/POL2_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2_shCTRL-2_EtOH_Rep1.5dae483bcc86ef9bbe80b69a85e2c43e.mugqic.done
)
homer_make_tag_directory_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_8_JOB_ID: homer_make_tag_directory.WCE_shCRTL-1_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.WCE_shCRTL-1_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.WCE_shCRTL-1_Dex_Rep1.d3502f996aa104acf54fcdbb2288b58e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.WCE_shCRTL-1_Dex_Rep1.d3502f996aa104acf54fcdbb2288b58e.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/WCE_shCRTL-1_Dex_Rep1 \
  alignment/WCE_shCRTL-1_Dex_Rep1/WCE_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.WCE_shCRTL-1_Dex_Rep1.d3502f996aa104acf54fcdbb2288b58e.mugqic.done
)
homer_make_tag_directory_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_9_JOB_ID: homer_make_tag_directory.POL2_shNIPBL-3_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2_shNIPBL-3_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2_shNIPBL-3_Dex_Rep1.23d612f563cf2d6dff0918b6812129bd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2_shNIPBL-3_Dex_Rep1.23d612f563cf2d6dff0918b6812129bd.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2_shNIPBL-3_Dex_Rep1 \
  alignment/POL2_shNIPBL-3_Dex_Rep1/POL2_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2_shNIPBL-3_Dex_Rep1.23d612f563cf2d6dff0918b6812129bd.mugqic.done
)
homer_make_tag_directory_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_10_JOB_ID: homer_make_tag_directory.POL2_shNIPBL-5_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2_shNIPBL-5_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2_shNIPBL-5_Dex_Rep1.c64322eb8ea9ce05cf4e325b7c04a77c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2_shNIPBL-5_Dex_Rep1.c64322eb8ea9ce05cf4e325b7c04a77c.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2_shNIPBL-5_Dex_Rep1 \
  alignment/POL2_shNIPBL-5_Dex_Rep1/POL2_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2_shNIPBL-5_Dex_Rep1.c64322eb8ea9ce05cf4e325b7c04a77c.mugqic.done
)
homer_make_tag_directory_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_11_JOB_ID: homer_make_tag_directory.WCE_shCTRL-1_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.WCE_shCTRL-1_EtOH_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.WCE_shCTRL-1_EtOH_Rep1.bfa2e4a0e1a51ed1eff397524f31c4ea.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.WCE_shCTRL-1_EtOH_Rep1.bfa2e4a0e1a51ed1eff397524f31c4ea.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/WCE_shCTRL-1_EtOH_Rep1 \
  alignment/WCE_shCTRL-1_EtOH_Rep1/WCE_shCTRL-1_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.WCE_shCTRL-1_EtOH_Rep1.bfa2e4a0e1a51ed1eff397524f31c4ea.mugqic.done
)
homer_make_tag_directory_11_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_12_JOB_ID: homer_make_tag_directory.WCE_shCTRL-2_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.WCE_shCTRL-2_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.WCE_shCTRL-2_Dex_Rep1.736587965358b3da0d012a8c529ae76a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.WCE_shCTRL-2_Dex_Rep1.736587965358b3da0d012a8c529ae76a.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/WCE_shCTRL-2_Dex_Rep1 \
  alignment/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.WCE_shCTRL-2_Dex_Rep1.736587965358b3da0d012a8c529ae76a.mugqic.done
)
homer_make_tag_directory_12_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_13_JOB_ID: homer_make_tag_directory.WCE_shNIPBL-3_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.WCE_shNIPBL-3_EtOH_Rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.WCE_shNIPBL-3_EtOH_Rep1.528f139a9c46420d9ee161828dd5bf74.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.WCE_shNIPBL-3_EtOH_Rep1.528f139a9c46420d9ee161828dd5bf74.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/WCE_shNIPBL-3_EtOH_Rep1 \
  alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.WCE_shNIPBL-3_EtOH_Rep1.528f139a9c46420d9ee161828dd5bf74.mugqic.done
)
homer_make_tag_directory_13_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_tag_directory_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_14_JOB_ID: homer_make_tag_directory.POL2_shCRTL-1_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2_shCRTL-1_EtOH_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2_shCRTL-1_EtOH_Rep1.27cf34bf81de47c0922a11b34e75018d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2_shCRTL-1_EtOH_Rep1.27cf34bf81de47c0922a11b34e75018d.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2_shCRTL-1_EtOH_Rep1 \
  alignment/POL2_shCRTL-1_EtOH_Rep1/POL2_shCRTL-1_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2_shCRTL-1_EtOH_Rep1.27cf34bf81de47c0922a11b34e75018d.mugqic.done
)
homer_make_tag_directory_14_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_15_JOB_ID: homer_make_tag_directory.WCE_shNIPBL-5_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.WCE_shNIPBL-5_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.WCE_shNIPBL-5_Dex_Rep1.25379ab405d2974df70e2823fb4c1f1a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.WCE_shNIPBL-5_Dex_Rep1.25379ab405d2974df70e2823fb4c1f1a.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/WCE_shNIPBL-5_Dex_Rep1 \
  alignment/WCE_shNIPBL-5_Dex_Rep1/WCE_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.WCE_shNIPBL-5_Dex_Rep1.25379ab405d2974df70e2823fb4c1f1a.mugqic.done
)
homer_make_tag_directory_15_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_16_JOB_ID: homer_make_tag_directory.WCE_shNIPBL-3_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.WCE_shNIPBL-3_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.WCE_shNIPBL-3_Dex_Rep1.6cc2eefc00ca81a653670d4ada77f9ec.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.WCE_shNIPBL-3_Dex_Rep1.6cc2eefc00ca81a653670d4ada77f9ec.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/WCE_shNIPBL-3_Dex_Rep1 \
  alignment/WCE_shNIPBL-3_Dex_Rep1/WCE_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.WCE_shNIPBL-3_Dex_Rep1.6cc2eefc00ca81a653670d4ada77f9ec.mugqic.done
)
homer_make_tag_directory_16_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_17_JOB_ID: homer_make_tag_directory.POL2-ser2_shNIPBL-3_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2-ser2_shNIPBL-3_EtOH_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2-ser2_shNIPBL-3_EtOH_Rep1.b8625d777a2c8f2a9df0608c522a3cab.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2-ser2_shNIPBL-3_EtOH_Rep1.b8625d777a2c8f2a9df0608c522a3cab.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2-ser2_shNIPBL-3_EtOH_Rep1 \
  alignment/POL2-ser2_shNIPBL-3_EtOH_Rep1/POL2-ser2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2-ser2_shNIPBL-3_EtOH_Rep1.b8625d777a2c8f2a9df0608c522a3cab.mugqic.done
)
homer_make_tag_directory_17_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_18_JOB_ID: homer_make_tag_directory.BRD4_NA_EtOH_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.BRD4_NA_EtOH_Rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.BRD4_NA_EtOH_Rep2.c8665361813ac8154f0040a646ec8b1f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.BRD4_NA_EtOH_Rep2.c8665361813ac8154f0040a646ec8b1f.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/BRD4_NA_EtOH_Rep2 \
  alignment/BRD4_NA_EtOH_Rep2/BRD4_NA_EtOH_Rep2.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.BRD4_NA_EtOH_Rep2.c8665361813ac8154f0040a646ec8b1f.mugqic.done
)
homer_make_tag_directory_18_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_19_JOB_ID: homer_make_tag_directory.WCE_NA_Dex_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.WCE_NA_Dex_Rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.WCE_NA_Dex_Rep2.0e14acf96548f914cb753d6fdffc442a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.WCE_NA_Dex_Rep2.0e14acf96548f914cb753d6fdffc442a.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/WCE_NA_Dex_Rep2 \
  alignment/WCE_NA_Dex_Rep2/WCE_NA_Dex_Rep2.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.WCE_NA_Dex_Rep2.0e14acf96548f914cb753d6fdffc442a.mugqic.done
)
homer_make_tag_directory_19_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_20_JOB_ID: homer_make_tag_directory.CDK9_NA_Dex_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.CDK9_NA_Dex_Rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.CDK9_NA_Dex_Rep2.2723a1cfdd90b8dd5331ceb9c62c926a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.CDK9_NA_Dex_Rep2.2723a1cfdd90b8dd5331ceb9c62c926a.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/CDK9_NA_Dex_Rep2 \
  alignment/CDK9_NA_Dex_Rep2/CDK9_NA_Dex_Rep2.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.CDK9_NA_Dex_Rep2.2723a1cfdd90b8dd5331ceb9c62c926a.mugqic.done
)
homer_make_tag_directory_20_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_21_JOB_ID: homer_make_tag_directory.POL2-ser2_shNIPBL-5_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2-ser2_shNIPBL-5_EtOH_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2-ser2_shNIPBL-5_EtOH_Rep1.fb3a2b34caf1f6bf8ffdd562c413495b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2-ser2_shNIPBL-5_EtOH_Rep1.fb3a2b34caf1f6bf8ffdd562c413495b.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2-ser2_shNIPBL-5_EtOH_Rep1 \
  alignment/POL2-ser2_shNIPBL-5_EtOH_Rep1/POL2-ser2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2-ser2_shNIPBL-5_EtOH_Rep1.fb3a2b34caf1f6bf8ffdd562c413495b.mugqic.done
)
homer_make_tag_directory_21_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_21_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_22_JOB_ID: homer_make_tag_directory.POL2-ser2_shCTRL-2_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2-ser2_shCTRL-2_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2-ser2_shCTRL-2_Dex_Rep1.e8a051bd6fa6e3bbf6b89aaab6459bea.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2-ser2_shCTRL-2_Dex_Rep1.e8a051bd6fa6e3bbf6b89aaab6459bea.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2-ser2_shCTRL-2_Dex_Rep1 \
  alignment/POL2-ser2_shCTRL-2_Dex_Rep1/POL2-ser2_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2-ser2_shCTRL-2_Dex_Rep1.e8a051bd6fa6e3bbf6b89aaab6459bea.mugqic.done
)
homer_make_tag_directory_22_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_22_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_23_JOB_ID: homer_make_tag_directory.POL2-ser2_shNIPBL-3_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2-ser2_shNIPBL-3_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2-ser2_shNIPBL-3_Dex_Rep1.bfa81a9a199c181013ec0a46dea31700.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2-ser2_shNIPBL-3_Dex_Rep1.bfa81a9a199c181013ec0a46dea31700.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2-ser2_shNIPBL-3_Dex_Rep1 \
  alignment/POL2-ser2_shNIPBL-3_Dex_Rep1/POL2-ser2_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2-ser2_shNIPBL-3_Dex_Rep1.bfa81a9a199c181013ec0a46dea31700.mugqic.done
)
homer_make_tag_directory_23_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_23_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_24_JOB_ID: homer_make_tag_directory.POL2-ser2_shCRTL-1_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2-ser2_shCRTL-1_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2-ser2_shCRTL-1_Dex_Rep1.446b882f3a20b63df4102b7edc4e8729.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2-ser2_shCRTL-1_Dex_Rep1.446b882f3a20b63df4102b7edc4e8729.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2-ser2_shCRTL-1_Dex_Rep1 \
  alignment/POL2-ser2_shCRTL-1_Dex_Rep1/POL2-ser2_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2-ser2_shCRTL-1_Dex_Rep1.446b882f3a20b63df4102b7edc4e8729.mugqic.done
)
homer_make_tag_directory_24_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_24_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_25_JOB_ID: homer_make_tag_directory.POL2-ser2_shNIPBL-5_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2-ser2_shNIPBL-5_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2-ser2_shNIPBL-5_Dex_Rep1.cf2e2f5aade67a9af86ca4dada1cfd03.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2-ser2_shNIPBL-5_Dex_Rep1.cf2e2f5aade67a9af86ca4dada1cfd03.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2-ser2_shNIPBL-5_Dex_Rep1 \
  alignment/POL2-ser2_shNIPBL-5_Dex_Rep1/POL2-ser2_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2-ser2_shNIPBL-5_Dex_Rep1.cf2e2f5aade67a9af86ca4dada1cfd03.mugqic.done
)
homer_make_tag_directory_25_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_25_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_26_JOB_ID: homer_make_tag_directory.WCE_NA_EtOH_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.WCE_NA_EtOH_Rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.WCE_NA_EtOH_Rep2.14027a8d462e669b9631e39023b9a75e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.WCE_NA_EtOH_Rep2.14027a8d462e669b9631e39023b9a75e.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/WCE_NA_EtOH_Rep2 \
  alignment/WCE_NA_EtOH_Rep2/WCE_NA_EtOH_Rep2.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.WCE_NA_EtOH_Rep2.14027a8d462e669b9631e39023b9a75e.mugqic.done
)
homer_make_tag_directory_26_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_26_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_27_JOB_ID: homer_make_tag_directory.POL2-ser2_shCRTL-1_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2-ser2_shCRTL-1_EtOH_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2-ser2_shCRTL-1_EtOH_Rep1.f9e0f663860161598e45905a932f21b2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2-ser2_shCRTL-1_EtOH_Rep1.f9e0f663860161598e45905a932f21b2.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2-ser2_shCRTL-1_EtOH_Rep1 \
  alignment/POL2-ser2_shCRTL-1_EtOH_Rep1/POL2-ser2_shCRTL-1_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2-ser2_shCRTL-1_EtOH_Rep1.f9e0f663860161598e45905a932f21b2.mugqic.done
)
homer_make_tag_directory_27_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_27_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_28_JOB_ID: homer_make_tag_directory.POL2-ser2_shCTRL-2_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2-ser2_shCTRL-2_EtOH_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2-ser2_shCTRL-2_EtOH_Rep1.e47a5ff816e11541d14a55058ed43c8b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2-ser2_shCTRL-2_EtOH_Rep1.e47a5ff816e11541d14a55058ed43c8b.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2-ser2_shCTRL-2_EtOH_Rep1 \
  alignment/POL2-ser2_shCTRL-2_EtOH_Rep1/POL2-ser2_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2-ser2_shCTRL-2_EtOH_Rep1.e47a5ff816e11541d14a55058ed43c8b.mugqic.done
)
homer_make_tag_directory_28_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_28_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_29_JOB_ID: homer_make_tag_directory.CDK9_NA_EtOH_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.CDK9_NA_EtOH_Rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.CDK9_NA_EtOH_Rep2.537b10d094a31444fec35923c51c3f74.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.CDK9_NA_EtOH_Rep2.537b10d094a31444fec35923c51c3f74.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/CDK9_NA_EtOH_Rep2 \
  alignment/CDK9_NA_EtOH_Rep2/CDK9_NA_EtOH_Rep2.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.CDK9_NA_EtOH_Rep2.537b10d094a31444fec35923c51c3f74.mugqic.done
)
homer_make_tag_directory_29_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_29_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_30_JOB_ID: homer_make_tag_directory.BRD4_NA_Dex_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.BRD4_NA_Dex_Rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.BRD4_NA_Dex_Rep2.67f60ca782cbfbfd086a4e768a8f4566.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.BRD4_NA_Dex_Rep2.67f60ca782cbfbfd086a4e768a8f4566.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/BRD4_NA_Dex_Rep2 \
  alignment/BRD4_NA_Dex_Rep2/BRD4_NA_Dex_Rep2.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.BRD4_NA_Dex_Rep2.67f60ca782cbfbfd086a4e768a8f4566.mugqic.done
)
homer_make_tag_directory_30_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_30_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: qc_metrics
#-------------------------------------------------------------------------------
STEP=qc_metrics
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: qc_metrics_1_JOB_ID: qc_plots_R
#-------------------------------------------------------------------------------
JOB_NAME=qc_plots_R
JOB_DEPENDENCIES=$homer_make_tag_directory_1_JOB_ID:$homer_make_tag_directory_2_JOB_ID:$homer_make_tag_directory_3_JOB_ID:$homer_make_tag_directory_4_JOB_ID:$homer_make_tag_directory_5_JOB_ID:$homer_make_tag_directory_6_JOB_ID:$homer_make_tag_directory_7_JOB_ID:$homer_make_tag_directory_8_JOB_ID:$homer_make_tag_directory_9_JOB_ID:$homer_make_tag_directory_10_JOB_ID:$homer_make_tag_directory_11_JOB_ID:$homer_make_tag_directory_12_JOB_ID:$homer_make_tag_directory_13_JOB_ID:$homer_make_tag_directory_14_JOB_ID:$homer_make_tag_directory_15_JOB_ID:$homer_make_tag_directory_16_JOB_ID:$homer_make_tag_directory_17_JOB_ID:$homer_make_tag_directory_18_JOB_ID:$homer_make_tag_directory_19_JOB_ID:$homer_make_tag_directory_20_JOB_ID:$homer_make_tag_directory_21_JOB_ID:$homer_make_tag_directory_22_JOB_ID:$homer_make_tag_directory_23_JOB_ID:$homer_make_tag_directory_24_JOB_ID:$homer_make_tag_directory_25_JOB_ID:$homer_make_tag_directory_26_JOB_ID:$homer_make_tag_directory_27_JOB_ID:$homer_make_tag_directory_28_JOB_ID:$homer_make_tag_directory_29_JOB_ID:$homer_make_tag_directory_30_JOB_ID
JOB_DONE=job_output/qc_metrics/qc_plots_R.15b0e86760c8425d81478c44ae38f14d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'qc_plots_R.15b0e86760c8425d81478c44ae38f14d.mugqic.done'
module load mugqic/mugqic_tools/2.1.5 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p graphs && \
Rscript $R_TOOLS/chipSeqGenerateQCMetrics.R \
  ../../raw/design.txt \
  /gs/scratch/efournier/A549_Michele/output/chip-pipeline-GRCh38 && \
cp /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.qc_metrics.md report/ChipSeq.qc_metrics.md && \
for sample in POL2_shCRTL-1_Dex_Rep1 POL2_shCTRL-2_Dex_Rep1 WCE_shCTRL-2_EtOH_Rep1 POL2_shNIPBL-3_EtOH_Rep1 WCE_shNIPBL-5_EtOH_Rep1 POL2_shNIPBL-5_EtOH_Rep1 POL2_shCTRL-2_EtOH_Rep1 WCE_shCRTL-1_Dex_Rep1 POL2_shNIPBL-3_Dex_Rep1 POL2_shNIPBL-5_Dex_Rep1 WCE_shCTRL-1_EtOH_Rep1 WCE_shCTRL-2_Dex_Rep1 WCE_shNIPBL-3_EtOH_Rep1 POL2_shCRTL-1_EtOH_Rep1 WCE_shNIPBL-5_Dex_Rep1 WCE_shNIPBL-3_Dex_Rep1 POL2-ser2_shNIPBL-3_EtOH_Rep1 BRD4_NA_EtOH_Rep2 WCE_NA_Dex_Rep2 CDK9_NA_Dex_Rep2 POL2-ser2_shNIPBL-5_EtOH_Rep1 POL2-ser2_shCTRL-2_Dex_Rep1 POL2-ser2_shNIPBL-3_Dex_Rep1 POL2-ser2_shCRTL-1_Dex_Rep1 POL2-ser2_shNIPBL-5_Dex_Rep1 WCE_NA_EtOH_Rep2 POL2-ser2_shCRTL-1_EtOH_Rep1 POL2-ser2_shCTRL-2_EtOH_Rep1 CDK9_NA_EtOH_Rep2 BRD4_NA_Dex_Rep2
do
  cp --parents graphs/${sample}_QC_Metrics.ps report/
  convert -rotate 90 graphs/${sample}_QC_Metrics.ps report/graphs/${sample}_QC_Metrics.png
  echo -e "----

![QC Metrics for Sample $sample ([download high-res image](graphs/${sample}_QC_Metrics.ps))](graphs/${sample}_QC_Metrics.png)
" \
  >> report/ChipSeq.qc_metrics.md
done
qc_plots_R.15b0e86760c8425d81478c44ae38f14d.mugqic.done
)
qc_metrics_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$qc_metrics_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: homer_make_ucsc_file
#-------------------------------------------------------------------------------
STEP=homer_make_ucsc_file
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_1_JOB_ID: homer_make_ucsc_file.POL2_shCRTL-1_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2_shCRTL-1_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_1_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2_shCRTL-1_Dex_Rep1.9b5f0a6d5449c5893004ca49ca9b8b2c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2_shCRTL-1_Dex_Rep1.9b5f0a6d5449c5893004ca49ca9b8b2c.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2_shCRTL-1_Dex_Rep1 && \
makeUCSCfile \
  tags/POL2_shCRTL-1_Dex_Rep1 | \
gzip -1 -c > tracks/POL2_shCRTL-1_Dex_Rep1/POL2_shCRTL-1_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2_shCRTL-1_Dex_Rep1.9b5f0a6d5449c5893004ca49ca9b8b2c.mugqic.done
)
homer_make_ucsc_file_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_2_JOB_ID: homer_make_ucsc_file.POL2_shCTRL-2_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2_shCTRL-2_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_2_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2_shCTRL-2_Dex_Rep1.2ff023f60845cd80e6d06b32606e1747.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2_shCTRL-2_Dex_Rep1.2ff023f60845cd80e6d06b32606e1747.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2_shCTRL-2_Dex_Rep1 && \
makeUCSCfile \
  tags/POL2_shCTRL-2_Dex_Rep1 | \
gzip -1 -c > tracks/POL2_shCTRL-2_Dex_Rep1/POL2_shCTRL-2_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2_shCTRL-2_Dex_Rep1.2ff023f60845cd80e6d06b32606e1747.mugqic.done
)
homer_make_ucsc_file_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_3_JOB_ID: homer_make_ucsc_file.WCE_shCTRL-2_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.WCE_shCTRL-2_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_3_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.WCE_shCTRL-2_EtOH_Rep1.98b8c509aff83fdfe352f303f0d28ef3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.WCE_shCTRL-2_EtOH_Rep1.98b8c509aff83fdfe352f303f0d28ef3.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/WCE_shCTRL-2_EtOH_Rep1 && \
makeUCSCfile \
  tags/WCE_shCTRL-2_EtOH_Rep1 | \
gzip -1 -c > tracks/WCE_shCTRL-2_EtOH_Rep1/WCE_shCTRL-2_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.WCE_shCTRL-2_EtOH_Rep1.98b8c509aff83fdfe352f303f0d28ef3.mugqic.done
)
homer_make_ucsc_file_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_4_JOB_ID: homer_make_ucsc_file.POL2_shNIPBL-3_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2_shNIPBL-3_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_4_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2_shNIPBL-3_EtOH_Rep1.67aac446d5eb2bac15afdb71264ec83a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2_shNIPBL-3_EtOH_Rep1.67aac446d5eb2bac15afdb71264ec83a.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2_shNIPBL-3_EtOH_Rep1 && \
makeUCSCfile \
  tags/POL2_shNIPBL-3_EtOH_Rep1 | \
gzip -1 -c > tracks/POL2_shNIPBL-3_EtOH_Rep1/POL2_shNIPBL-3_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2_shNIPBL-3_EtOH_Rep1.67aac446d5eb2bac15afdb71264ec83a.mugqic.done
)
homer_make_ucsc_file_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_5_JOB_ID: homer_make_ucsc_file.WCE_shNIPBL-5_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.WCE_shNIPBL-5_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_5_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.WCE_shNIPBL-5_EtOH_Rep1.2a9ab5f21263fc1d0af09b77c8481466.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.WCE_shNIPBL-5_EtOH_Rep1.2a9ab5f21263fc1d0af09b77c8481466.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/WCE_shNIPBL-5_EtOH_Rep1 && \
makeUCSCfile \
  tags/WCE_shNIPBL-5_EtOH_Rep1 | \
gzip -1 -c > tracks/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.WCE_shNIPBL-5_EtOH_Rep1.2a9ab5f21263fc1d0af09b77c8481466.mugqic.done
)
homer_make_ucsc_file_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_6_JOB_ID: homer_make_ucsc_file.POL2_shNIPBL-5_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2_shNIPBL-5_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_6_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2_shNIPBL-5_EtOH_Rep1.072f60a4277188bdb769e52b1a1c73fb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2_shNIPBL-5_EtOH_Rep1.072f60a4277188bdb769e52b1a1c73fb.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2_shNIPBL-5_EtOH_Rep1 && \
makeUCSCfile \
  tags/POL2_shNIPBL-5_EtOH_Rep1 | \
gzip -1 -c > tracks/POL2_shNIPBL-5_EtOH_Rep1/POL2_shNIPBL-5_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2_shNIPBL-5_EtOH_Rep1.072f60a4277188bdb769e52b1a1c73fb.mugqic.done
)
homer_make_ucsc_file_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_7_JOB_ID: homer_make_ucsc_file.POL2_shCTRL-2_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2_shCTRL-2_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_7_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2_shCTRL-2_EtOH_Rep1.c4f6118bcfa83e0e5a9d5de09120da7e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2_shCTRL-2_EtOH_Rep1.c4f6118bcfa83e0e5a9d5de09120da7e.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2_shCTRL-2_EtOH_Rep1 && \
makeUCSCfile \
  tags/POL2_shCTRL-2_EtOH_Rep1 | \
gzip -1 -c > tracks/POL2_shCTRL-2_EtOH_Rep1/POL2_shCTRL-2_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2_shCTRL-2_EtOH_Rep1.c4f6118bcfa83e0e5a9d5de09120da7e.mugqic.done
)
homer_make_ucsc_file_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_8_JOB_ID: homer_make_ucsc_file.WCE_shCRTL-1_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.WCE_shCRTL-1_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_8_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.WCE_shCRTL-1_Dex_Rep1.cfc8b432aebc51ce3646b02221ddbecb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.WCE_shCRTL-1_Dex_Rep1.cfc8b432aebc51ce3646b02221ddbecb.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/WCE_shCRTL-1_Dex_Rep1 && \
makeUCSCfile \
  tags/WCE_shCRTL-1_Dex_Rep1 | \
gzip -1 -c > tracks/WCE_shCRTL-1_Dex_Rep1/WCE_shCRTL-1_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.WCE_shCRTL-1_Dex_Rep1.cfc8b432aebc51ce3646b02221ddbecb.mugqic.done
)
homer_make_ucsc_file_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_9_JOB_ID: homer_make_ucsc_file.POL2_shNIPBL-3_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2_shNIPBL-3_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_9_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2_shNIPBL-3_Dex_Rep1.db2923255c47cbadd52fcd2939daaf37.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2_shNIPBL-3_Dex_Rep1.db2923255c47cbadd52fcd2939daaf37.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2_shNIPBL-3_Dex_Rep1 && \
makeUCSCfile \
  tags/POL2_shNIPBL-3_Dex_Rep1 | \
gzip -1 -c > tracks/POL2_shNIPBL-3_Dex_Rep1/POL2_shNIPBL-3_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2_shNIPBL-3_Dex_Rep1.db2923255c47cbadd52fcd2939daaf37.mugqic.done
)
homer_make_ucsc_file_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_10_JOB_ID: homer_make_ucsc_file.POL2_shNIPBL-5_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2_shNIPBL-5_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_10_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2_shNIPBL-5_Dex_Rep1.62d40007e60dde339e0c3f1ba459afec.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2_shNIPBL-5_Dex_Rep1.62d40007e60dde339e0c3f1ba459afec.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2_shNIPBL-5_Dex_Rep1 && \
makeUCSCfile \
  tags/POL2_shNIPBL-5_Dex_Rep1 | \
gzip -1 -c > tracks/POL2_shNIPBL-5_Dex_Rep1/POL2_shNIPBL-5_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2_shNIPBL-5_Dex_Rep1.62d40007e60dde339e0c3f1ba459afec.mugqic.done
)
homer_make_ucsc_file_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_11_JOB_ID: homer_make_ucsc_file.WCE_shCTRL-1_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.WCE_shCTRL-1_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_11_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.WCE_shCTRL-1_EtOH_Rep1.f16f95fe833adf3a59f1a746799dfde8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.WCE_shCTRL-1_EtOH_Rep1.f16f95fe833adf3a59f1a746799dfde8.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/WCE_shCTRL-1_EtOH_Rep1 && \
makeUCSCfile \
  tags/WCE_shCTRL-1_EtOH_Rep1 | \
gzip -1 -c > tracks/WCE_shCTRL-1_EtOH_Rep1/WCE_shCTRL-1_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.WCE_shCTRL-1_EtOH_Rep1.f16f95fe833adf3a59f1a746799dfde8.mugqic.done
)
homer_make_ucsc_file_11_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_12_JOB_ID: homer_make_ucsc_file.WCE_shCTRL-2_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.WCE_shCTRL-2_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_12_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.WCE_shCTRL-2_Dex_Rep1.431698b678ef0e0be31d85d911635121.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.WCE_shCTRL-2_Dex_Rep1.431698b678ef0e0be31d85d911635121.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/WCE_shCTRL-2_Dex_Rep1 && \
makeUCSCfile \
  tags/WCE_shCTRL-2_Dex_Rep1 | \
gzip -1 -c > tracks/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.WCE_shCTRL-2_Dex_Rep1.431698b678ef0e0be31d85d911635121.mugqic.done
)
homer_make_ucsc_file_12_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_13_JOB_ID: homer_make_ucsc_file.WCE_shNIPBL-3_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.WCE_shNIPBL-3_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_13_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.WCE_shNIPBL-3_EtOH_Rep1.f77411ac2e5e267023e8fe4d9cc49ee5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.WCE_shNIPBL-3_EtOH_Rep1.f77411ac2e5e267023e8fe4d9cc49ee5.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/WCE_shNIPBL-3_EtOH_Rep1 && \
makeUCSCfile \
  tags/WCE_shNIPBL-3_EtOH_Rep1 | \
gzip -1 -c > tracks/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.WCE_shNIPBL-3_EtOH_Rep1.f77411ac2e5e267023e8fe4d9cc49ee5.mugqic.done
)
homer_make_ucsc_file_13_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_14_JOB_ID: homer_make_ucsc_file.POL2_shCRTL-1_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2_shCRTL-1_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_14_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2_shCRTL-1_EtOH_Rep1.81bb3c570754360746c2f5602ea03f78.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2_shCRTL-1_EtOH_Rep1.81bb3c570754360746c2f5602ea03f78.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2_shCRTL-1_EtOH_Rep1 && \
makeUCSCfile \
  tags/POL2_shCRTL-1_EtOH_Rep1 | \
gzip -1 -c > tracks/POL2_shCRTL-1_EtOH_Rep1/POL2_shCRTL-1_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2_shCRTL-1_EtOH_Rep1.81bb3c570754360746c2f5602ea03f78.mugqic.done
)
homer_make_ucsc_file_14_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_15_JOB_ID: homer_make_ucsc_file.WCE_shNIPBL-5_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.WCE_shNIPBL-5_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_15_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.WCE_shNIPBL-5_Dex_Rep1.9107fea1c09d5039335ecaf6386f829f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.WCE_shNIPBL-5_Dex_Rep1.9107fea1c09d5039335ecaf6386f829f.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/WCE_shNIPBL-5_Dex_Rep1 && \
makeUCSCfile \
  tags/WCE_shNIPBL-5_Dex_Rep1 | \
gzip -1 -c > tracks/WCE_shNIPBL-5_Dex_Rep1/WCE_shNIPBL-5_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.WCE_shNIPBL-5_Dex_Rep1.9107fea1c09d5039335ecaf6386f829f.mugqic.done
)
homer_make_ucsc_file_15_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_16_JOB_ID: homer_make_ucsc_file.WCE_shNIPBL-3_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.WCE_shNIPBL-3_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_16_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.WCE_shNIPBL-3_Dex_Rep1.c2fa1342c5dafe28ef0594cc36048fcf.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.WCE_shNIPBL-3_Dex_Rep1.c2fa1342c5dafe28ef0594cc36048fcf.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/WCE_shNIPBL-3_Dex_Rep1 && \
makeUCSCfile \
  tags/WCE_shNIPBL-3_Dex_Rep1 | \
gzip -1 -c > tracks/WCE_shNIPBL-3_Dex_Rep1/WCE_shNIPBL-3_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.WCE_shNIPBL-3_Dex_Rep1.c2fa1342c5dafe28ef0594cc36048fcf.mugqic.done
)
homer_make_ucsc_file_16_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_17_JOB_ID: homer_make_ucsc_file.POL2-ser2_shNIPBL-3_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2-ser2_shNIPBL-3_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_17_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2-ser2_shNIPBL-3_EtOH_Rep1.adb63d8b47167521b84de19140300d51.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2-ser2_shNIPBL-3_EtOH_Rep1.adb63d8b47167521b84de19140300d51.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2-ser2_shNIPBL-3_EtOH_Rep1 && \
makeUCSCfile \
  tags/POL2-ser2_shNIPBL-3_EtOH_Rep1 | \
gzip -1 -c > tracks/POL2-ser2_shNIPBL-3_EtOH_Rep1/POL2-ser2_shNIPBL-3_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2-ser2_shNIPBL-3_EtOH_Rep1.adb63d8b47167521b84de19140300d51.mugqic.done
)
homer_make_ucsc_file_17_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_18_JOB_ID: homer_make_ucsc_file.BRD4_NA_EtOH_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.BRD4_NA_EtOH_Rep2
JOB_DEPENDENCIES=$homer_make_tag_directory_18_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.BRD4_NA_EtOH_Rep2.4636a2be9f2cb389242700c72ef8b03b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.BRD4_NA_EtOH_Rep2.4636a2be9f2cb389242700c72ef8b03b.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/BRD4_NA_EtOH_Rep2 && \
makeUCSCfile \
  tags/BRD4_NA_EtOH_Rep2 | \
gzip -1 -c > tracks/BRD4_NA_EtOH_Rep2/BRD4_NA_EtOH_Rep2.ucsc.bedGraph.gz
homer_make_ucsc_file.BRD4_NA_EtOH_Rep2.4636a2be9f2cb389242700c72ef8b03b.mugqic.done
)
homer_make_ucsc_file_18_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_19_JOB_ID: homer_make_ucsc_file.WCE_NA_Dex_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.WCE_NA_Dex_Rep2
JOB_DEPENDENCIES=$homer_make_tag_directory_19_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.WCE_NA_Dex_Rep2.3b783316b50b151c3f2f973d652b411e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.WCE_NA_Dex_Rep2.3b783316b50b151c3f2f973d652b411e.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/WCE_NA_Dex_Rep2 && \
makeUCSCfile \
  tags/WCE_NA_Dex_Rep2 | \
gzip -1 -c > tracks/WCE_NA_Dex_Rep2/WCE_NA_Dex_Rep2.ucsc.bedGraph.gz
homer_make_ucsc_file.WCE_NA_Dex_Rep2.3b783316b50b151c3f2f973d652b411e.mugqic.done
)
homer_make_ucsc_file_19_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_20_JOB_ID: homer_make_ucsc_file.CDK9_NA_Dex_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.CDK9_NA_Dex_Rep2
JOB_DEPENDENCIES=$homer_make_tag_directory_20_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.CDK9_NA_Dex_Rep2.f864f712f7b800a1a0740e249c76c195.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.CDK9_NA_Dex_Rep2.f864f712f7b800a1a0740e249c76c195.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/CDK9_NA_Dex_Rep2 && \
makeUCSCfile \
  tags/CDK9_NA_Dex_Rep2 | \
gzip -1 -c > tracks/CDK9_NA_Dex_Rep2/CDK9_NA_Dex_Rep2.ucsc.bedGraph.gz
homer_make_ucsc_file.CDK9_NA_Dex_Rep2.f864f712f7b800a1a0740e249c76c195.mugqic.done
)
homer_make_ucsc_file_20_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_21_JOB_ID: homer_make_ucsc_file.POL2-ser2_shNIPBL-5_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2-ser2_shNIPBL-5_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_21_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2-ser2_shNIPBL-5_EtOH_Rep1.2f52681a1c173bb17fe2df7211466fd3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2-ser2_shNIPBL-5_EtOH_Rep1.2f52681a1c173bb17fe2df7211466fd3.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2-ser2_shNIPBL-5_EtOH_Rep1 && \
makeUCSCfile \
  tags/POL2-ser2_shNIPBL-5_EtOH_Rep1 | \
gzip -1 -c > tracks/POL2-ser2_shNIPBL-5_EtOH_Rep1/POL2-ser2_shNIPBL-5_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2-ser2_shNIPBL-5_EtOH_Rep1.2f52681a1c173bb17fe2df7211466fd3.mugqic.done
)
homer_make_ucsc_file_21_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_21_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_22_JOB_ID: homer_make_ucsc_file.POL2-ser2_shCTRL-2_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2-ser2_shCTRL-2_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_22_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2-ser2_shCTRL-2_Dex_Rep1.b41dd856e84a854a57ea7af069e3b0a2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2-ser2_shCTRL-2_Dex_Rep1.b41dd856e84a854a57ea7af069e3b0a2.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2-ser2_shCTRL-2_Dex_Rep1 && \
makeUCSCfile \
  tags/POL2-ser2_shCTRL-2_Dex_Rep1 | \
gzip -1 -c > tracks/POL2-ser2_shCTRL-2_Dex_Rep1/POL2-ser2_shCTRL-2_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2-ser2_shCTRL-2_Dex_Rep1.b41dd856e84a854a57ea7af069e3b0a2.mugqic.done
)
homer_make_ucsc_file_22_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_22_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_23_JOB_ID: homer_make_ucsc_file.POL2-ser2_shNIPBL-3_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2-ser2_shNIPBL-3_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_23_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2-ser2_shNIPBL-3_Dex_Rep1.64f4382190ebcc4d63591699b1bf0c5d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2-ser2_shNIPBL-3_Dex_Rep1.64f4382190ebcc4d63591699b1bf0c5d.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2-ser2_shNIPBL-3_Dex_Rep1 && \
makeUCSCfile \
  tags/POL2-ser2_shNIPBL-3_Dex_Rep1 | \
gzip -1 -c > tracks/POL2-ser2_shNIPBL-3_Dex_Rep1/POL2-ser2_shNIPBL-3_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2-ser2_shNIPBL-3_Dex_Rep1.64f4382190ebcc4d63591699b1bf0c5d.mugqic.done
)
homer_make_ucsc_file_23_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_23_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_24_JOB_ID: homer_make_ucsc_file.POL2-ser2_shCRTL-1_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2-ser2_shCRTL-1_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_24_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2-ser2_shCRTL-1_Dex_Rep1.a4f8c11399635c3f61583fc87d8f6f4c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2-ser2_shCRTL-1_Dex_Rep1.a4f8c11399635c3f61583fc87d8f6f4c.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2-ser2_shCRTL-1_Dex_Rep1 && \
makeUCSCfile \
  tags/POL2-ser2_shCRTL-1_Dex_Rep1 | \
gzip -1 -c > tracks/POL2-ser2_shCRTL-1_Dex_Rep1/POL2-ser2_shCRTL-1_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2-ser2_shCRTL-1_Dex_Rep1.a4f8c11399635c3f61583fc87d8f6f4c.mugqic.done
)
homer_make_ucsc_file_24_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_24_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_25_JOB_ID: homer_make_ucsc_file.POL2-ser2_shNIPBL-5_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2-ser2_shNIPBL-5_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_25_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2-ser2_shNIPBL-5_Dex_Rep1.8c49c24db13980a6f2bd9ed7290ae38e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2-ser2_shNIPBL-5_Dex_Rep1.8c49c24db13980a6f2bd9ed7290ae38e.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2-ser2_shNIPBL-5_Dex_Rep1 && \
makeUCSCfile \
  tags/POL2-ser2_shNIPBL-5_Dex_Rep1 | \
gzip -1 -c > tracks/POL2-ser2_shNIPBL-5_Dex_Rep1/POL2-ser2_shNIPBL-5_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2-ser2_shNIPBL-5_Dex_Rep1.8c49c24db13980a6f2bd9ed7290ae38e.mugqic.done
)
homer_make_ucsc_file_25_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_25_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_26_JOB_ID: homer_make_ucsc_file.WCE_NA_EtOH_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.WCE_NA_EtOH_Rep2
JOB_DEPENDENCIES=$homer_make_tag_directory_26_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.WCE_NA_EtOH_Rep2.5a17c8aaf015d0317cdbf97942a35ea4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.WCE_NA_EtOH_Rep2.5a17c8aaf015d0317cdbf97942a35ea4.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/WCE_NA_EtOH_Rep2 && \
makeUCSCfile \
  tags/WCE_NA_EtOH_Rep2 | \
gzip -1 -c > tracks/WCE_NA_EtOH_Rep2/WCE_NA_EtOH_Rep2.ucsc.bedGraph.gz
homer_make_ucsc_file.WCE_NA_EtOH_Rep2.5a17c8aaf015d0317cdbf97942a35ea4.mugqic.done
)
homer_make_ucsc_file_26_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_26_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_27_JOB_ID: homer_make_ucsc_file.POL2-ser2_shCRTL-1_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2-ser2_shCRTL-1_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_27_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2-ser2_shCRTL-1_EtOH_Rep1.9fcf8b131cb1e5000ad1f62acc8589ed.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2-ser2_shCRTL-1_EtOH_Rep1.9fcf8b131cb1e5000ad1f62acc8589ed.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2-ser2_shCRTL-1_EtOH_Rep1 && \
makeUCSCfile \
  tags/POL2-ser2_shCRTL-1_EtOH_Rep1 | \
gzip -1 -c > tracks/POL2-ser2_shCRTL-1_EtOH_Rep1/POL2-ser2_shCRTL-1_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2-ser2_shCRTL-1_EtOH_Rep1.9fcf8b131cb1e5000ad1f62acc8589ed.mugqic.done
)
homer_make_ucsc_file_27_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_27_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_28_JOB_ID: homer_make_ucsc_file.POL2-ser2_shCTRL-2_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2-ser2_shCTRL-2_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_28_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2-ser2_shCTRL-2_EtOH_Rep1.e4e9e77f3060c970f16e1008c5edd780.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2-ser2_shCTRL-2_EtOH_Rep1.e4e9e77f3060c970f16e1008c5edd780.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2-ser2_shCTRL-2_EtOH_Rep1 && \
makeUCSCfile \
  tags/POL2-ser2_shCTRL-2_EtOH_Rep1 | \
gzip -1 -c > tracks/POL2-ser2_shCTRL-2_EtOH_Rep1/POL2-ser2_shCTRL-2_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2-ser2_shCTRL-2_EtOH_Rep1.e4e9e77f3060c970f16e1008c5edd780.mugqic.done
)
homer_make_ucsc_file_28_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_28_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_29_JOB_ID: homer_make_ucsc_file.CDK9_NA_EtOH_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.CDK9_NA_EtOH_Rep2
JOB_DEPENDENCIES=$homer_make_tag_directory_29_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.CDK9_NA_EtOH_Rep2.14cc547494c803d446cddb449855d9e3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.CDK9_NA_EtOH_Rep2.14cc547494c803d446cddb449855d9e3.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/CDK9_NA_EtOH_Rep2 && \
makeUCSCfile \
  tags/CDK9_NA_EtOH_Rep2 | \
gzip -1 -c > tracks/CDK9_NA_EtOH_Rep2/CDK9_NA_EtOH_Rep2.ucsc.bedGraph.gz
homer_make_ucsc_file.CDK9_NA_EtOH_Rep2.14cc547494c803d446cddb449855d9e3.mugqic.done
)
homer_make_ucsc_file_29_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_29_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_30_JOB_ID: homer_make_ucsc_file.BRD4_NA_Dex_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.BRD4_NA_Dex_Rep2
JOB_DEPENDENCIES=$homer_make_tag_directory_30_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.BRD4_NA_Dex_Rep2.9e1778243fd937424a951a786512ff4d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.BRD4_NA_Dex_Rep2.9e1778243fd937424a951a786512ff4d.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/BRD4_NA_Dex_Rep2 && \
makeUCSCfile \
  tags/BRD4_NA_Dex_Rep2 | \
gzip -1 -c > tracks/BRD4_NA_Dex_Rep2/BRD4_NA_Dex_Rep2.ucsc.bedGraph.gz
homer_make_ucsc_file.BRD4_NA_Dex_Rep2.9e1778243fd937424a951a786512ff4d.mugqic.done
)
homer_make_ucsc_file_30_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_30_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_31_JOB_ID: homer_make_ucsc_file_report
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file_report
JOB_DEPENDENCIES=$homer_make_ucsc_file_30_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file_report.c9cac04b3bc71f34cbcde042cf5b419d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file_report.c9cac04b3bc71f34cbcde042cf5b419d.mugqic.done'
mkdir -p report && \
zip -r report/tracks.zip tracks/*/*.ucsc.bedGraph.gz && \
cp /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.homer_make_ucsc_file.md report/
homer_make_ucsc_file_report.c9cac04b3bc71f34cbcde042cf5b419d.mugqic.done
)
homer_make_ucsc_file_31_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_31_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: macs2_callpeak
#-------------------------------------------------------------------------------
STEP=macs2_callpeak
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_1_JOB_ID: macs2_callpeak.POL2_shCTRL-2_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shCTRL-2_Dex
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shCTRL-2_Dex.7319e09db4591ebc2100584d7c52c485.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shCTRL-2_Dex.7319e09db4591ebc2100584d7c52c485.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shCTRL-2_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shCTRL-2_Dex_Rep1/POL2_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shCTRL-2_Dex/POL2_shCTRL-2_Dex \
  >& peak_call/POL2_shCTRL-2_Dex/POL2_shCTRL-2_Dex.diag.macs.out
macs2_callpeak.POL2_shCTRL-2_Dex.7319e09db4591ebc2100584d7c52c485.mugqic.done
)
macs2_callpeak_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$macs2_callpeak_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_2_JOB_ID: macs2_callpeak.POL2_shNIPBL-3_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shNIPBL-3_EtOH
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shNIPBL-3_EtOH.8ca44a0b913167ee0cba25206b687df9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shNIPBL-3_EtOH.8ca44a0b913167ee0cba25206b687df9.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shNIPBL-3_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shNIPBL-3_EtOH_Rep1/POL2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shNIPBL-3_EtOH/POL2_shNIPBL-3_EtOH \
  >& peak_call/POL2_shNIPBL-3_EtOH/POL2_shNIPBL-3_EtOH.diag.macs.out
macs2_callpeak.POL2_shNIPBL-3_EtOH.8ca44a0b913167ee0cba25206b687df9.mugqic.done
)
macs2_callpeak_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_3_JOB_ID: macs2_callpeak.POL2_shNIPBL_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shNIPBL_EtOH
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shNIPBL_EtOH.3a7cc5c9c25f3c89d127077b10261035.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shNIPBL_EtOH.3a7cc5c9c25f3c89d127077b10261035.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shNIPBL_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shNIPBL-3_EtOH_Rep1/POL2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  alignment/POL2_shNIPBL-5_EtOH_Rep1/POL2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  alignment/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shNIPBL_EtOH/POL2_shNIPBL_EtOH \
  >& peak_call/POL2_shNIPBL_EtOH/POL2_shNIPBL_EtOH.diag.macs.out
macs2_callpeak.POL2_shNIPBL_EtOH.3a7cc5c9c25f3c89d127077b10261035.mugqic.done
)
macs2_callpeak_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_4_JOB_ID: macs2_callpeak.POL2-ser2_shNIPBL-3_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shNIPBL-3_EtOH
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shNIPBL-3_EtOH.d5def451d559f265d46fd0a2adae9cdb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shNIPBL-3_EtOH.d5def451d559f265d46fd0a2adae9cdb.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shNIPBL-3_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shNIPBL-3_EtOH_Rep1/POL2-ser2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shNIPBL-3_EtOH/POL2-ser2_shNIPBL-3_EtOH \
  >& peak_call/POL2-ser2_shNIPBL-3_EtOH/POL2-ser2_shNIPBL-3_EtOH.diag.macs.out
macs2_callpeak.POL2-ser2_shNIPBL-3_EtOH.d5def451d559f265d46fd0a2adae9cdb.mugqic.done
)
macs2_callpeak_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_5_JOB_ID: macs2_callpeak.POL2-ser2_shNIPBL_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shNIPBL_EtOH
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shNIPBL_EtOH.9bbdc0832284cf0c52346f88dd103a4f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shNIPBL_EtOH.9bbdc0832284cf0c52346f88dd103a4f.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shNIPBL_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shNIPBL-3_EtOH_Rep1/POL2-ser2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  alignment/POL2-ser2_shNIPBL-5_EtOH_Rep1/POL2-ser2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  alignment/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shNIPBL_EtOH/POL2-ser2_shNIPBL_EtOH \
  >& peak_call/POL2-ser2_shNIPBL_EtOH/POL2-ser2_shNIPBL_EtOH.diag.macs.out
macs2_callpeak.POL2-ser2_shNIPBL_EtOH.9bbdc0832284cf0c52346f88dd103a4f.mugqic.done
)
macs2_callpeak_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_6_JOB_ID: macs2_callpeak_report
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_report
JOB_DEPENDENCIES=$macs2_callpeak_1_JOB_ID:$macs2_callpeak_2_JOB_ID:$macs2_callpeak_3_JOB_ID:$macs2_callpeak_4_JOB_ID:$macs2_callpeak_5_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_report.0dc265f315ad60373ac2773cf09e4052.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_report.0dc265f315ad60373ac2773cf09e4052.mugqic.done'
mkdir -p report && \
cp /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.macs2_callpeak.md report/ && \
for contrast in POL2_shCRTL-1_Dex POL2_shCTRL-2_Dex POL2_shCRTL_Dex POL2_shCRTL-1_EtOH POL2_shCTRL-2_EtOH POL2_shCTRL_EtOH POL2_shNIPBL-3_Dex POL2_shNIPBL-5_Dex POL2_shNIPBL_Dex POL2_shNIPBL-3_EtOH POL2_shNIPBL-5_EtOH POL2_shNIPBL_EtOH POL2-ser2_shCRTL-1_Dex POL2-ser2_shCTRL-2_Dex POL2-ser2_shCRTL_Dex POL2-ser2_shCRTL-1_EtOH POL2-ser2_shCTRL-2_EtOH POL2-ser2_shCTRL_EtOH POL2-ser2_shNIPBL-3_Dex POL2-ser2_shNIPBL-5_Dex POL2-ser2_shNIPBL_Dex POL2-ser2_shNIPBL-3_EtOH POL2-ser2_shNIPBL-5_EtOH POL2-ser2_shNIPBL_EtOH
do
  cp -a --parents peak_call/$contrast/ report/ && \
  echo -e "* [Peak Calls File for Design $contrast](peak_call/$contrast/${contrast}_peaks.xls)" \
  >> report/ChipSeq.macs2_callpeak.md
done
macs2_callpeak_report.0dc265f315ad60373ac2773cf09e4052.mugqic.done
)
macs2_callpeak_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# Call home with pipeline statistics
#-------------------------------------------------------------------------------
wget "http://mugqic.hpc.mcgill.ca/cgi-bin/pipeline.cgi?hostname=lg-1r17-n02&ip=10.241.129.12&pipeline=ChipSeq&steps=picard_sam_to_fastq,trimmomatic,merge_trimmomatic_stats,bwa_mem_picard_sort_sam,samtools_view_filter,picard_merge_sam_files,picard_mark_duplicates,metrics,homer_make_tag_directory,qc_metrics,homer_make_ucsc_file,macs2_callpeak&samples=30" --quiet --output-document=/dev/null

