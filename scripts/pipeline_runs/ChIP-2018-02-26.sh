#!/bin/bash
# Exit immediately on error
set -eu -o pipefail

#-------------------------------------------------------------------------------
# ChipSeq PBSScheduler Job Submission Bash script
# Version: 2.2.0
# Created on: 2018-02-26T18:53:48
# Steps:
#   picard_sam_to_fastq: 0 job... skipping
#   trimmomatic: 1 job
#   merge_trimmomatic_stats: 1 job
#   bwa_mem_picard_sort_sam: 2 jobs
#   samtools_view_filter: 5 jobs
#   picard_merge_sam_files: 6 jobs
#   picard_mark_duplicates: 9 jobs
#   metrics: 2 jobs
#   homer_make_tag_directory: 30 jobs
#   qc_metrics: 1 job
#   homer_make_ucsc_file: 31 jobs
#   macs2_callpeak: 24 jobs
#   TOTAL: 112 jobs
#-------------------------------------------------------------------------------

OUTPUT_DIR=/gs/scratch/efournier/A549_Michele/output/chip-pipeline-GRCh38
JOB_OUTPUT_DIR=$OUTPUT_DIR/job_output
TIMESTAMP=`date +%FT%H.%M.%S`
JOB_LIST=$JOB_OUTPUT_DIR/ChipSeq_job_list_$TIMESTAMP
mkdir -p $OUTPUT_DIR
cd $OUTPUT_DIR


#-------------------------------------------------------------------------------
# STEP: trimmomatic
#-------------------------------------------------------------------------------
STEP=trimmomatic
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: trimmomatic_1_JOB_ID: trimmomatic.HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1.9b715fc8b02f63e79776217b71c3299f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1.9b715fc8b02f63e79776217b71c3299f.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/WCE_shCTRL-1_EtOH_Rep1 && \
`cat > trim/WCE_shCTRL-1_EtOH_Rep1/HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 6 \
  -phred33 \
  /gs/scratch/efournier/A549_Michele/raw/HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1.fastq.gz \
  trim/WCE_shCTRL-1_EtOH_Rep1/HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1.trim.single.fastq.gz \
  ILLUMINACLIP:trim/WCE_shCTRL-1_EtOH_Rep1/HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/WCE_shCTRL-1_EtOH_Rep1/HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1.trim.log
trimmomatic.HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1.9b715fc8b02f63e79776217b71c3299f.mugqic.done
)
trimmomatic_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=6 | grep "[0-9]")
echo "$trimmomatic_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: merge_trimmomatic_stats
#-------------------------------------------------------------------------------
STEP=merge_trimmomatic_stats
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: merge_trimmomatic_stats_1_JOB_ID: merge_trimmomatic_stats
#-------------------------------------------------------------------------------
JOB_NAME=merge_trimmomatic_stats
JOB_DEPENDENCIES=$trimmomatic_1_JOB_ID
JOB_DONE=job_output/merge_trimmomatic_stats/merge_trimmomatic_stats.adbaf548ca254b62bc6757ffd2dc2df6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'merge_trimmomatic_stats.adbaf548ca254b62bc6757ffd2dc2df6.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p metrics && \
echo 'Sample	Readset	Raw Single Reads #	Surviving Single Reads #	Surviving Single Reads %' > metrics/trimReadsetTable.tsv && \
grep ^Input trim/POL2_shCRTL-1_Dex_Rep1/HI.4552.002.Index_10.A549_ChIP_POL2_shCRTL-1_Dex_100nM_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/POL2_shCRTL-1_Dex_Rep1	HI.4552.002.Index_10.A549_ChIP_POL2_shCRTL-1_Dex_100nM_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/POL2_shCTRL-2_Dex_Rep1/HI.4552.002.Index_11.A549_ChIP_POL2_shCTRL-2_Dex_100nM_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/POL2_shCTRL-2_Dex_Rep1	HI.4552.002.Index_11.A549_ChIP_POL2_shCTRL-2_Dex_100nM_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/WCE_shCTRL-2_EtOH_Rep1/HI.4552.002.Index_13.A549_ChIP_WCE_shCTRL-2_EtOH_0-01_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/WCE_shCTRL-2_EtOH_Rep1	HI.4552.002.Index_13.A549_ChIP_WCE_shCTRL-2_EtOH_0-01_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/POL2_shNIPBL-3_EtOH_Rep1/HI.4552.002.Index_14.A549_ChIP_POL2_shNIPBL-3_EtOH_0-01_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/POL2_shNIPBL-3_EtOH_Rep1	HI.4552.002.Index_14.A549_ChIP_POL2_shNIPBL-3_EtOH_0-01_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/WCE_shNIPBL-5_EtOH_Rep1/HI.4552.002.Index_15.A549_ChIP_WCE_shNIPBL-5_EtOH_0-01_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/WCE_shNIPBL-5_EtOH_Rep1	HI.4552.002.Index_15.A549_ChIP_WCE_shNIPBL-5_EtOH_0-01_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/POL2_shNIPBL-5_EtOH_Rep1/HI.4552.002.Index_16.A549_ChIP_POL2_shNIPBL-5_EtOH_0-01_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/POL2_shNIPBL-5_EtOH_Rep1	HI.4552.002.Index_16.A549_ChIP_POL2_shNIPBL-5_EtOH_0-01_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/POL2_shCTRL-2_EtOH_Rep1/HI.4552.002.Index_18.A549_ChIP_POL2_shCTRL-2_EtOH_0-01_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/POL2_shCTRL-2_EtOH_Rep1	HI.4552.002.Index_18.A549_ChIP_POL2_shCTRL-2_EtOH_0-01_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/WCE_shCRTL-1_Dex_Rep1/HI.4552.002.Index_1.A549_ChIP_WCE_shCRTL-1_Dex_100nM_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/WCE_shCRTL-1_Dex_Rep1	HI.4552.002.Index_1.A549_ChIP_WCE_shCRTL-1_Dex_100nM_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/POL2_shNIPBL-3_Dex_Rep1/HI.4552.002.Index_20.A549_ChIP_POL2_shNIPBL-3_Dex_100nM_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/POL2_shNIPBL-3_Dex_Rep1	HI.4552.002.Index_20.A549_ChIP_POL2_shNIPBL-3_Dex_100nM_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/POL2_shNIPBL-5_Dex_Rep1/HI.4552.002.Index_22.A549_ChIP_POL2_shNIPBL-5_Dex_100nM_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/POL2_shNIPBL-5_Dex_Rep1	HI.4552.002.Index_22.A549_ChIP_POL2_shNIPBL-5_Dex_100nM_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/WCE_shCTRL-1_EtOH_Rep1/HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/WCE_shCTRL-1_EtOH_Rep1	HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/WCE_shCTRL-2_Dex_Rep1/HI.4552.002.Index_3.A549_ChIP_WCE_shCTRL-2_Dex_100nM_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/WCE_shCTRL-2_Dex_Rep1	HI.4552.002.Index_3.A549_ChIP_WCE_shCTRL-2_Dex_100nM_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/WCE_shNIPBL-3_EtOH_Rep1/HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/WCE_shNIPBL-3_EtOH_Rep1	HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/POL2_shCRTL-1_EtOH_Rep1/HI.4552.002.Index_7.A549_ChIP_POL2_shCRTL-1_EtOH_0-01_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/POL2_shCRTL-1_EtOH_Rep1	HI.4552.002.Index_7.A549_ChIP_POL2_shCRTL-1_EtOH_0-01_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/WCE_shNIPBL-5_Dex_Rep1/HI.4552.002.Index_8.A549_ChIP_WCE_shNIPBL-5_Dex_100nM_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/WCE_shNIPBL-5_Dex_Rep1	HI.4552.002.Index_8.A549_ChIP_WCE_shNIPBL-5_Dex_100nM_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/WCE_shNIPBL-3_Dex_Rep1/HI.4552.002.Index_9.A549_ChIP_WCE_shNIPBL-3_Dex_100nM_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/WCE_shNIPBL-3_Dex_Rep1	HI.4552.002.Index_9.A549_ChIP_WCE_shNIPBL-3_Dex_100nM_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/POL2-ser2_shNIPBL-3_EtOH_Rep1/HI.4552.003.Index_12.A549_ChIP_POL2-ser2_shNIPBL-3_EtOH_0-01_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/POL2-ser2_shNIPBL-3_EtOH_Rep1	HI.4552.003.Index_12.A549_ChIP_POL2-ser2_shNIPBL-3_EtOH_0-01_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/BRD4_NA_EtOH_Rep2/HI.4552.003.Index_13.A549_ChIP_BRD4_NA_EtOH_0-01_1h_Rep2_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/BRD4_NA_EtOH_Rep2	HI.4552.003.Index_13.A549_ChIP_BRD4_NA_EtOH_0-01_1h_Rep2_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/WCE_NA_Dex_Rep2/HI.4552.003.Index_15.A549_ChIP_WCE_NA_Dex_100nM_1h_Rep2_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/WCE_NA_Dex_Rep2	HI.4552.003.Index_15.A549_ChIP_WCE_NA_Dex_100nM_1h_Rep2_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/CDK9_NA_Dex_Rep2/HI.4552.003.Index_18.A549_ChIP_CDK9_NA_Dex_100nM_1h_Rep2_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/CDK9_NA_Dex_Rep2	HI.4552.003.Index_18.A549_ChIP_CDK9_NA_Dex_100nM_1h_Rep2_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/POL2-ser2_shNIPBL-5_EtOH_Rep1/HI.4552.003.Index_19.A549_ChIP_POL2-ser2_shNIPBL-5_EtOH_0-01_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/POL2-ser2_shNIPBL-5_EtOH_Rep1	HI.4552.003.Index_19.A549_ChIP_POL2-ser2_shNIPBL-5_EtOH_0-01_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/POL2-ser2_shCTRL-2_Dex_Rep1/HI.4552.003.Index_21.A549_ChIP_POL2-ser2_shCTRL-2_Dex_100nM_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/POL2-ser2_shCTRL-2_Dex_Rep1	HI.4552.003.Index_21.A549_ChIP_POL2-ser2_shCTRL-2_Dex_100nM_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/POL2-ser2_shNIPBL-3_Dex_Rep1/HI.4552.003.Index_23.A549_ChIP_POL2-ser2_shNIPBL-3_Dex_100nM_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/POL2-ser2_shNIPBL-3_Dex_Rep1	HI.4552.003.Index_23.A549_ChIP_POL2-ser2_shNIPBL-3_Dex_100nM_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/POL2-ser2_shCRTL-1_Dex_Rep1/HI.4552.003.Index_25.A549_ChIP_POL2-ser2_shCRTL-1_Dex_100nM_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/POL2-ser2_shCRTL-1_Dex_Rep1	HI.4552.003.Index_25.A549_ChIP_POL2-ser2_shCRTL-1_Dex_100nM_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/POL2-ser2_shNIPBL-5_Dex_Rep1/HI.4552.003.Index_27.A549_ChIP_POL2-ser2_shNIPBL-5_Dex_100nM_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/POL2-ser2_shNIPBL-5_Dex_Rep1	HI.4552.003.Index_27.A549_ChIP_POL2-ser2_shNIPBL-5_Dex_100nM_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/WCE_NA_EtOH_Rep2/HI.4552.003.Index_2.A549_ChIP_WCE_NA_EtOH_0-01_1h_Rep2_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/WCE_NA_EtOH_Rep2	HI.4552.003.Index_2.A549_ChIP_WCE_NA_EtOH_0-01_1h_Rep2_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/POL2-ser2_shCRTL-1_EtOH_Rep1/HI.4552.003.Index_4.A549_ChIP_POL2-ser2_shCRTL-1_EtOH_0-01_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/POL2-ser2_shCRTL-1_EtOH_Rep1	HI.4552.003.Index_4.A549_ChIP_POL2-ser2_shCRTL-1_EtOH_0-01_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/POL2-ser2_shCTRL-2_EtOH_Rep1/HI.4552.003.Index_5.A549_ChIP_POL2-ser2_shCTRL-2_EtOH_0-01_1h_Rep1_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/POL2-ser2_shCTRL-2_EtOH_Rep1	HI.4552.003.Index_5.A549_ChIP_POL2-ser2_shCTRL-2_EtOH_0-01_1h_Rep1_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/CDK9_NA_EtOH_Rep2/HI.4552.003.Index_6.A549_ChIP_CDK9_NA_EtOH_0-01_1h_Rep2_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/CDK9_NA_EtOH_Rep2	HI.4552.003.Index_6.A549_ChIP_CDK9_NA_EtOH_0-01_1h_Rep2_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/BRD4_NA_Dex_Rep2/HI.4552.003.Index_7.A549_ChIP_BRD4_NA_Dex_100nM_1h_Rep2_R1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/BRD4_NA_Dex_Rep2	HI.4552.003.Index_7.A549_ChIP_BRD4_NA_Dex_100nM_1h_Rep2_R1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
cut -f1,3- metrics/trimReadsetTable.tsv | awk -F"	" '{OFS="	"; if (NR==1) {if ($2=="Raw Paired Reads #") {paired=1};print "Sample", "Raw Reads #", "Surviving Reads #", "Surviving %"} else {if (paired) {$2=$2*2; $3=$3*2}; raw[$1]+=$2; surviving[$1]+=$3}}END{for (sample in raw){print sample, raw[sample], surviving[sample], surviving[sample] / raw[sample] * 100}}' \
  > metrics/trimSampleTable.tsv && \
mkdir -p report && \
cp metrics/trimReadsetTable.tsv metrics/trimSampleTable.tsv report/ && \
trim_readset_table_md=`LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----|-----:|-----:|-----:"} else {print $1, $2, sprintf("%\47d", $3), sprintf("%\47d", $4), sprintf("%.1f", $5)}}' metrics/trimReadsetTable.tsv` && \
pandoc \
  /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/Illumina.merge_trimmomatic_stats.md \
  --template /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/Illumina.merge_trimmomatic_stats.md \
  --variable trailing_min_quality=30 \
  --variable min_length=50 \
  --variable read_type=Single \
  --variable trim_readset_table="$trim_readset_table_md" \
  --to markdown \
  > report/Illumina.merge_trimmomatic_stats.md
merge_trimmomatic_stats.adbaf548ca254b62bc6757ffd2dc2df6.mugqic.done
)
merge_trimmomatic_stats_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$merge_trimmomatic_stats_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: bwa_mem_picard_sort_sam
#-------------------------------------------------------------------------------
STEP=bwa_mem_picard_sort_sam
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_1_JOB_ID: bwa_mem_picard_sort_sam.HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1
JOB_DEPENDENCIES=$trimmomatic_1_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1.7e2d4f56a94c22562d58972d267244ec.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1.7e2d4f56a94c22562d58972d267244ec.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p alignment/WCE_shCTRL-1_EtOH_Rep1/HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1 && \
bwa mem  \
  -M -t 7 \
  -R '@RG	ID:HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1	SM:WCE_shCTRL-1_EtOH_Rep1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/bwa_index/Homo_sapiens.GRCh38.fa \
  trim/WCE_shCTRL-1_EtOH_Rep1/HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1.trim.single.fastq.gz | \
java -Djava.io.tmpdir=/localscratch/ -XX:ParallelGCThreads=1 -Dsamjdk.buffer_size=4194304 -Xmx15G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/localscratch/ \
  INPUT=/dev/stdin \
  OUTPUT=alignment/WCE_shCTRL-1_EtOH_Rep1/HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1/HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1.sorted.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=3750000
bwa_mem_picard_sort_sam.HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1.7e2d4f56a94c22562d58972d267244ec.mugqic.done
)
bwa_mem_picard_sort_sam_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bwa_mem_picard_sort_sam_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_2_JOB_ID: bwa_mem_picard_sort_sam_report
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam_report
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_1_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam_report.b10d0f7eacf0b8b59354726b32e5aa9c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam_report.b10d0f7eacf0b8b59354726b32e5aa9c.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p report && \
pandoc --to=markdown \
  --template /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/DnaSeq.bwa_mem_picard_sort_sam.md \
  --variable scientific_name="Homo_sapiens" \
  --variable assembly="GRCh38" \
  /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/DnaSeq.bwa_mem_picard_sort_sam.md \
  > report/DnaSeq.bwa_mem_picard_sort_sam.md
bwa_mem_picard_sort_sam_report.b10d0f7eacf0b8b59354726b32e5aa9c.mugqic.done
)
bwa_mem_picard_sort_sam_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bwa_mem_picard_sort_sam_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: samtools_view_filter
#-------------------------------------------------------------------------------
STEP=samtools_view_filter
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_1_JOB_ID: samtools_view_filter.HI.4552.002.Index_1.A549_ChIP_WCE_shCRTL-1_Dex_100nM_1h_Rep1_R1
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4552.002.Index_1.A549_ChIP_WCE_shCRTL-1_Dex_100nM_1h_Rep1_R1
JOB_DEPENDENCIES=
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4552.002.Index_1.A549_ChIP_WCE_shCRTL-1_Dex_100nM_1h_Rep1_R1.aba432cf1786833ddae955ef3c79d860.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4552.002.Index_1.A549_ChIP_WCE_shCRTL-1_Dex_100nM_1h_Rep1_R1.aba432cf1786833ddae955ef3c79d860.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -b -F4 -q 20 \
  alignment/WCE_shCRTL-1_Dex_Rep1/HI.4552.002.Index_1.A549_ChIP_WCE_shCRTL-1_Dex_100nM_1h_Rep1_R1/HI.4552.002.Index_1.A549_ChIP_WCE_shCRTL-1_Dex_100nM_1h_Rep1_R1.sorted.bam \
  > alignment/WCE_shCRTL-1_Dex_Rep1/HI.4552.002.Index_1.A549_ChIP_WCE_shCRTL-1_Dex_100nM_1h_Rep1_R1/HI.4552.002.Index_1.A549_ChIP_WCE_shCRTL-1_Dex_100nM_1h_Rep1_R1.sorted.filtered.bam
samtools_view_filter.HI.4552.002.Index_1.A549_ChIP_WCE_shCRTL-1_Dex_100nM_1h_Rep1_R1.aba432cf1786833ddae955ef3c79d860.mugqic.done
)
samtools_view_filter_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$samtools_view_filter_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_2_JOB_ID: samtools_view_filter.HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_1_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1.1fc04e3ada1c488d78c01b9a160940a2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1.1fc04e3ada1c488d78c01b9a160940a2.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -b -F4 -q 20 \
  alignment/WCE_shCTRL-1_EtOH_Rep1/HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1/HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1.sorted.bam \
  > alignment/WCE_shCTRL-1_EtOH_Rep1/HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1/HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1.sorted.filtered.bam
samtools_view_filter.HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1.1fc04e3ada1c488d78c01b9a160940a2.mugqic.done
)
samtools_view_filter_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$samtools_view_filter_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_3_JOB_ID: samtools_view_filter.HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1
JOB_DEPENDENCIES=
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1.622b22fa2d6019a30fa670ceea179314.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1.622b22fa2d6019a30fa670ceea179314.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -b -F4 -q 20 \
  alignment/WCE_shNIPBL-3_EtOH_Rep1/HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1/HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1.sorted.bam \
  > alignment/WCE_shNIPBL-3_EtOH_Rep1/HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1/HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1.sorted.filtered.bam
samtools_view_filter.HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1.622b22fa2d6019a30fa670ceea179314.mugqic.done
)
samtools_view_filter_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$samtools_view_filter_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_4_JOB_ID: samtools_view_filter.HI.4552.003.Index_5.A549_ChIP_POL2-ser2_shCTRL-2_EtOH_0-01_1h_Rep1_R1
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4552.003.Index_5.A549_ChIP_POL2-ser2_shCTRL-2_EtOH_0-01_1h_Rep1_R1
JOB_DEPENDENCIES=
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4552.003.Index_5.A549_ChIP_POL2-ser2_shCTRL-2_EtOH_0-01_1h_Rep1_R1.3329ed942f0ad28da22dc8235264c901.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4552.003.Index_5.A549_ChIP_POL2-ser2_shCTRL-2_EtOH_0-01_1h_Rep1_R1.3329ed942f0ad28da22dc8235264c901.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -b -F4 -q 20 \
  alignment/POL2-ser2_shCTRL-2_EtOH_Rep1/HI.4552.003.Index_5.A549_ChIP_POL2-ser2_shCTRL-2_EtOH_0-01_1h_Rep1_R1/HI.4552.003.Index_5.A549_ChIP_POL2-ser2_shCTRL-2_EtOH_0-01_1h_Rep1_R1.sorted.bam \
  > alignment/POL2-ser2_shCTRL-2_EtOH_Rep1/HI.4552.003.Index_5.A549_ChIP_POL2-ser2_shCTRL-2_EtOH_0-01_1h_Rep1_R1/HI.4552.003.Index_5.A549_ChIP_POL2-ser2_shCTRL-2_EtOH_0-01_1h_Rep1_R1.sorted.filtered.bam
samtools_view_filter.HI.4552.003.Index_5.A549_ChIP_POL2-ser2_shCTRL-2_EtOH_0-01_1h_Rep1_R1.3329ed942f0ad28da22dc8235264c901.mugqic.done
)
samtools_view_filter_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$samtools_view_filter_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_5_JOB_ID: samtools_view_filter_report
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter_report
JOB_DEPENDENCIES=$samtools_view_filter_1_JOB_ID:$samtools_view_filter_2_JOB_ID:$samtools_view_filter_3_JOB_ID:$samtools_view_filter_4_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter_report.b5cac64c2a4ab579900138ddfbed49a6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter_report.b5cac64c2a4ab579900138ddfbed49a6.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p report && \
pandoc --to=markdown \
  --template /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.samtools_view_filter.md \
  --variable min_mapq="20" \
  /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.samtools_view_filter.md \
  > report/ChipSeq.samtools_view_filter.md
samtools_view_filter_report.b5cac64c2a4ab579900138ddfbed49a6.mugqic.done
)
samtools_view_filter_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$samtools_view_filter_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: picard_merge_sam_files
#-------------------------------------------------------------------------------
STEP=picard_merge_sam_files
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_1_JOB_ID: symlink_readset_sample_bam.WCE_shNIPBL-5_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.WCE_shNIPBL-5_EtOH_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.WCE_shNIPBL-5_EtOH_Rep1.50802e163700219cfeea503c518ce106.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.WCE_shNIPBL-5_EtOH_Rep1.50802e163700219cfeea503c518ce106.mugqic.done'
mkdir -p alignment/WCE_shNIPBL-5_EtOH_Rep1 && \
ln -s -f HI.4552.002.Index_15.A549_ChIP_WCE_shNIPBL-5_EtOH_0-01_1h_Rep1_R1/HI.4552.002.Index_15.A549_ChIP_WCE_shNIPBL-5_EtOH_0-01_1h_Rep1_R1.sorted.filtered.bam alignment/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.merged.bam
symlink_readset_sample_bam.WCE_shNIPBL-5_EtOH_Rep1.50802e163700219cfeea503c518ce106.mugqic.done
)
picard_merge_sam_files_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$picard_merge_sam_files_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_2_JOB_ID: symlink_readset_sample_bam.WCE_shCRTL-1_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.WCE_shCRTL-1_Dex_Rep1
JOB_DEPENDENCIES=$samtools_view_filter_1_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.WCE_shCRTL-1_Dex_Rep1.4a2224639b398975c3d9a63102090f1f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.WCE_shCRTL-1_Dex_Rep1.4a2224639b398975c3d9a63102090f1f.mugqic.done'
mkdir -p alignment/WCE_shCRTL-1_Dex_Rep1 && \
ln -s -f HI.4552.002.Index_1.A549_ChIP_WCE_shCRTL-1_Dex_100nM_1h_Rep1_R1/HI.4552.002.Index_1.A549_ChIP_WCE_shCRTL-1_Dex_100nM_1h_Rep1_R1.sorted.filtered.bam alignment/WCE_shCRTL-1_Dex_Rep1/WCE_shCRTL-1_Dex_Rep1.merged.bam
symlink_readset_sample_bam.WCE_shCRTL-1_Dex_Rep1.4a2224639b398975c3d9a63102090f1f.mugqic.done
)
picard_merge_sam_files_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_merge_sam_files_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_3_JOB_ID: symlink_readset_sample_bam.WCE_shCTRL-1_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.WCE_shCTRL-1_EtOH_Rep1
JOB_DEPENDENCIES=$samtools_view_filter_2_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.WCE_shCTRL-1_EtOH_Rep1.bb135c7154757355ca74f4f48673a8e8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.WCE_shCTRL-1_EtOH_Rep1.bb135c7154757355ca74f4f48673a8e8.mugqic.done'
mkdir -p alignment/WCE_shCTRL-1_EtOH_Rep1 && \
ln -s -f HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1/HI.4552.002.Index_2.A549_ChIP_WCE_shCTRL-1_EtOH_0-01_1h_Rep1_R1.sorted.filtered.bam alignment/WCE_shCTRL-1_EtOH_Rep1/WCE_shCTRL-1_EtOH_Rep1.merged.bam
symlink_readset_sample_bam.WCE_shCTRL-1_EtOH_Rep1.bb135c7154757355ca74f4f48673a8e8.mugqic.done
)
picard_merge_sam_files_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_merge_sam_files_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_4_JOB_ID: symlink_readset_sample_bam.WCE_shCTRL-2_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.WCE_shCTRL-2_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.WCE_shCTRL-2_Dex_Rep1.eda24bcde88428c4a4cdb7e889a5db88.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.WCE_shCTRL-2_Dex_Rep1.eda24bcde88428c4a4cdb7e889a5db88.mugqic.done'
mkdir -p alignment/WCE_shCTRL-2_Dex_Rep1 && \
ln -s -f HI.4552.002.Index_3.A549_ChIP_WCE_shCTRL-2_Dex_100nM_1h_Rep1_R1/HI.4552.002.Index_3.A549_ChIP_WCE_shCTRL-2_Dex_100nM_1h_Rep1_R1.sorted.filtered.bam alignment/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.merged.bam
symlink_readset_sample_bam.WCE_shCTRL-2_Dex_Rep1.eda24bcde88428c4a4cdb7e889a5db88.mugqic.done
)
picard_merge_sam_files_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$picard_merge_sam_files_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_5_JOB_ID: symlink_readset_sample_bam.WCE_shNIPBL-3_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.WCE_shNIPBL-3_EtOH_Rep1
JOB_DEPENDENCIES=$samtools_view_filter_3_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.WCE_shNIPBL-3_EtOH_Rep1.b6e6416c5715b70a16e3f524a86d5dc1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.WCE_shNIPBL-3_EtOH_Rep1.b6e6416c5715b70a16e3f524a86d5dc1.mugqic.done'
mkdir -p alignment/WCE_shNIPBL-3_EtOH_Rep1 && \
ln -s -f HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1/HI.4552.002.Index_6.A549_ChIP_WCE_shNIPBL-3_EtOH_0-01_1h_Rep1_R1.sorted.filtered.bam alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.merged.bam
symlink_readset_sample_bam.WCE_shNIPBL-3_EtOH_Rep1.b6e6416c5715b70a16e3f524a86d5dc1.mugqic.done
)
picard_merge_sam_files_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_merge_sam_files_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_6_JOB_ID: symlink_readset_sample_bam.POL2-ser2_shCTRL-2_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.POL2-ser2_shCTRL-2_EtOH_Rep1
JOB_DEPENDENCIES=$samtools_view_filter_4_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.POL2-ser2_shCTRL-2_EtOH_Rep1.8f0e7a8bc304b59cf3248e289b15c3f2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.POL2-ser2_shCTRL-2_EtOH_Rep1.8f0e7a8bc304b59cf3248e289b15c3f2.mugqic.done'
mkdir -p alignment/POL2-ser2_shCTRL-2_EtOH_Rep1 && \
ln -s -f HI.4552.003.Index_5.A549_ChIP_POL2-ser2_shCTRL-2_EtOH_0-01_1h_Rep1_R1/HI.4552.003.Index_5.A549_ChIP_POL2-ser2_shCTRL-2_EtOH_0-01_1h_Rep1_R1.sorted.filtered.bam alignment/POL2-ser2_shCTRL-2_EtOH_Rep1/POL2-ser2_shCTRL-2_EtOH_Rep1.merged.bam
symlink_readset_sample_bam.POL2-ser2_shCTRL-2_EtOH_Rep1.8f0e7a8bc304b59cf3248e289b15c3f2.mugqic.done
)
picard_merge_sam_files_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_merge_sam_files_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: picard_mark_duplicates
#-------------------------------------------------------------------------------
STEP=picard_mark_duplicates
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_1_JOB_ID: picard_mark_duplicates.WCE_shNIPBL-5_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.WCE_shNIPBL-5_EtOH_Rep1
JOB_DEPENDENCIES=$picard_merge_sam_files_1_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.WCE_shNIPBL-5_EtOH_Rep1.7f0b6ae999a18f07b15c4f02553dabe1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.WCE_shNIPBL-5_EtOH_Rep1.7f0b6ae999a18f07b15c4f02553dabe1.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/localscratch/ -XX:ParallelGCThreads=1 -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/localscratch/ \
  INPUT=alignment/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.merged.bam \
  OUTPUT=alignment/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  METRICS_FILE=alignment/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.sorted.dup.metrics \
  MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.WCE_shNIPBL-5_EtOH_Rep1.7f0b6ae999a18f07b15c4f02553dabe1.mugqic.done
)
picard_mark_duplicates_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=2 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_2_JOB_ID: picard_mark_duplicates.WCE_shCRTL-1_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.WCE_shCRTL-1_Dex_Rep1
JOB_DEPENDENCIES=$picard_merge_sam_files_2_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.WCE_shCRTL-1_Dex_Rep1.9c959bfa82db70510bdb31118819fcab.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.WCE_shCRTL-1_Dex_Rep1.9c959bfa82db70510bdb31118819fcab.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/localscratch/ -XX:ParallelGCThreads=1 -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/localscratch/ \
  INPUT=alignment/WCE_shCRTL-1_Dex_Rep1/WCE_shCRTL-1_Dex_Rep1.merged.bam \
  OUTPUT=alignment/WCE_shCRTL-1_Dex_Rep1/WCE_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  METRICS_FILE=alignment/WCE_shCRTL-1_Dex_Rep1/WCE_shCRTL-1_Dex_Rep1.sorted.dup.metrics \
  MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.WCE_shCRTL-1_Dex_Rep1.9c959bfa82db70510bdb31118819fcab.mugqic.done
)
picard_mark_duplicates_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=2 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_3_JOB_ID: picard_mark_duplicates.WCE_shCTRL-1_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.WCE_shCTRL-1_EtOH_Rep1
JOB_DEPENDENCIES=$picard_merge_sam_files_3_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.WCE_shCTRL-1_EtOH_Rep1.e9a3ff180fa1b132d5492458cfd80f76.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.WCE_shCTRL-1_EtOH_Rep1.e9a3ff180fa1b132d5492458cfd80f76.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/localscratch/ -XX:ParallelGCThreads=1 -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/localscratch/ \
  INPUT=alignment/WCE_shCTRL-1_EtOH_Rep1/WCE_shCTRL-1_EtOH_Rep1.merged.bam \
  OUTPUT=alignment/WCE_shCTRL-1_EtOH_Rep1/WCE_shCTRL-1_EtOH_Rep1.sorted.dup.bam \
  METRICS_FILE=alignment/WCE_shCTRL-1_EtOH_Rep1/WCE_shCTRL-1_EtOH_Rep1.sorted.dup.metrics \
  MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.WCE_shCTRL-1_EtOH_Rep1.e9a3ff180fa1b132d5492458cfd80f76.mugqic.done
)
picard_mark_duplicates_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=2 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_4_JOB_ID: picard_mark_duplicates.WCE_shCTRL-2_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.WCE_shCTRL-2_Dex_Rep1
JOB_DEPENDENCIES=$picard_merge_sam_files_4_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.WCE_shCTRL-2_Dex_Rep1.128e91c651b9cff15ed6a19fae08180c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.WCE_shCTRL-2_Dex_Rep1.128e91c651b9cff15ed6a19fae08180c.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/localscratch/ -XX:ParallelGCThreads=1 -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/localscratch/ \
  INPUT=alignment/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.merged.bam \
  OUTPUT=alignment/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  METRICS_FILE=alignment/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.sorted.dup.metrics \
  MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.WCE_shCTRL-2_Dex_Rep1.128e91c651b9cff15ed6a19fae08180c.mugqic.done
)
picard_mark_duplicates_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=2 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_5_JOB_ID: picard_mark_duplicates.WCE_shNIPBL-3_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.WCE_shNIPBL-3_EtOH_Rep1
JOB_DEPENDENCIES=$picard_merge_sam_files_5_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.WCE_shNIPBL-3_EtOH_Rep1.6bcd0686f9bf1bc436a4c81aece7ea7f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.WCE_shNIPBL-3_EtOH_Rep1.6bcd0686f9bf1bc436a4c81aece7ea7f.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/localscratch/ -XX:ParallelGCThreads=1 -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/localscratch/ \
  INPUT=alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.merged.bam \
  OUTPUT=alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  METRICS_FILE=alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.metrics \
  MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.WCE_shNIPBL-3_EtOH_Rep1.6bcd0686f9bf1bc436a4c81aece7ea7f.mugqic.done
)
picard_mark_duplicates_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=2 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_6_JOB_ID: picard_mark_duplicates.WCE_shNIPBL-5_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.WCE_shNIPBL-5_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.WCE_shNIPBL-5_Dex_Rep1.c7fd38d8f896113256b2d3848bf3bb8b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.WCE_shNIPBL-5_Dex_Rep1.c7fd38d8f896113256b2d3848bf3bb8b.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/localscratch/ -XX:ParallelGCThreads=1 -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/localscratch/ \
  INPUT=alignment/WCE_shNIPBL-5_Dex_Rep1/WCE_shNIPBL-5_Dex_Rep1.merged.bam \
  OUTPUT=alignment/WCE_shNIPBL-5_Dex_Rep1/WCE_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  METRICS_FILE=alignment/WCE_shNIPBL-5_Dex_Rep1/WCE_shNIPBL-5_Dex_Rep1.sorted.dup.metrics \
  MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.WCE_shNIPBL-5_Dex_Rep1.c7fd38d8f896113256b2d3848bf3bb8b.mugqic.done
)
picard_mark_duplicates_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=2 | grep "[0-9]")
echo "$picard_mark_duplicates_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_7_JOB_ID: picard_mark_duplicates.WCE_shNIPBL-3_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.WCE_shNIPBL-3_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.WCE_shNIPBL-3_Dex_Rep1.b01d805977f58def40ee931938da2e8d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.WCE_shNIPBL-3_Dex_Rep1.b01d805977f58def40ee931938da2e8d.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/localscratch/ -XX:ParallelGCThreads=1 -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/localscratch/ \
  INPUT=alignment/WCE_shNIPBL-3_Dex_Rep1/WCE_shNIPBL-3_Dex_Rep1.merged.bam \
  OUTPUT=alignment/WCE_shNIPBL-3_Dex_Rep1/WCE_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  METRICS_FILE=alignment/WCE_shNIPBL-3_Dex_Rep1/WCE_shNIPBL-3_Dex_Rep1.sorted.dup.metrics \
  MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.WCE_shNIPBL-3_Dex_Rep1.b01d805977f58def40ee931938da2e8d.mugqic.done
)
picard_mark_duplicates_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=2 | grep "[0-9]")
echo "$picard_mark_duplicates_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_8_JOB_ID: picard_mark_duplicates.POL2-ser2_shCTRL-2_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.POL2-ser2_shCTRL-2_EtOH_Rep1
JOB_DEPENDENCIES=$picard_merge_sam_files_6_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.POL2-ser2_shCTRL-2_EtOH_Rep1.0cfc08dd0b3f3da625096dae7cbc8ce6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.POL2-ser2_shCTRL-2_EtOH_Rep1.0cfc08dd0b3f3da625096dae7cbc8ce6.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=/localscratch/ -XX:ParallelGCThreads=1 -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/localscratch/ \
  INPUT=alignment/POL2-ser2_shCTRL-2_EtOH_Rep1/POL2-ser2_shCTRL-2_EtOH_Rep1.merged.bam \
  OUTPUT=alignment/POL2-ser2_shCTRL-2_EtOH_Rep1/POL2-ser2_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  METRICS_FILE=alignment/POL2-ser2_shCTRL-2_EtOH_Rep1/POL2-ser2_shCTRL-2_EtOH_Rep1.sorted.dup.metrics \
  MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.POL2-ser2_shCTRL-2_EtOH_Rep1.0cfc08dd0b3f3da625096dae7cbc8ce6.mugqic.done
)
picard_mark_duplicates_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=48:00:0 -q metaq -l nodes=1:ppn=2 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_9_JOB_ID: picard_mark_duplicates_report
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates_report
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID:$picard_mark_duplicates_2_JOB_ID:$picard_mark_duplicates_3_JOB_ID:$picard_mark_duplicates_4_JOB_ID:$picard_mark_duplicates_5_JOB_ID:$picard_mark_duplicates_6_JOB_ID:$picard_mark_duplicates_7_JOB_ID:$picard_mark_duplicates_8_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates_report.c80ab57aaab7999422d809ae0583b4b5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates_report.c80ab57aaab7999422d809ae0583b4b5.mugqic.done'
mkdir -p report && \
cp \
  /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.picard_mark_duplicates.md \
  report/ChipSeq.picard_mark_duplicates.md
picard_mark_duplicates_report.c80ab57aaab7999422d809ae0583b4b5.mugqic.done
)
picard_mark_duplicates_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: metrics
#-------------------------------------------------------------------------------
STEP=metrics
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: metrics_1_JOB_ID: metrics.flagstat
#-------------------------------------------------------------------------------
JOB_NAME=metrics.flagstat
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID:$picard_mark_duplicates_2_JOB_ID:$picard_mark_duplicates_3_JOB_ID:$picard_mark_duplicates_4_JOB_ID:$picard_mark_duplicates_5_JOB_ID:$picard_mark_duplicates_6_JOB_ID:$picard_mark_duplicates_7_JOB_ID:$picard_mark_duplicates_8_JOB_ID
JOB_DONE=job_output/metrics/metrics.flagstat.96355fdef215f074305d48d156d6efa0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'metrics.flagstat.96355fdef215f074305d48d156d6efa0.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools flagstat \
  alignment/POL2_shCRTL-1_Dex_Rep1/POL2_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  > alignment/POL2_shCRTL-1_Dex_Rep1/POL2_shCRTL-1_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2_shCTRL-2_Dex_Rep1/POL2_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  > alignment/POL2_shCTRL-2_Dex_Rep1/POL2_shCTRL-2_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/WCE_shCTRL-2_EtOH_Rep1/WCE_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  > alignment/WCE_shCTRL-2_EtOH_Rep1/WCE_shCTRL-2_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2_shNIPBL-3_EtOH_Rep1/POL2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  > alignment/POL2_shNIPBL-3_EtOH_Rep1/POL2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  > alignment/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2_shNIPBL-5_EtOH_Rep1/POL2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  > alignment/POL2_shNIPBL-5_EtOH_Rep1/POL2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2_shCTRL-2_EtOH_Rep1/POL2_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  > alignment/POL2_shCTRL-2_EtOH_Rep1/POL2_shCTRL-2_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/WCE_shCRTL-1_Dex_Rep1/WCE_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  > alignment/WCE_shCRTL-1_Dex_Rep1/WCE_shCRTL-1_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2_shNIPBL-3_Dex_Rep1/POL2_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  > alignment/POL2_shNIPBL-3_Dex_Rep1/POL2_shNIPBL-3_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2_shNIPBL-5_Dex_Rep1/POL2_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  > alignment/POL2_shNIPBL-5_Dex_Rep1/POL2_shNIPBL-5_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/WCE_shCTRL-1_EtOH_Rep1/WCE_shCTRL-1_EtOH_Rep1.sorted.dup.bam \
  > alignment/WCE_shCTRL-1_EtOH_Rep1/WCE_shCTRL-1_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  > alignment/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  > alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2_shCRTL-1_EtOH_Rep1/POL2_shCRTL-1_EtOH_Rep1.sorted.dup.bam \
  > alignment/POL2_shCRTL-1_EtOH_Rep1/POL2_shCRTL-1_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/WCE_shNIPBL-5_Dex_Rep1/WCE_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  > alignment/WCE_shNIPBL-5_Dex_Rep1/WCE_shNIPBL-5_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/WCE_shNIPBL-3_Dex_Rep1/WCE_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  > alignment/WCE_shNIPBL-3_Dex_Rep1/WCE_shNIPBL-3_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2-ser2_shNIPBL-3_EtOH_Rep1/POL2-ser2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  > alignment/POL2-ser2_shNIPBL-3_EtOH_Rep1/POL2-ser2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/BRD4_NA_EtOH_Rep2/BRD4_NA_EtOH_Rep2.sorted.dup.bam \
  > alignment/BRD4_NA_EtOH_Rep2/BRD4_NA_EtOH_Rep2.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/WCE_NA_Dex_Rep2/WCE_NA_Dex_Rep2.sorted.dup.bam \
  > alignment/WCE_NA_Dex_Rep2/WCE_NA_Dex_Rep2.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/CDK9_NA_Dex_Rep2/CDK9_NA_Dex_Rep2.sorted.dup.bam \
  > alignment/CDK9_NA_Dex_Rep2/CDK9_NA_Dex_Rep2.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2-ser2_shNIPBL-5_EtOH_Rep1/POL2-ser2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  > alignment/POL2-ser2_shNIPBL-5_EtOH_Rep1/POL2-ser2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2-ser2_shCTRL-2_Dex_Rep1/POL2-ser2_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  > alignment/POL2-ser2_shCTRL-2_Dex_Rep1/POL2-ser2_shCTRL-2_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2-ser2_shNIPBL-3_Dex_Rep1/POL2-ser2_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  > alignment/POL2-ser2_shNIPBL-3_Dex_Rep1/POL2-ser2_shNIPBL-3_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2-ser2_shCRTL-1_Dex_Rep1/POL2-ser2_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  > alignment/POL2-ser2_shCRTL-1_Dex_Rep1/POL2-ser2_shCRTL-1_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2-ser2_shNIPBL-5_Dex_Rep1/POL2-ser2_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  > alignment/POL2-ser2_shNIPBL-5_Dex_Rep1/POL2-ser2_shNIPBL-5_Dex_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/WCE_NA_EtOH_Rep2/WCE_NA_EtOH_Rep2.sorted.dup.bam \
  > alignment/WCE_NA_EtOH_Rep2/WCE_NA_EtOH_Rep2.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2-ser2_shCRTL-1_EtOH_Rep1/POL2-ser2_shCRTL-1_EtOH_Rep1.sorted.dup.bam \
  > alignment/POL2-ser2_shCRTL-1_EtOH_Rep1/POL2-ser2_shCRTL-1_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/POL2-ser2_shCTRL-2_EtOH_Rep1/POL2-ser2_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  > alignment/POL2-ser2_shCTRL-2_EtOH_Rep1/POL2-ser2_shCTRL-2_EtOH_Rep1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/CDK9_NA_EtOH_Rep2/CDK9_NA_EtOH_Rep2.sorted.dup.bam \
  > alignment/CDK9_NA_EtOH_Rep2/CDK9_NA_EtOH_Rep2.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/BRD4_NA_Dex_Rep2/BRD4_NA_Dex_Rep2.sorted.dup.bam \
  > alignment/BRD4_NA_Dex_Rep2/BRD4_NA_Dex_Rep2.sorted.dup.bam.flagstat
metrics.flagstat.96355fdef215f074305d48d156d6efa0.mugqic.done
)
metrics_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$metrics_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: metrics_2_JOB_ID: metrics_report
#-------------------------------------------------------------------------------
JOB_NAME=metrics_report
JOB_DEPENDENCIES=$metrics_1_JOB_ID
JOB_DONE=job_output/metrics/metrics_report.505a0d8303c0b2fa1256b6cfe93c7f8a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'metrics_report.505a0d8303c0b2fa1256b6cfe93c7f8a.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
for sample in POL2_shCRTL-1_Dex_Rep1 POL2_shCTRL-2_Dex_Rep1 WCE_shCTRL-2_EtOH_Rep1 POL2_shNIPBL-3_EtOH_Rep1 WCE_shNIPBL-5_EtOH_Rep1 POL2_shNIPBL-5_EtOH_Rep1 POL2_shCTRL-2_EtOH_Rep1 WCE_shCRTL-1_Dex_Rep1 POL2_shNIPBL-3_Dex_Rep1 POL2_shNIPBL-5_Dex_Rep1 WCE_shCTRL-1_EtOH_Rep1 WCE_shCTRL-2_Dex_Rep1 WCE_shNIPBL-3_EtOH_Rep1 POL2_shCRTL-1_EtOH_Rep1 WCE_shNIPBL-5_Dex_Rep1 WCE_shNIPBL-3_Dex_Rep1 POL2-ser2_shNIPBL-3_EtOH_Rep1 BRD4_NA_EtOH_Rep2 WCE_NA_Dex_Rep2 CDK9_NA_Dex_Rep2 POL2-ser2_shNIPBL-5_EtOH_Rep1 POL2-ser2_shCTRL-2_Dex_Rep1 POL2-ser2_shNIPBL-3_Dex_Rep1 POL2-ser2_shCRTL-1_Dex_Rep1 POL2-ser2_shNIPBL-5_Dex_Rep1 WCE_NA_EtOH_Rep2 POL2-ser2_shCRTL-1_EtOH_Rep1 POL2-ser2_shCTRL-2_EtOH_Rep1 CDK9_NA_EtOH_Rep2 BRD4_NA_Dex_Rep2
do
  flagstat_file=alignment/$sample/$sample.sorted.dup.bam.flagstat
  echo -e "$sample	`grep -P '^\d+ \+ \d+ mapped' $flagstat_file | grep -Po '^\d+'`	`grep -P '^\d+ \+ \d+ duplicate' $flagstat_file | grep -Po '^\d+'`"
done | \
awk -F"	" '{OFS="	"; print $0, $3 / $2 * 100}' | sed '1iSample	Aligned Filtered Reads	Duplicate Reads	Duplicate %' \
  > metrics/SampleMetrics.stats && \
mkdir -p report && \
if [[ -f metrics/trimSampleTable.tsv ]]
then
  awk -F "	" 'FNR==NR{trim_line[$1]=$0; surviving[$1]=$3; next}{OFS="	"; if ($1=="Sample") {print trim_line[$1], $2, "Aligned Filtered %", $3, $4} else {print trim_line[$1], $2, $2 / surviving[$1] * 100, $3, $4}}' metrics/trimSampleTable.tsv metrics/SampleMetrics.stats \
  > report/trimMemSampleTable.tsv
else
  cp metrics/SampleMetrics.stats report/trimMemSampleTable.tsv
fi && \
trim_mem_sample_table=`if [[ -f metrics/trimSampleTable.tsv ]] ; then LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----:|-----:|-----:|-----:|-----:|-----:|-----:"} else {print $1, sprintf("%\47d", $2), sprintf("%\47d", $3), sprintf("%.1f", $4), sprintf("%\47d", $5), sprintf("%.1f", $6), sprintf("%\47d", $7), sprintf("%.1f", $8)}}' report/trimMemSampleTable.tsv ; else LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----:|-----:|-----:"} else {print $1, sprintf("%\47d", $2), sprintf("%\47d", $3), sprintf("%.1f", $4)}}' report/trimMemSampleTable.tsv ; fi` && \
pandoc --to=markdown \
  --template /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.metrics.md \
  --variable trim_mem_sample_table="$trim_mem_sample_table" \
  /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.metrics.md \
  > report/ChipSeq.metrics.md

metrics_report.505a0d8303c0b2fa1256b6cfe93c7f8a.mugqic.done
)
metrics_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$metrics_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: homer_make_tag_directory
#-------------------------------------------------------------------------------
STEP=homer_make_tag_directory
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_1_JOB_ID: homer_make_tag_directory.POL2_shCRTL-1_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2_shCRTL-1_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2_shCRTL-1_Dex_Rep1.ad205b089d8ff4ba09c028ca35b2544c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2_shCRTL-1_Dex_Rep1.ad205b089d8ff4ba09c028ca35b2544c.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2_shCRTL-1_Dex_Rep1 \
  alignment/POL2_shCRTL-1_Dex_Rep1/POL2_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2_shCRTL-1_Dex_Rep1.ad205b089d8ff4ba09c028ca35b2544c.mugqic.done
)
homer_make_tag_directory_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_2_JOB_ID: homer_make_tag_directory.POL2_shCTRL-2_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2_shCTRL-2_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2_shCTRL-2_Dex_Rep1.c5a5ae41f4a89ebed0d91e2691ceb227.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2_shCTRL-2_Dex_Rep1.c5a5ae41f4a89ebed0d91e2691ceb227.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2_shCTRL-2_Dex_Rep1 \
  alignment/POL2_shCTRL-2_Dex_Rep1/POL2_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2_shCTRL-2_Dex_Rep1.c5a5ae41f4a89ebed0d91e2691ceb227.mugqic.done
)
homer_make_tag_directory_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_3_JOB_ID: homer_make_tag_directory.WCE_shCTRL-2_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.WCE_shCTRL-2_EtOH_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.WCE_shCTRL-2_EtOH_Rep1.fa27febe95e428dfafcf3ab46703282d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.WCE_shCTRL-2_EtOH_Rep1.fa27febe95e428dfafcf3ab46703282d.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/WCE_shCTRL-2_EtOH_Rep1 \
  alignment/WCE_shCTRL-2_EtOH_Rep1/WCE_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.WCE_shCTRL-2_EtOH_Rep1.fa27febe95e428dfafcf3ab46703282d.mugqic.done
)
homer_make_tag_directory_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_4_JOB_ID: homer_make_tag_directory.POL2_shNIPBL-3_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2_shNIPBL-3_EtOH_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2_shNIPBL-3_EtOH_Rep1.7834d68ebee894ad2bd92c311928145c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2_shNIPBL-3_EtOH_Rep1.7834d68ebee894ad2bd92c311928145c.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2_shNIPBL-3_EtOH_Rep1 \
  alignment/POL2_shNIPBL-3_EtOH_Rep1/POL2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2_shNIPBL-3_EtOH_Rep1.7834d68ebee894ad2bd92c311928145c.mugqic.done
)
homer_make_tag_directory_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_5_JOB_ID: homer_make_tag_directory.WCE_shNIPBL-5_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.WCE_shNIPBL-5_EtOH_Rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.WCE_shNIPBL-5_EtOH_Rep1.e54ed40d860735275f0d6b0c7450d5c5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.WCE_shNIPBL-5_EtOH_Rep1.e54ed40d860735275f0d6b0c7450d5c5.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/WCE_shNIPBL-5_EtOH_Rep1 \
  alignment/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.WCE_shNIPBL-5_EtOH_Rep1.e54ed40d860735275f0d6b0c7450d5c5.mugqic.done
)
homer_make_tag_directory_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_tag_directory_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_6_JOB_ID: homer_make_tag_directory.POL2_shNIPBL-5_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2_shNIPBL-5_EtOH_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2_shNIPBL-5_EtOH_Rep1.b91f4a9dcf773b111615c1a9ce9a9d22.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2_shNIPBL-5_EtOH_Rep1.b91f4a9dcf773b111615c1a9ce9a9d22.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2_shNIPBL-5_EtOH_Rep1 \
  alignment/POL2_shNIPBL-5_EtOH_Rep1/POL2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2_shNIPBL-5_EtOH_Rep1.b91f4a9dcf773b111615c1a9ce9a9d22.mugqic.done
)
homer_make_tag_directory_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_7_JOB_ID: homer_make_tag_directory.POL2_shCTRL-2_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2_shCTRL-2_EtOH_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2_shCTRL-2_EtOH_Rep1.5dae483bcc86ef9bbe80b69a85e2c43e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2_shCTRL-2_EtOH_Rep1.5dae483bcc86ef9bbe80b69a85e2c43e.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2_shCTRL-2_EtOH_Rep1 \
  alignment/POL2_shCTRL-2_EtOH_Rep1/POL2_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2_shCTRL-2_EtOH_Rep1.5dae483bcc86ef9bbe80b69a85e2c43e.mugqic.done
)
homer_make_tag_directory_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_8_JOB_ID: homer_make_tag_directory.WCE_shCRTL-1_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.WCE_shCRTL-1_Dex_Rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_2_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.WCE_shCRTL-1_Dex_Rep1.d3502f996aa104acf54fcdbb2288b58e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.WCE_shCRTL-1_Dex_Rep1.d3502f996aa104acf54fcdbb2288b58e.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/WCE_shCRTL-1_Dex_Rep1 \
  alignment/WCE_shCRTL-1_Dex_Rep1/WCE_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.WCE_shCRTL-1_Dex_Rep1.d3502f996aa104acf54fcdbb2288b58e.mugqic.done
)
homer_make_tag_directory_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_tag_directory_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_9_JOB_ID: homer_make_tag_directory.POL2_shNIPBL-3_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2_shNIPBL-3_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2_shNIPBL-3_Dex_Rep1.23d612f563cf2d6dff0918b6812129bd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2_shNIPBL-3_Dex_Rep1.23d612f563cf2d6dff0918b6812129bd.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2_shNIPBL-3_Dex_Rep1 \
  alignment/POL2_shNIPBL-3_Dex_Rep1/POL2_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2_shNIPBL-3_Dex_Rep1.23d612f563cf2d6dff0918b6812129bd.mugqic.done
)
homer_make_tag_directory_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_10_JOB_ID: homer_make_tag_directory.POL2_shNIPBL-5_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2_shNIPBL-5_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2_shNIPBL-5_Dex_Rep1.c64322eb8ea9ce05cf4e325b7c04a77c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2_shNIPBL-5_Dex_Rep1.c64322eb8ea9ce05cf4e325b7c04a77c.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2_shNIPBL-5_Dex_Rep1 \
  alignment/POL2_shNIPBL-5_Dex_Rep1/POL2_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2_shNIPBL-5_Dex_Rep1.c64322eb8ea9ce05cf4e325b7c04a77c.mugqic.done
)
homer_make_tag_directory_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_11_JOB_ID: homer_make_tag_directory.WCE_shCTRL-1_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.WCE_shCTRL-1_EtOH_Rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_3_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.WCE_shCTRL-1_EtOH_Rep1.bfa2e4a0e1a51ed1eff397524f31c4ea.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.WCE_shCTRL-1_EtOH_Rep1.bfa2e4a0e1a51ed1eff397524f31c4ea.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/WCE_shCTRL-1_EtOH_Rep1 \
  alignment/WCE_shCTRL-1_EtOH_Rep1/WCE_shCTRL-1_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.WCE_shCTRL-1_EtOH_Rep1.bfa2e4a0e1a51ed1eff397524f31c4ea.mugqic.done
)
homer_make_tag_directory_11_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_tag_directory_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_12_JOB_ID: homer_make_tag_directory.WCE_shCTRL-2_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.WCE_shCTRL-2_Dex_Rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.WCE_shCTRL-2_Dex_Rep1.736587965358b3da0d012a8c529ae76a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.WCE_shCTRL-2_Dex_Rep1.736587965358b3da0d012a8c529ae76a.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/WCE_shCTRL-2_Dex_Rep1 \
  alignment/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.WCE_shCTRL-2_Dex_Rep1.736587965358b3da0d012a8c529ae76a.mugqic.done
)
homer_make_tag_directory_12_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_tag_directory_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_13_JOB_ID: homer_make_tag_directory.WCE_shNIPBL-3_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.WCE_shNIPBL-3_EtOH_Rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_5_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.WCE_shNIPBL-3_EtOH_Rep1.528f139a9c46420d9ee161828dd5bf74.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.WCE_shNIPBL-3_EtOH_Rep1.528f139a9c46420d9ee161828dd5bf74.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/WCE_shNIPBL-3_EtOH_Rep1 \
  alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.WCE_shNIPBL-3_EtOH_Rep1.528f139a9c46420d9ee161828dd5bf74.mugqic.done
)
homer_make_tag_directory_13_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_tag_directory_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_14_JOB_ID: homer_make_tag_directory.POL2_shCRTL-1_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2_shCRTL-1_EtOH_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2_shCRTL-1_EtOH_Rep1.27cf34bf81de47c0922a11b34e75018d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2_shCRTL-1_EtOH_Rep1.27cf34bf81de47c0922a11b34e75018d.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2_shCRTL-1_EtOH_Rep1 \
  alignment/POL2_shCRTL-1_EtOH_Rep1/POL2_shCRTL-1_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2_shCRTL-1_EtOH_Rep1.27cf34bf81de47c0922a11b34e75018d.mugqic.done
)
homer_make_tag_directory_14_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_15_JOB_ID: homer_make_tag_directory.WCE_shNIPBL-5_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.WCE_shNIPBL-5_Dex_Rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_6_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.WCE_shNIPBL-5_Dex_Rep1.25379ab405d2974df70e2823fb4c1f1a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.WCE_shNIPBL-5_Dex_Rep1.25379ab405d2974df70e2823fb4c1f1a.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/WCE_shNIPBL-5_Dex_Rep1 \
  alignment/WCE_shNIPBL-5_Dex_Rep1/WCE_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.WCE_shNIPBL-5_Dex_Rep1.25379ab405d2974df70e2823fb4c1f1a.mugqic.done
)
homer_make_tag_directory_15_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_tag_directory_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_16_JOB_ID: homer_make_tag_directory.WCE_shNIPBL-3_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.WCE_shNIPBL-3_Dex_Rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_7_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.WCE_shNIPBL-3_Dex_Rep1.6cc2eefc00ca81a653670d4ada77f9ec.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.WCE_shNIPBL-3_Dex_Rep1.6cc2eefc00ca81a653670d4ada77f9ec.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/WCE_shNIPBL-3_Dex_Rep1 \
  alignment/WCE_shNIPBL-3_Dex_Rep1/WCE_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.WCE_shNIPBL-3_Dex_Rep1.6cc2eefc00ca81a653670d4ada77f9ec.mugqic.done
)
homer_make_tag_directory_16_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_tag_directory_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_17_JOB_ID: homer_make_tag_directory.POL2-ser2_shNIPBL-3_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2-ser2_shNIPBL-3_EtOH_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2-ser2_shNIPBL-3_EtOH_Rep1.b8625d777a2c8f2a9df0608c522a3cab.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2-ser2_shNIPBL-3_EtOH_Rep1.b8625d777a2c8f2a9df0608c522a3cab.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2-ser2_shNIPBL-3_EtOH_Rep1 \
  alignment/POL2-ser2_shNIPBL-3_EtOH_Rep1/POL2-ser2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2-ser2_shNIPBL-3_EtOH_Rep1.b8625d777a2c8f2a9df0608c522a3cab.mugqic.done
)
homer_make_tag_directory_17_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_18_JOB_ID: homer_make_tag_directory.BRD4_NA_EtOH_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.BRD4_NA_EtOH_Rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.BRD4_NA_EtOH_Rep2.c8665361813ac8154f0040a646ec8b1f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.BRD4_NA_EtOH_Rep2.c8665361813ac8154f0040a646ec8b1f.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/BRD4_NA_EtOH_Rep2 \
  alignment/BRD4_NA_EtOH_Rep2/BRD4_NA_EtOH_Rep2.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.BRD4_NA_EtOH_Rep2.c8665361813ac8154f0040a646ec8b1f.mugqic.done
)
homer_make_tag_directory_18_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_19_JOB_ID: homer_make_tag_directory.WCE_NA_Dex_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.WCE_NA_Dex_Rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.WCE_NA_Dex_Rep2.0e14acf96548f914cb753d6fdffc442a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.WCE_NA_Dex_Rep2.0e14acf96548f914cb753d6fdffc442a.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/WCE_NA_Dex_Rep2 \
  alignment/WCE_NA_Dex_Rep2/WCE_NA_Dex_Rep2.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.WCE_NA_Dex_Rep2.0e14acf96548f914cb753d6fdffc442a.mugqic.done
)
homer_make_tag_directory_19_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_20_JOB_ID: homer_make_tag_directory.CDK9_NA_Dex_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.CDK9_NA_Dex_Rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.CDK9_NA_Dex_Rep2.2723a1cfdd90b8dd5331ceb9c62c926a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.CDK9_NA_Dex_Rep2.2723a1cfdd90b8dd5331ceb9c62c926a.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/CDK9_NA_Dex_Rep2 \
  alignment/CDK9_NA_Dex_Rep2/CDK9_NA_Dex_Rep2.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.CDK9_NA_Dex_Rep2.2723a1cfdd90b8dd5331ceb9c62c926a.mugqic.done
)
homer_make_tag_directory_20_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_21_JOB_ID: homer_make_tag_directory.POL2-ser2_shNIPBL-5_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2-ser2_shNIPBL-5_EtOH_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2-ser2_shNIPBL-5_EtOH_Rep1.fb3a2b34caf1f6bf8ffdd562c413495b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2-ser2_shNIPBL-5_EtOH_Rep1.fb3a2b34caf1f6bf8ffdd562c413495b.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2-ser2_shNIPBL-5_EtOH_Rep1 \
  alignment/POL2-ser2_shNIPBL-5_EtOH_Rep1/POL2-ser2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2-ser2_shNIPBL-5_EtOH_Rep1.fb3a2b34caf1f6bf8ffdd562c413495b.mugqic.done
)
homer_make_tag_directory_21_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_21_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_22_JOB_ID: homer_make_tag_directory.POL2-ser2_shCTRL-2_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2-ser2_shCTRL-2_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2-ser2_shCTRL-2_Dex_Rep1.e8a051bd6fa6e3bbf6b89aaab6459bea.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2-ser2_shCTRL-2_Dex_Rep1.e8a051bd6fa6e3bbf6b89aaab6459bea.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2-ser2_shCTRL-2_Dex_Rep1 \
  alignment/POL2-ser2_shCTRL-2_Dex_Rep1/POL2-ser2_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2-ser2_shCTRL-2_Dex_Rep1.e8a051bd6fa6e3bbf6b89aaab6459bea.mugqic.done
)
homer_make_tag_directory_22_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_22_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_23_JOB_ID: homer_make_tag_directory.POL2-ser2_shNIPBL-3_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2-ser2_shNIPBL-3_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2-ser2_shNIPBL-3_Dex_Rep1.bfa81a9a199c181013ec0a46dea31700.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2-ser2_shNIPBL-3_Dex_Rep1.bfa81a9a199c181013ec0a46dea31700.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2-ser2_shNIPBL-3_Dex_Rep1 \
  alignment/POL2-ser2_shNIPBL-3_Dex_Rep1/POL2-ser2_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2-ser2_shNIPBL-3_Dex_Rep1.bfa81a9a199c181013ec0a46dea31700.mugqic.done
)
homer_make_tag_directory_23_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_23_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_24_JOB_ID: homer_make_tag_directory.POL2-ser2_shCRTL-1_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2-ser2_shCRTL-1_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2-ser2_shCRTL-1_Dex_Rep1.446b882f3a20b63df4102b7edc4e8729.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2-ser2_shCRTL-1_Dex_Rep1.446b882f3a20b63df4102b7edc4e8729.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2-ser2_shCRTL-1_Dex_Rep1 \
  alignment/POL2-ser2_shCRTL-1_Dex_Rep1/POL2-ser2_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2-ser2_shCRTL-1_Dex_Rep1.446b882f3a20b63df4102b7edc4e8729.mugqic.done
)
homer_make_tag_directory_24_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_24_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_25_JOB_ID: homer_make_tag_directory.POL2-ser2_shNIPBL-5_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2-ser2_shNIPBL-5_Dex_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2-ser2_shNIPBL-5_Dex_Rep1.cf2e2f5aade67a9af86ca4dada1cfd03.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2-ser2_shNIPBL-5_Dex_Rep1.cf2e2f5aade67a9af86ca4dada1cfd03.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2-ser2_shNIPBL-5_Dex_Rep1 \
  alignment/POL2-ser2_shNIPBL-5_Dex_Rep1/POL2-ser2_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2-ser2_shNIPBL-5_Dex_Rep1.cf2e2f5aade67a9af86ca4dada1cfd03.mugqic.done
)
homer_make_tag_directory_25_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_25_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_26_JOB_ID: homer_make_tag_directory.WCE_NA_EtOH_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.WCE_NA_EtOH_Rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.WCE_NA_EtOH_Rep2.14027a8d462e669b9631e39023b9a75e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.WCE_NA_EtOH_Rep2.14027a8d462e669b9631e39023b9a75e.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/WCE_NA_EtOH_Rep2 \
  alignment/WCE_NA_EtOH_Rep2/WCE_NA_EtOH_Rep2.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.WCE_NA_EtOH_Rep2.14027a8d462e669b9631e39023b9a75e.mugqic.done
)
homer_make_tag_directory_26_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_26_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_27_JOB_ID: homer_make_tag_directory.POL2-ser2_shCRTL-1_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2-ser2_shCRTL-1_EtOH_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2-ser2_shCRTL-1_EtOH_Rep1.f9e0f663860161598e45905a932f21b2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2-ser2_shCRTL-1_EtOH_Rep1.f9e0f663860161598e45905a932f21b2.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2-ser2_shCRTL-1_EtOH_Rep1 \
  alignment/POL2-ser2_shCRTL-1_EtOH_Rep1/POL2-ser2_shCRTL-1_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2-ser2_shCRTL-1_EtOH_Rep1.f9e0f663860161598e45905a932f21b2.mugqic.done
)
homer_make_tag_directory_27_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_27_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_28_JOB_ID: homer_make_tag_directory.POL2-ser2_shCTRL-2_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.POL2-ser2_shCTRL-2_EtOH_Rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_8_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.POL2-ser2_shCTRL-2_EtOH_Rep1.e47a5ff816e11541d14a55058ed43c8b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.POL2-ser2_shCTRL-2_EtOH_Rep1.e47a5ff816e11541d14a55058ed43c8b.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/POL2-ser2_shCTRL-2_EtOH_Rep1 \
  alignment/POL2-ser2_shCTRL-2_EtOH_Rep1/POL2-ser2_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.POL2-ser2_shCTRL-2_EtOH_Rep1.e47a5ff816e11541d14a55058ed43c8b.mugqic.done
)
homer_make_tag_directory_28_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_tag_directory_28_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_29_JOB_ID: homer_make_tag_directory.CDK9_NA_EtOH_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.CDK9_NA_EtOH_Rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.CDK9_NA_EtOH_Rep2.537b10d094a31444fec35923c51c3f74.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.CDK9_NA_EtOH_Rep2.537b10d094a31444fec35923c51c3f74.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/CDK9_NA_EtOH_Rep2 \
  alignment/CDK9_NA_EtOH_Rep2/CDK9_NA_EtOH_Rep2.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.CDK9_NA_EtOH_Rep2.537b10d094a31444fec35923c51c3f74.mugqic.done
)
homer_make_tag_directory_29_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_29_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_30_JOB_ID: homer_make_tag_directory.BRD4_NA_Dex_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.BRD4_NA_Dex_Rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.BRD4_NA_Dex_Rep2.67f60ca782cbfbfd086a4e768a8f4566.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.BRD4_NA_Dex_Rep2.67f60ca782cbfbfd086a4e768a8f4566.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/BRD4_NA_Dex_Rep2 \
  alignment/BRD4_NA_Dex_Rep2/BRD4_NA_Dex_Rep2.sorted.dup.bam \
  -checkGC -genome GRCh38
homer_make_tag_directory.BRD4_NA_Dex_Rep2.67f60ca782cbfbfd086a4e768a8f4566.mugqic.done
)
homer_make_tag_directory_30_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_make_tag_directory_30_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: qc_metrics
#-------------------------------------------------------------------------------
STEP=qc_metrics
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: qc_metrics_1_JOB_ID: qc_plots_R
#-------------------------------------------------------------------------------
JOB_NAME=qc_plots_R
JOB_DEPENDENCIES=$homer_make_tag_directory_1_JOB_ID:$homer_make_tag_directory_2_JOB_ID:$homer_make_tag_directory_3_JOB_ID:$homer_make_tag_directory_4_JOB_ID:$homer_make_tag_directory_5_JOB_ID:$homer_make_tag_directory_6_JOB_ID:$homer_make_tag_directory_7_JOB_ID:$homer_make_tag_directory_8_JOB_ID:$homer_make_tag_directory_9_JOB_ID:$homer_make_tag_directory_10_JOB_ID:$homer_make_tag_directory_11_JOB_ID:$homer_make_tag_directory_12_JOB_ID:$homer_make_tag_directory_13_JOB_ID:$homer_make_tag_directory_14_JOB_ID:$homer_make_tag_directory_15_JOB_ID:$homer_make_tag_directory_16_JOB_ID:$homer_make_tag_directory_17_JOB_ID:$homer_make_tag_directory_18_JOB_ID:$homer_make_tag_directory_19_JOB_ID:$homer_make_tag_directory_20_JOB_ID:$homer_make_tag_directory_21_JOB_ID:$homer_make_tag_directory_22_JOB_ID:$homer_make_tag_directory_23_JOB_ID:$homer_make_tag_directory_24_JOB_ID:$homer_make_tag_directory_25_JOB_ID:$homer_make_tag_directory_26_JOB_ID:$homer_make_tag_directory_27_JOB_ID:$homer_make_tag_directory_28_JOB_ID:$homer_make_tag_directory_29_JOB_ID:$homer_make_tag_directory_30_JOB_ID
JOB_DONE=job_output/qc_metrics/qc_plots_R.15b0e86760c8425d81478c44ae38f14d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'qc_plots_R.15b0e86760c8425d81478c44ae38f14d.mugqic.done'
module load mugqic/mugqic_tools/2.1.5 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p graphs && \
Rscript $R_TOOLS/chipSeqGenerateQCMetrics.R \
  ../../raw/design.txt \
  /gs/scratch/efournier/A549_Michele/output/chip-pipeline-GRCh38 && \
cp /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.qc_metrics.md report/ChipSeq.qc_metrics.md && \
for sample in POL2_shCRTL-1_Dex_Rep1 POL2_shCTRL-2_Dex_Rep1 WCE_shCTRL-2_EtOH_Rep1 POL2_shNIPBL-3_EtOH_Rep1 WCE_shNIPBL-5_EtOH_Rep1 POL2_shNIPBL-5_EtOH_Rep1 POL2_shCTRL-2_EtOH_Rep1 WCE_shCRTL-1_Dex_Rep1 POL2_shNIPBL-3_Dex_Rep1 POL2_shNIPBL-5_Dex_Rep1 WCE_shCTRL-1_EtOH_Rep1 WCE_shCTRL-2_Dex_Rep1 WCE_shNIPBL-3_EtOH_Rep1 POL2_shCRTL-1_EtOH_Rep1 WCE_shNIPBL-5_Dex_Rep1 WCE_shNIPBL-3_Dex_Rep1 POL2-ser2_shNIPBL-3_EtOH_Rep1 BRD4_NA_EtOH_Rep2 WCE_NA_Dex_Rep2 CDK9_NA_Dex_Rep2 POL2-ser2_shNIPBL-5_EtOH_Rep1 POL2-ser2_shCTRL-2_Dex_Rep1 POL2-ser2_shNIPBL-3_Dex_Rep1 POL2-ser2_shCRTL-1_Dex_Rep1 POL2-ser2_shNIPBL-5_Dex_Rep1 WCE_NA_EtOH_Rep2 POL2-ser2_shCRTL-1_EtOH_Rep1 POL2-ser2_shCTRL-2_EtOH_Rep1 CDK9_NA_EtOH_Rep2 BRD4_NA_Dex_Rep2
do
  cp --parents graphs/${sample}_QC_Metrics.ps report/
  convert -rotate 90 graphs/${sample}_QC_Metrics.ps report/graphs/${sample}_QC_Metrics.png
  echo -e "----

![QC Metrics for Sample $sample ([download high-res image](graphs/${sample}_QC_Metrics.ps))](graphs/${sample}_QC_Metrics.png)
" \
  >> report/ChipSeq.qc_metrics.md
done
qc_plots_R.15b0e86760c8425d81478c44ae38f14d.mugqic.done
)
qc_metrics_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$qc_metrics_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: homer_make_ucsc_file
#-------------------------------------------------------------------------------
STEP=homer_make_ucsc_file
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_1_JOB_ID: homer_make_ucsc_file.POL2_shCRTL-1_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2_shCRTL-1_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_1_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2_shCRTL-1_Dex_Rep1.9b5f0a6d5449c5893004ca49ca9b8b2c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2_shCRTL-1_Dex_Rep1.9b5f0a6d5449c5893004ca49ca9b8b2c.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2_shCRTL-1_Dex_Rep1 && \
makeUCSCfile \
  tags/POL2_shCRTL-1_Dex_Rep1 | \
gzip -1 -c > tracks/POL2_shCRTL-1_Dex_Rep1/POL2_shCRTL-1_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2_shCRTL-1_Dex_Rep1.9b5f0a6d5449c5893004ca49ca9b8b2c.mugqic.done
)
homer_make_ucsc_file_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_2_JOB_ID: homer_make_ucsc_file.POL2_shCTRL-2_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2_shCTRL-2_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_2_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2_shCTRL-2_Dex_Rep1.2ff023f60845cd80e6d06b32606e1747.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2_shCTRL-2_Dex_Rep1.2ff023f60845cd80e6d06b32606e1747.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2_shCTRL-2_Dex_Rep1 && \
makeUCSCfile \
  tags/POL2_shCTRL-2_Dex_Rep1 | \
gzip -1 -c > tracks/POL2_shCTRL-2_Dex_Rep1/POL2_shCTRL-2_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2_shCTRL-2_Dex_Rep1.2ff023f60845cd80e6d06b32606e1747.mugqic.done
)
homer_make_ucsc_file_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_3_JOB_ID: homer_make_ucsc_file.WCE_shCTRL-2_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.WCE_shCTRL-2_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_3_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.WCE_shCTRL-2_EtOH_Rep1.98b8c509aff83fdfe352f303f0d28ef3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.WCE_shCTRL-2_EtOH_Rep1.98b8c509aff83fdfe352f303f0d28ef3.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/WCE_shCTRL-2_EtOH_Rep1 && \
makeUCSCfile \
  tags/WCE_shCTRL-2_EtOH_Rep1 | \
gzip -1 -c > tracks/WCE_shCTRL-2_EtOH_Rep1/WCE_shCTRL-2_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.WCE_shCTRL-2_EtOH_Rep1.98b8c509aff83fdfe352f303f0d28ef3.mugqic.done
)
homer_make_ucsc_file_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_4_JOB_ID: homer_make_ucsc_file.POL2_shNIPBL-3_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2_shNIPBL-3_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_4_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2_shNIPBL-3_EtOH_Rep1.67aac446d5eb2bac15afdb71264ec83a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2_shNIPBL-3_EtOH_Rep1.67aac446d5eb2bac15afdb71264ec83a.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2_shNIPBL-3_EtOH_Rep1 && \
makeUCSCfile \
  tags/POL2_shNIPBL-3_EtOH_Rep1 | \
gzip -1 -c > tracks/POL2_shNIPBL-3_EtOH_Rep1/POL2_shNIPBL-3_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2_shNIPBL-3_EtOH_Rep1.67aac446d5eb2bac15afdb71264ec83a.mugqic.done
)
homer_make_ucsc_file_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_5_JOB_ID: homer_make_ucsc_file.WCE_shNIPBL-5_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.WCE_shNIPBL-5_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_5_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.WCE_shNIPBL-5_EtOH_Rep1.2a9ab5f21263fc1d0af09b77c8481466.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.WCE_shNIPBL-5_EtOH_Rep1.2a9ab5f21263fc1d0af09b77c8481466.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/WCE_shNIPBL-5_EtOH_Rep1 && \
makeUCSCfile \
  tags/WCE_shNIPBL-5_EtOH_Rep1 | \
gzip -1 -c > tracks/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.WCE_shNIPBL-5_EtOH_Rep1.2a9ab5f21263fc1d0af09b77c8481466.mugqic.done
)
homer_make_ucsc_file_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_6_JOB_ID: homer_make_ucsc_file.POL2_shNIPBL-5_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2_shNIPBL-5_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_6_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2_shNIPBL-5_EtOH_Rep1.072f60a4277188bdb769e52b1a1c73fb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2_shNIPBL-5_EtOH_Rep1.072f60a4277188bdb769e52b1a1c73fb.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2_shNIPBL-5_EtOH_Rep1 && \
makeUCSCfile \
  tags/POL2_shNIPBL-5_EtOH_Rep1 | \
gzip -1 -c > tracks/POL2_shNIPBL-5_EtOH_Rep1/POL2_shNIPBL-5_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2_shNIPBL-5_EtOH_Rep1.072f60a4277188bdb769e52b1a1c73fb.mugqic.done
)
homer_make_ucsc_file_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_7_JOB_ID: homer_make_ucsc_file.POL2_shCTRL-2_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2_shCTRL-2_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_7_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2_shCTRL-2_EtOH_Rep1.c4f6118bcfa83e0e5a9d5de09120da7e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2_shCTRL-2_EtOH_Rep1.c4f6118bcfa83e0e5a9d5de09120da7e.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2_shCTRL-2_EtOH_Rep1 && \
makeUCSCfile \
  tags/POL2_shCTRL-2_EtOH_Rep1 | \
gzip -1 -c > tracks/POL2_shCTRL-2_EtOH_Rep1/POL2_shCTRL-2_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2_shCTRL-2_EtOH_Rep1.c4f6118bcfa83e0e5a9d5de09120da7e.mugqic.done
)
homer_make_ucsc_file_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_8_JOB_ID: homer_make_ucsc_file.WCE_shCRTL-1_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.WCE_shCRTL-1_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_8_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.WCE_shCRTL-1_Dex_Rep1.cfc8b432aebc51ce3646b02221ddbecb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.WCE_shCRTL-1_Dex_Rep1.cfc8b432aebc51ce3646b02221ddbecb.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/WCE_shCRTL-1_Dex_Rep1 && \
makeUCSCfile \
  tags/WCE_shCRTL-1_Dex_Rep1 | \
gzip -1 -c > tracks/WCE_shCRTL-1_Dex_Rep1/WCE_shCRTL-1_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.WCE_shCRTL-1_Dex_Rep1.cfc8b432aebc51ce3646b02221ddbecb.mugqic.done
)
homer_make_ucsc_file_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_9_JOB_ID: homer_make_ucsc_file.POL2_shNIPBL-3_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2_shNIPBL-3_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_9_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2_shNIPBL-3_Dex_Rep1.db2923255c47cbadd52fcd2939daaf37.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2_shNIPBL-3_Dex_Rep1.db2923255c47cbadd52fcd2939daaf37.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2_shNIPBL-3_Dex_Rep1 && \
makeUCSCfile \
  tags/POL2_shNIPBL-3_Dex_Rep1 | \
gzip -1 -c > tracks/POL2_shNIPBL-3_Dex_Rep1/POL2_shNIPBL-3_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2_shNIPBL-3_Dex_Rep1.db2923255c47cbadd52fcd2939daaf37.mugqic.done
)
homer_make_ucsc_file_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_10_JOB_ID: homer_make_ucsc_file.POL2_shNIPBL-5_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2_shNIPBL-5_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_10_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2_shNIPBL-5_Dex_Rep1.62d40007e60dde339e0c3f1ba459afec.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2_shNIPBL-5_Dex_Rep1.62d40007e60dde339e0c3f1ba459afec.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2_shNIPBL-5_Dex_Rep1 && \
makeUCSCfile \
  tags/POL2_shNIPBL-5_Dex_Rep1 | \
gzip -1 -c > tracks/POL2_shNIPBL-5_Dex_Rep1/POL2_shNIPBL-5_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2_shNIPBL-5_Dex_Rep1.62d40007e60dde339e0c3f1ba459afec.mugqic.done
)
homer_make_ucsc_file_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_11_JOB_ID: homer_make_ucsc_file.WCE_shCTRL-1_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.WCE_shCTRL-1_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_11_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.WCE_shCTRL-1_EtOH_Rep1.f16f95fe833adf3a59f1a746799dfde8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.WCE_shCTRL-1_EtOH_Rep1.f16f95fe833adf3a59f1a746799dfde8.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/WCE_shCTRL-1_EtOH_Rep1 && \
makeUCSCfile \
  tags/WCE_shCTRL-1_EtOH_Rep1 | \
gzip -1 -c > tracks/WCE_shCTRL-1_EtOH_Rep1/WCE_shCTRL-1_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.WCE_shCTRL-1_EtOH_Rep1.f16f95fe833adf3a59f1a746799dfde8.mugqic.done
)
homer_make_ucsc_file_11_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_12_JOB_ID: homer_make_ucsc_file.WCE_shCTRL-2_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.WCE_shCTRL-2_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_12_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.WCE_shCTRL-2_Dex_Rep1.431698b678ef0e0be31d85d911635121.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.WCE_shCTRL-2_Dex_Rep1.431698b678ef0e0be31d85d911635121.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/WCE_shCTRL-2_Dex_Rep1 && \
makeUCSCfile \
  tags/WCE_shCTRL-2_Dex_Rep1 | \
gzip -1 -c > tracks/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.WCE_shCTRL-2_Dex_Rep1.431698b678ef0e0be31d85d911635121.mugqic.done
)
homer_make_ucsc_file_12_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_13_JOB_ID: homer_make_ucsc_file.WCE_shNIPBL-3_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.WCE_shNIPBL-3_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_13_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.WCE_shNIPBL-3_EtOH_Rep1.f77411ac2e5e267023e8fe4d9cc49ee5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.WCE_shNIPBL-3_EtOH_Rep1.f77411ac2e5e267023e8fe4d9cc49ee5.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/WCE_shNIPBL-3_EtOH_Rep1 && \
makeUCSCfile \
  tags/WCE_shNIPBL-3_EtOH_Rep1 | \
gzip -1 -c > tracks/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.WCE_shNIPBL-3_EtOH_Rep1.f77411ac2e5e267023e8fe4d9cc49ee5.mugqic.done
)
homer_make_ucsc_file_13_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_14_JOB_ID: homer_make_ucsc_file.POL2_shCRTL-1_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2_shCRTL-1_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_14_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2_shCRTL-1_EtOH_Rep1.81bb3c570754360746c2f5602ea03f78.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2_shCRTL-1_EtOH_Rep1.81bb3c570754360746c2f5602ea03f78.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2_shCRTL-1_EtOH_Rep1 && \
makeUCSCfile \
  tags/POL2_shCRTL-1_EtOH_Rep1 | \
gzip -1 -c > tracks/POL2_shCRTL-1_EtOH_Rep1/POL2_shCRTL-1_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2_shCRTL-1_EtOH_Rep1.81bb3c570754360746c2f5602ea03f78.mugqic.done
)
homer_make_ucsc_file_14_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_15_JOB_ID: homer_make_ucsc_file.WCE_shNIPBL-5_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.WCE_shNIPBL-5_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_15_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.WCE_shNIPBL-5_Dex_Rep1.9107fea1c09d5039335ecaf6386f829f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.WCE_shNIPBL-5_Dex_Rep1.9107fea1c09d5039335ecaf6386f829f.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/WCE_shNIPBL-5_Dex_Rep1 && \
makeUCSCfile \
  tags/WCE_shNIPBL-5_Dex_Rep1 | \
gzip -1 -c > tracks/WCE_shNIPBL-5_Dex_Rep1/WCE_shNIPBL-5_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.WCE_shNIPBL-5_Dex_Rep1.9107fea1c09d5039335ecaf6386f829f.mugqic.done
)
homer_make_ucsc_file_15_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_16_JOB_ID: homer_make_ucsc_file.WCE_shNIPBL-3_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.WCE_shNIPBL-3_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_16_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.WCE_shNIPBL-3_Dex_Rep1.c2fa1342c5dafe28ef0594cc36048fcf.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.WCE_shNIPBL-3_Dex_Rep1.c2fa1342c5dafe28ef0594cc36048fcf.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/WCE_shNIPBL-3_Dex_Rep1 && \
makeUCSCfile \
  tags/WCE_shNIPBL-3_Dex_Rep1 | \
gzip -1 -c > tracks/WCE_shNIPBL-3_Dex_Rep1/WCE_shNIPBL-3_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.WCE_shNIPBL-3_Dex_Rep1.c2fa1342c5dafe28ef0594cc36048fcf.mugqic.done
)
homer_make_ucsc_file_16_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_17_JOB_ID: homer_make_ucsc_file.POL2-ser2_shNIPBL-3_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2-ser2_shNIPBL-3_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_17_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2-ser2_shNIPBL-3_EtOH_Rep1.adb63d8b47167521b84de19140300d51.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2-ser2_shNIPBL-3_EtOH_Rep1.adb63d8b47167521b84de19140300d51.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2-ser2_shNIPBL-3_EtOH_Rep1 && \
makeUCSCfile \
  tags/POL2-ser2_shNIPBL-3_EtOH_Rep1 | \
gzip -1 -c > tracks/POL2-ser2_shNIPBL-3_EtOH_Rep1/POL2-ser2_shNIPBL-3_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2-ser2_shNIPBL-3_EtOH_Rep1.adb63d8b47167521b84de19140300d51.mugqic.done
)
homer_make_ucsc_file_17_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_18_JOB_ID: homer_make_ucsc_file.BRD4_NA_EtOH_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.BRD4_NA_EtOH_Rep2
JOB_DEPENDENCIES=$homer_make_tag_directory_18_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.BRD4_NA_EtOH_Rep2.4636a2be9f2cb389242700c72ef8b03b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.BRD4_NA_EtOH_Rep2.4636a2be9f2cb389242700c72ef8b03b.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/BRD4_NA_EtOH_Rep2 && \
makeUCSCfile \
  tags/BRD4_NA_EtOH_Rep2 | \
gzip -1 -c > tracks/BRD4_NA_EtOH_Rep2/BRD4_NA_EtOH_Rep2.ucsc.bedGraph.gz
homer_make_ucsc_file.BRD4_NA_EtOH_Rep2.4636a2be9f2cb389242700c72ef8b03b.mugqic.done
)
homer_make_ucsc_file_18_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_19_JOB_ID: homer_make_ucsc_file.WCE_NA_Dex_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.WCE_NA_Dex_Rep2
JOB_DEPENDENCIES=$homer_make_tag_directory_19_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.WCE_NA_Dex_Rep2.3b783316b50b151c3f2f973d652b411e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.WCE_NA_Dex_Rep2.3b783316b50b151c3f2f973d652b411e.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/WCE_NA_Dex_Rep2 && \
makeUCSCfile \
  tags/WCE_NA_Dex_Rep2 | \
gzip -1 -c > tracks/WCE_NA_Dex_Rep2/WCE_NA_Dex_Rep2.ucsc.bedGraph.gz
homer_make_ucsc_file.WCE_NA_Dex_Rep2.3b783316b50b151c3f2f973d652b411e.mugqic.done
)
homer_make_ucsc_file_19_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_20_JOB_ID: homer_make_ucsc_file.CDK9_NA_Dex_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.CDK9_NA_Dex_Rep2
JOB_DEPENDENCIES=$homer_make_tag_directory_20_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.CDK9_NA_Dex_Rep2.f864f712f7b800a1a0740e249c76c195.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.CDK9_NA_Dex_Rep2.f864f712f7b800a1a0740e249c76c195.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/CDK9_NA_Dex_Rep2 && \
makeUCSCfile \
  tags/CDK9_NA_Dex_Rep2 | \
gzip -1 -c > tracks/CDK9_NA_Dex_Rep2/CDK9_NA_Dex_Rep2.ucsc.bedGraph.gz
homer_make_ucsc_file.CDK9_NA_Dex_Rep2.f864f712f7b800a1a0740e249c76c195.mugqic.done
)
homer_make_ucsc_file_20_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_21_JOB_ID: homer_make_ucsc_file.POL2-ser2_shNIPBL-5_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2-ser2_shNIPBL-5_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_21_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2-ser2_shNIPBL-5_EtOH_Rep1.2f52681a1c173bb17fe2df7211466fd3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2-ser2_shNIPBL-5_EtOH_Rep1.2f52681a1c173bb17fe2df7211466fd3.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2-ser2_shNIPBL-5_EtOH_Rep1 && \
makeUCSCfile \
  tags/POL2-ser2_shNIPBL-5_EtOH_Rep1 | \
gzip -1 -c > tracks/POL2-ser2_shNIPBL-5_EtOH_Rep1/POL2-ser2_shNIPBL-5_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2-ser2_shNIPBL-5_EtOH_Rep1.2f52681a1c173bb17fe2df7211466fd3.mugqic.done
)
homer_make_ucsc_file_21_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_21_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_22_JOB_ID: homer_make_ucsc_file.POL2-ser2_shCTRL-2_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2-ser2_shCTRL-2_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_22_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2-ser2_shCTRL-2_Dex_Rep1.b41dd856e84a854a57ea7af069e3b0a2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2-ser2_shCTRL-2_Dex_Rep1.b41dd856e84a854a57ea7af069e3b0a2.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2-ser2_shCTRL-2_Dex_Rep1 && \
makeUCSCfile \
  tags/POL2-ser2_shCTRL-2_Dex_Rep1 | \
gzip -1 -c > tracks/POL2-ser2_shCTRL-2_Dex_Rep1/POL2-ser2_shCTRL-2_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2-ser2_shCTRL-2_Dex_Rep1.b41dd856e84a854a57ea7af069e3b0a2.mugqic.done
)
homer_make_ucsc_file_22_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_22_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_23_JOB_ID: homer_make_ucsc_file.POL2-ser2_shNIPBL-3_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2-ser2_shNIPBL-3_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_23_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2-ser2_shNIPBL-3_Dex_Rep1.64f4382190ebcc4d63591699b1bf0c5d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2-ser2_shNIPBL-3_Dex_Rep1.64f4382190ebcc4d63591699b1bf0c5d.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2-ser2_shNIPBL-3_Dex_Rep1 && \
makeUCSCfile \
  tags/POL2-ser2_shNIPBL-3_Dex_Rep1 | \
gzip -1 -c > tracks/POL2-ser2_shNIPBL-3_Dex_Rep1/POL2-ser2_shNIPBL-3_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2-ser2_shNIPBL-3_Dex_Rep1.64f4382190ebcc4d63591699b1bf0c5d.mugqic.done
)
homer_make_ucsc_file_23_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_23_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_24_JOB_ID: homer_make_ucsc_file.POL2-ser2_shCRTL-1_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2-ser2_shCRTL-1_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_24_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2-ser2_shCRTL-1_Dex_Rep1.a4f8c11399635c3f61583fc87d8f6f4c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2-ser2_shCRTL-1_Dex_Rep1.a4f8c11399635c3f61583fc87d8f6f4c.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2-ser2_shCRTL-1_Dex_Rep1 && \
makeUCSCfile \
  tags/POL2-ser2_shCRTL-1_Dex_Rep1 | \
gzip -1 -c > tracks/POL2-ser2_shCRTL-1_Dex_Rep1/POL2-ser2_shCRTL-1_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2-ser2_shCRTL-1_Dex_Rep1.a4f8c11399635c3f61583fc87d8f6f4c.mugqic.done
)
homer_make_ucsc_file_24_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_24_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_25_JOB_ID: homer_make_ucsc_file.POL2-ser2_shNIPBL-5_Dex_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2-ser2_shNIPBL-5_Dex_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_25_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2-ser2_shNIPBL-5_Dex_Rep1.8c49c24db13980a6f2bd9ed7290ae38e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2-ser2_shNIPBL-5_Dex_Rep1.8c49c24db13980a6f2bd9ed7290ae38e.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2-ser2_shNIPBL-5_Dex_Rep1 && \
makeUCSCfile \
  tags/POL2-ser2_shNIPBL-5_Dex_Rep1 | \
gzip -1 -c > tracks/POL2-ser2_shNIPBL-5_Dex_Rep1/POL2-ser2_shNIPBL-5_Dex_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2-ser2_shNIPBL-5_Dex_Rep1.8c49c24db13980a6f2bd9ed7290ae38e.mugqic.done
)
homer_make_ucsc_file_25_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_25_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_26_JOB_ID: homer_make_ucsc_file.WCE_NA_EtOH_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.WCE_NA_EtOH_Rep2
JOB_DEPENDENCIES=$homer_make_tag_directory_26_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.WCE_NA_EtOH_Rep2.5a17c8aaf015d0317cdbf97942a35ea4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.WCE_NA_EtOH_Rep2.5a17c8aaf015d0317cdbf97942a35ea4.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/WCE_NA_EtOH_Rep2 && \
makeUCSCfile \
  tags/WCE_NA_EtOH_Rep2 | \
gzip -1 -c > tracks/WCE_NA_EtOH_Rep2/WCE_NA_EtOH_Rep2.ucsc.bedGraph.gz
homer_make_ucsc_file.WCE_NA_EtOH_Rep2.5a17c8aaf015d0317cdbf97942a35ea4.mugqic.done
)
homer_make_ucsc_file_26_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_26_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_27_JOB_ID: homer_make_ucsc_file.POL2-ser2_shCRTL-1_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2-ser2_shCRTL-1_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_27_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2-ser2_shCRTL-1_EtOH_Rep1.9fcf8b131cb1e5000ad1f62acc8589ed.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2-ser2_shCRTL-1_EtOH_Rep1.9fcf8b131cb1e5000ad1f62acc8589ed.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2-ser2_shCRTL-1_EtOH_Rep1 && \
makeUCSCfile \
  tags/POL2-ser2_shCRTL-1_EtOH_Rep1 | \
gzip -1 -c > tracks/POL2-ser2_shCRTL-1_EtOH_Rep1/POL2-ser2_shCRTL-1_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2-ser2_shCRTL-1_EtOH_Rep1.9fcf8b131cb1e5000ad1f62acc8589ed.mugqic.done
)
homer_make_ucsc_file_27_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_27_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_28_JOB_ID: homer_make_ucsc_file.POL2-ser2_shCTRL-2_EtOH_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.POL2-ser2_shCTRL-2_EtOH_Rep1
JOB_DEPENDENCIES=$homer_make_tag_directory_28_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.POL2-ser2_shCTRL-2_EtOH_Rep1.e4e9e77f3060c970f16e1008c5edd780.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.POL2-ser2_shCTRL-2_EtOH_Rep1.e4e9e77f3060c970f16e1008c5edd780.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/POL2-ser2_shCTRL-2_EtOH_Rep1 && \
makeUCSCfile \
  tags/POL2-ser2_shCTRL-2_EtOH_Rep1 | \
gzip -1 -c > tracks/POL2-ser2_shCTRL-2_EtOH_Rep1/POL2-ser2_shCTRL-2_EtOH_Rep1.ucsc.bedGraph.gz
homer_make_ucsc_file.POL2-ser2_shCTRL-2_EtOH_Rep1.e4e9e77f3060c970f16e1008c5edd780.mugqic.done
)
homer_make_ucsc_file_28_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_28_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_29_JOB_ID: homer_make_ucsc_file.CDK9_NA_EtOH_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.CDK9_NA_EtOH_Rep2
JOB_DEPENDENCIES=$homer_make_tag_directory_29_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.CDK9_NA_EtOH_Rep2.14cc547494c803d446cddb449855d9e3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.CDK9_NA_EtOH_Rep2.14cc547494c803d446cddb449855d9e3.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/CDK9_NA_EtOH_Rep2 && \
makeUCSCfile \
  tags/CDK9_NA_EtOH_Rep2 | \
gzip -1 -c > tracks/CDK9_NA_EtOH_Rep2/CDK9_NA_EtOH_Rep2.ucsc.bedGraph.gz
homer_make_ucsc_file.CDK9_NA_EtOH_Rep2.14cc547494c803d446cddb449855d9e3.mugqic.done
)
homer_make_ucsc_file_29_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_29_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_30_JOB_ID: homer_make_ucsc_file.BRD4_NA_Dex_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.BRD4_NA_Dex_Rep2
JOB_DEPENDENCIES=$homer_make_tag_directory_30_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.BRD4_NA_Dex_Rep2.9e1778243fd937424a951a786512ff4d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.BRD4_NA_Dex_Rep2.9e1778243fd937424a951a786512ff4d.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/BRD4_NA_Dex_Rep2 && \
makeUCSCfile \
  tags/BRD4_NA_Dex_Rep2 | \
gzip -1 -c > tracks/BRD4_NA_Dex_Rep2/BRD4_NA_Dex_Rep2.ucsc.bedGraph.gz
homer_make_ucsc_file.BRD4_NA_Dex_Rep2.9e1778243fd937424a951a786512ff4d.mugqic.done
)
homer_make_ucsc_file_30_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_30_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_31_JOB_ID: homer_make_ucsc_file_report
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file_report
JOB_DEPENDENCIES=$homer_make_ucsc_file_30_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file_report.c9cac04b3bc71f34cbcde042cf5b419d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file_report.c9cac04b3bc71f34cbcde042cf5b419d.mugqic.done'
mkdir -p report && \
zip -r report/tracks.zip tracks/*/*.ucsc.bedGraph.gz && \
cp /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.homer_make_ucsc_file.md report/
homer_make_ucsc_file_report.c9cac04b3bc71f34cbcde042cf5b419d.mugqic.done
)
homer_make_ucsc_file_31_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_31_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: macs2_callpeak
#-------------------------------------------------------------------------------
STEP=macs2_callpeak
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_1_JOB_ID: macs2_callpeak.POL2_shCRTL-1_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shCRTL-1_Dex
JOB_DEPENDENCIES=$picard_mark_duplicates_2_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shCRTL-1_Dex.bf21455b871f3ec6d872688ede4c2458.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shCRTL-1_Dex.bf21455b871f3ec6d872688ede4c2458.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shCRTL-1_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shCRTL-1_Dex_Rep1/POL2_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCRTL-1_Dex_Rep1/WCE_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shCRTL-1_Dex/POL2_shCRTL-1_Dex \
  >& peak_call/POL2_shCRTL-1_Dex/POL2_shCRTL-1_Dex.diag.macs.out
macs2_callpeak.POL2_shCRTL-1_Dex.bf21455b871f3ec6d872688ede4c2458.mugqic.done
)
macs2_callpeak_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_2_JOB_ID: macs2_callpeak.POL2_shCTRL-2_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shCTRL-2_Dex
JOB_DEPENDENCIES=$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shCTRL-2_Dex.7319e09db4591ebc2100584d7c52c485.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shCTRL-2_Dex.7319e09db4591ebc2100584d7c52c485.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shCTRL-2_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shCTRL-2_Dex_Rep1/POL2_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shCTRL-2_Dex/POL2_shCTRL-2_Dex \
  >& peak_call/POL2_shCTRL-2_Dex/POL2_shCTRL-2_Dex.diag.macs.out
macs2_callpeak.POL2_shCTRL-2_Dex.7319e09db4591ebc2100584d7c52c485.mugqic.done
)
macs2_callpeak_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_3_JOB_ID: macs2_callpeak.POL2_shCRTL_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shCRTL_Dex
JOB_DEPENDENCIES=$picard_mark_duplicates_2_JOB_ID:$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shCRTL_Dex.817e91341e4d07c0ee7e7eb675137797.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shCRTL_Dex.817e91341e4d07c0ee7e7eb675137797.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shCRTL_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shCRTL-1_Dex_Rep1/POL2_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  alignment/POL2_shCTRL-2_Dex_Rep1/POL2_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCRTL-1_Dex_Rep1/WCE_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  alignment/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shCRTL_Dex/POL2_shCRTL_Dex \
  >& peak_call/POL2_shCRTL_Dex/POL2_shCRTL_Dex.diag.macs.out
macs2_callpeak.POL2_shCRTL_Dex.817e91341e4d07c0ee7e7eb675137797.mugqic.done
)
macs2_callpeak_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_4_JOB_ID: macs2_callpeak.POL2_shCRTL-1_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shCRTL-1_EtOH
JOB_DEPENDENCIES=$picard_mark_duplicates_3_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shCRTL-1_EtOH.7a33d4ae55a1579596b00baf20e41ccb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shCRTL-1_EtOH.7a33d4ae55a1579596b00baf20e41ccb.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shCRTL-1_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shCRTL-1_EtOH_Rep1/POL2_shCRTL-1_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCTRL-1_EtOH_Rep1/WCE_shCTRL-1_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shCRTL-1_EtOH/POL2_shCRTL-1_EtOH \
  >& peak_call/POL2_shCRTL-1_EtOH/POL2_shCRTL-1_EtOH.diag.macs.out
macs2_callpeak.POL2_shCRTL-1_EtOH.7a33d4ae55a1579596b00baf20e41ccb.mugqic.done
)
macs2_callpeak_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_5_JOB_ID: macs2_callpeak.POL2_shCTRL_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shCTRL_EtOH
JOB_DEPENDENCIES=$picard_mark_duplicates_3_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shCTRL_EtOH.21b7af8f7b683547f4d4ae29b3b9e348.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shCTRL_EtOH.21b7af8f7b683547f4d4ae29b3b9e348.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shCTRL_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shCRTL-1_EtOH_Rep1/POL2_shCRTL-1_EtOH_Rep1.sorted.dup.bam \
  alignment/POL2_shCTRL-2_EtOH_Rep1/POL2_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCTRL-1_EtOH_Rep1/WCE_shCTRL-1_EtOH_Rep1.sorted.dup.bam \
  alignment/WCE_shCTRL-2_EtOH_Rep1/WCE_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shCTRL_EtOH/POL2_shCTRL_EtOH \
  >& peak_call/POL2_shCTRL_EtOH/POL2_shCTRL_EtOH.diag.macs.out
macs2_callpeak.POL2_shCTRL_EtOH.21b7af8f7b683547f4d4ae29b3b9e348.mugqic.done
)
macs2_callpeak_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_6_JOB_ID: macs2_callpeak.POL2_shNIPBL-3_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shNIPBL-3_Dex
JOB_DEPENDENCIES=$picard_mark_duplicates_7_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shNIPBL-3_Dex.fdbd4c9531c4640f4758dedf951f85fb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shNIPBL-3_Dex.fdbd4c9531c4640f4758dedf951f85fb.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shNIPBL-3_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shNIPBL-3_Dex_Rep1/POL2_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_Dex_Rep1/WCE_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shNIPBL-3_Dex/POL2_shNIPBL-3_Dex \
  >& peak_call/POL2_shNIPBL-3_Dex/POL2_shNIPBL-3_Dex.diag.macs.out
macs2_callpeak.POL2_shNIPBL-3_Dex.fdbd4c9531c4640f4758dedf951f85fb.mugqic.done
)
macs2_callpeak_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_7_JOB_ID: macs2_callpeak.POL2_shNIPBL-5_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shNIPBL-5_Dex
JOB_DEPENDENCIES=$picard_mark_duplicates_6_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shNIPBL-5_Dex.a083629576de2343b4c5afe22e90eb72.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shNIPBL-5_Dex.a083629576de2343b4c5afe22e90eb72.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shNIPBL-5_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shNIPBL-5_Dex_Rep1/POL2_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-5_Dex_Rep1/WCE_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shNIPBL-5_Dex/POL2_shNIPBL-5_Dex \
  >& peak_call/POL2_shNIPBL-5_Dex/POL2_shNIPBL-5_Dex.diag.macs.out
macs2_callpeak.POL2_shNIPBL-5_Dex.a083629576de2343b4c5afe22e90eb72.mugqic.done
)
macs2_callpeak_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_8_JOB_ID: macs2_callpeak.POL2_shNIPBL_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shNIPBL_Dex
JOB_DEPENDENCIES=$picard_mark_duplicates_6_JOB_ID:$picard_mark_duplicates_7_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shNIPBL_Dex.c588d05b64cb5cef55d0cdb49a82526f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shNIPBL_Dex.c588d05b64cb5cef55d0cdb49a82526f.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shNIPBL_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shNIPBL-3_Dex_Rep1/POL2_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  alignment/POL2_shNIPBL-5_Dex_Rep1/POL2_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_Dex_Rep1/WCE_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  alignment/WCE_shNIPBL-5_Dex_Rep1/WCE_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shNIPBL_Dex/POL2_shNIPBL_Dex \
  >& peak_call/POL2_shNIPBL_Dex/POL2_shNIPBL_Dex.diag.macs.out
macs2_callpeak.POL2_shNIPBL_Dex.c588d05b64cb5cef55d0cdb49a82526f.mugqic.done
)
macs2_callpeak_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_9_JOB_ID: macs2_callpeak.POL2_shNIPBL-3_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shNIPBL-3_EtOH
JOB_DEPENDENCIES=$picard_mark_duplicates_5_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shNIPBL-3_EtOH.8ca44a0b913167ee0cba25206b687df9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shNIPBL-3_EtOH.8ca44a0b913167ee0cba25206b687df9.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shNIPBL-3_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shNIPBL-3_EtOH_Rep1/POL2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shNIPBL-3_EtOH/POL2_shNIPBL-3_EtOH \
  >& peak_call/POL2_shNIPBL-3_EtOH/POL2_shNIPBL-3_EtOH.diag.macs.out
macs2_callpeak.POL2_shNIPBL-3_EtOH.8ca44a0b913167ee0cba25206b687df9.mugqic.done
)
macs2_callpeak_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_10_JOB_ID: macs2_callpeak.POL2_shNIPBL-5_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shNIPBL-5_EtOH
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shNIPBL-5_EtOH.1af248ac3a19b49c30326afad189550f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shNIPBL-5_EtOH.1af248ac3a19b49c30326afad189550f.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shNIPBL-5_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shNIPBL-5_EtOH_Rep1/POL2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shNIPBL-5_EtOH/POL2_shNIPBL-5_EtOH \
  >& peak_call/POL2_shNIPBL-5_EtOH/POL2_shNIPBL-5_EtOH.diag.macs.out
macs2_callpeak.POL2_shNIPBL-5_EtOH.1af248ac3a19b49c30326afad189550f.mugqic.done
)
macs2_callpeak_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_11_JOB_ID: macs2_callpeak.POL2_shNIPBL_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shNIPBL_EtOH
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID:$picard_mark_duplicates_5_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shNIPBL_EtOH.3a7cc5c9c25f3c89d127077b10261035.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shNIPBL_EtOH.3a7cc5c9c25f3c89d127077b10261035.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shNIPBL_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shNIPBL-3_EtOH_Rep1/POL2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  alignment/POL2_shNIPBL-5_EtOH_Rep1/POL2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  alignment/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shNIPBL_EtOH/POL2_shNIPBL_EtOH \
  >& peak_call/POL2_shNIPBL_EtOH/POL2_shNIPBL_EtOH.diag.macs.out
macs2_callpeak.POL2_shNIPBL_EtOH.3a7cc5c9c25f3c89d127077b10261035.mugqic.done
)
macs2_callpeak_11_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_12_JOB_ID: macs2_callpeak.POL2-ser2_shCRTL-1_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shCRTL-1_Dex
JOB_DEPENDENCIES=$picard_mark_duplicates_2_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shCRTL-1_Dex.18a8beb7652f1c62c60350855e6af7d2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shCRTL-1_Dex.18a8beb7652f1c62c60350855e6af7d2.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shCRTL-1_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shCRTL-1_Dex_Rep1/POL2-ser2_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCRTL-1_Dex_Rep1/WCE_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shCRTL-1_Dex/POL2-ser2_shCRTL-1_Dex \
  >& peak_call/POL2-ser2_shCRTL-1_Dex/POL2-ser2_shCRTL-1_Dex.diag.macs.out
macs2_callpeak.POL2-ser2_shCRTL-1_Dex.18a8beb7652f1c62c60350855e6af7d2.mugqic.done
)
macs2_callpeak_12_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_13_JOB_ID: macs2_callpeak.POL2-ser2_shCTRL-2_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shCTRL-2_Dex
JOB_DEPENDENCIES=$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shCTRL-2_Dex.19b884ecfa2fa6b092e10380b5b6dd6e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shCTRL-2_Dex.19b884ecfa2fa6b092e10380b5b6dd6e.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shCTRL-2_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shCTRL-2_Dex_Rep1/POL2-ser2_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shCTRL-2_Dex/POL2-ser2_shCTRL-2_Dex \
  >& peak_call/POL2-ser2_shCTRL-2_Dex/POL2-ser2_shCTRL-2_Dex.diag.macs.out
macs2_callpeak.POL2-ser2_shCTRL-2_Dex.19b884ecfa2fa6b092e10380b5b6dd6e.mugqic.done
)
macs2_callpeak_13_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_14_JOB_ID: macs2_callpeak.POL2-ser2_shCRTL_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shCRTL_Dex
JOB_DEPENDENCIES=$picard_mark_duplicates_2_JOB_ID:$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shCRTL_Dex.013b0065f228246f761cae747876a36d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shCRTL_Dex.013b0065f228246f761cae747876a36d.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shCRTL_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shCRTL-1_Dex_Rep1/POL2-ser2_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  alignment/POL2-ser2_shCTRL-2_Dex_Rep1/POL2-ser2_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCRTL-1_Dex_Rep1/WCE_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  alignment/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shCRTL_Dex/POL2-ser2_shCRTL_Dex \
  >& peak_call/POL2-ser2_shCRTL_Dex/POL2-ser2_shCRTL_Dex.diag.macs.out
macs2_callpeak.POL2-ser2_shCRTL_Dex.013b0065f228246f761cae747876a36d.mugqic.done
)
macs2_callpeak_14_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_15_JOB_ID: macs2_callpeak.POL2-ser2_shCRTL-1_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shCRTL-1_EtOH
JOB_DEPENDENCIES=$picard_mark_duplicates_3_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shCRTL-1_EtOH.be54006cc994c21aacc3c0bdab8eb668.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shCRTL-1_EtOH.be54006cc994c21aacc3c0bdab8eb668.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shCRTL-1_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shCRTL-1_EtOH_Rep1/POL2-ser2_shCRTL-1_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCTRL-1_EtOH_Rep1/WCE_shCTRL-1_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shCRTL-1_EtOH/POL2-ser2_shCRTL-1_EtOH \
  >& peak_call/POL2-ser2_shCRTL-1_EtOH/POL2-ser2_shCRTL-1_EtOH.diag.macs.out
macs2_callpeak.POL2-ser2_shCRTL-1_EtOH.be54006cc994c21aacc3c0bdab8eb668.mugqic.done
)
macs2_callpeak_15_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_16_JOB_ID: macs2_callpeak.POL2-ser2_shCTRL-2_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shCTRL-2_EtOH
JOB_DEPENDENCIES=$picard_mark_duplicates_8_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shCTRL-2_EtOH.358bf0d433abdfba65cc6d772fddfccb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shCTRL-2_EtOH.358bf0d433abdfba65cc6d772fddfccb.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shCTRL-2_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shCTRL-2_EtOH_Rep1/POL2-ser2_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCTRL-2_EtOH_Rep1/WCE_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shCTRL-2_EtOH/POL2-ser2_shCTRL-2_EtOH \
  >& peak_call/POL2-ser2_shCTRL-2_EtOH/POL2-ser2_shCTRL-2_EtOH.diag.macs.out
macs2_callpeak.POL2-ser2_shCTRL-2_EtOH.358bf0d433abdfba65cc6d772fddfccb.mugqic.done
)
macs2_callpeak_16_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_17_JOB_ID: macs2_callpeak.POL2-ser2_shCTRL_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shCTRL_EtOH
JOB_DEPENDENCIES=$picard_mark_duplicates_3_JOB_ID:$picard_mark_duplicates_8_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shCTRL_EtOH.89249683902f156263743d469a14c9be.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shCTRL_EtOH.89249683902f156263743d469a14c9be.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shCTRL_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shCRTL-1_EtOH_Rep1/POL2-ser2_shCRTL-1_EtOH_Rep1.sorted.dup.bam \
  alignment/POL2-ser2_shCTRL-2_EtOH_Rep1/POL2-ser2_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCTRL-1_EtOH_Rep1/WCE_shCTRL-1_EtOH_Rep1.sorted.dup.bam \
  alignment/WCE_shCTRL-2_EtOH_Rep1/WCE_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shCTRL_EtOH/POL2-ser2_shCTRL_EtOH \
  >& peak_call/POL2-ser2_shCTRL_EtOH/POL2-ser2_shCTRL_EtOH.diag.macs.out
macs2_callpeak.POL2-ser2_shCTRL_EtOH.89249683902f156263743d469a14c9be.mugqic.done
)
macs2_callpeak_17_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_18_JOB_ID: macs2_callpeak.POL2-ser2_shNIPBL-3_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shNIPBL-3_Dex
JOB_DEPENDENCIES=$picard_mark_duplicates_7_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shNIPBL-3_Dex.99da582611fadc98c28891af3ae2b261.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shNIPBL-3_Dex.99da582611fadc98c28891af3ae2b261.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shNIPBL-3_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shNIPBL-3_Dex_Rep1/POL2-ser2_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_Dex_Rep1/WCE_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shNIPBL-3_Dex/POL2-ser2_shNIPBL-3_Dex \
  >& peak_call/POL2-ser2_shNIPBL-3_Dex/POL2-ser2_shNIPBL-3_Dex.diag.macs.out
macs2_callpeak.POL2-ser2_shNIPBL-3_Dex.99da582611fadc98c28891af3ae2b261.mugqic.done
)
macs2_callpeak_18_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_19_JOB_ID: macs2_callpeak.POL2-ser2_shNIPBL-5_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shNIPBL-5_Dex
JOB_DEPENDENCIES=$picard_mark_duplicates_6_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shNIPBL-5_Dex.a916cb74bfdac8adf04009f37ed2a870.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shNIPBL-5_Dex.a916cb74bfdac8adf04009f37ed2a870.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shNIPBL-5_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shNIPBL-5_Dex_Rep1/POL2-ser2_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-5_Dex_Rep1/WCE_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shNIPBL-5_Dex/POL2-ser2_shNIPBL-5_Dex \
  >& peak_call/POL2-ser2_shNIPBL-5_Dex/POL2-ser2_shNIPBL-5_Dex.diag.macs.out
macs2_callpeak.POL2-ser2_shNIPBL-5_Dex.a916cb74bfdac8adf04009f37ed2a870.mugqic.done
)
macs2_callpeak_19_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_20_JOB_ID: macs2_callpeak.POL2-ser2_shNIPBL_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shNIPBL_Dex
JOB_DEPENDENCIES=$picard_mark_duplicates_6_JOB_ID:$picard_mark_duplicates_7_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shNIPBL_Dex.b7f7e4efd3a7720a5b02e44411ee93bd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shNIPBL_Dex.b7f7e4efd3a7720a5b02e44411ee93bd.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shNIPBL_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shNIPBL-3_Dex_Rep1/POL2-ser2_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  alignment/POL2-ser2_shNIPBL-5_Dex_Rep1/POL2-ser2_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_Dex_Rep1/WCE_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  alignment/WCE_shNIPBL-5_Dex_Rep1/WCE_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shNIPBL_Dex/POL2-ser2_shNIPBL_Dex \
  >& peak_call/POL2-ser2_shNIPBL_Dex/POL2-ser2_shNIPBL_Dex.diag.macs.out
macs2_callpeak.POL2-ser2_shNIPBL_Dex.b7f7e4efd3a7720a5b02e44411ee93bd.mugqic.done
)
macs2_callpeak_20_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_21_JOB_ID: macs2_callpeak.POL2-ser2_shNIPBL-3_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shNIPBL-3_EtOH
JOB_DEPENDENCIES=$picard_mark_duplicates_5_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shNIPBL-3_EtOH.d5def451d559f265d46fd0a2adae9cdb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shNIPBL-3_EtOH.d5def451d559f265d46fd0a2adae9cdb.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shNIPBL-3_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shNIPBL-3_EtOH_Rep1/POL2-ser2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shNIPBL-3_EtOH/POL2-ser2_shNIPBL-3_EtOH \
  >& peak_call/POL2-ser2_shNIPBL-3_EtOH/POL2-ser2_shNIPBL-3_EtOH.diag.macs.out
macs2_callpeak.POL2-ser2_shNIPBL-3_EtOH.d5def451d559f265d46fd0a2adae9cdb.mugqic.done
)
macs2_callpeak_21_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_21_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_22_JOB_ID: macs2_callpeak.POL2-ser2_shNIPBL-5_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shNIPBL-5_EtOH
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shNIPBL-5_EtOH.2383150f5251ccc8a27d665464764129.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shNIPBL-5_EtOH.2383150f5251ccc8a27d665464764129.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shNIPBL-5_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shNIPBL-5_EtOH_Rep1/POL2-ser2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shNIPBL-5_EtOH/POL2-ser2_shNIPBL-5_EtOH \
  >& peak_call/POL2-ser2_shNIPBL-5_EtOH/POL2-ser2_shNIPBL-5_EtOH.diag.macs.out
macs2_callpeak.POL2-ser2_shNIPBL-5_EtOH.2383150f5251ccc8a27d665464764129.mugqic.done
)
macs2_callpeak_22_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_22_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_23_JOB_ID: macs2_callpeak.POL2-ser2_shNIPBL_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shNIPBL_EtOH
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID:$picard_mark_duplicates_5_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shNIPBL_EtOH.9bbdc0832284cf0c52346f88dd103a4f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shNIPBL_EtOH.9bbdc0832284cf0c52346f88dd103a4f.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shNIPBL_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shNIPBL-3_EtOH_Rep1/POL2-ser2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  alignment/POL2-ser2_shNIPBL-5_EtOH_Rep1/POL2-ser2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  alignment/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shNIPBL_EtOH/POL2-ser2_shNIPBL_EtOH \
  >& peak_call/POL2-ser2_shNIPBL_EtOH/POL2-ser2_shNIPBL_EtOH.diag.macs.out
macs2_callpeak.POL2-ser2_shNIPBL_EtOH.9bbdc0832284cf0c52346f88dd103a4f.mugqic.done
)
macs2_callpeak_23_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_23_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_24_JOB_ID: macs2_callpeak_report
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_report
JOB_DEPENDENCIES=$macs2_callpeak_1_JOB_ID:$macs2_callpeak_2_JOB_ID:$macs2_callpeak_3_JOB_ID:$macs2_callpeak_4_JOB_ID:$macs2_callpeak_5_JOB_ID:$macs2_callpeak_6_JOB_ID:$macs2_callpeak_7_JOB_ID:$macs2_callpeak_8_JOB_ID:$macs2_callpeak_9_JOB_ID:$macs2_callpeak_10_JOB_ID:$macs2_callpeak_11_JOB_ID:$macs2_callpeak_12_JOB_ID:$macs2_callpeak_13_JOB_ID:$macs2_callpeak_14_JOB_ID:$macs2_callpeak_15_JOB_ID:$macs2_callpeak_16_JOB_ID:$macs2_callpeak_17_JOB_ID:$macs2_callpeak_18_JOB_ID:$macs2_callpeak_19_JOB_ID:$macs2_callpeak_20_JOB_ID:$macs2_callpeak_21_JOB_ID:$macs2_callpeak_22_JOB_ID:$macs2_callpeak_23_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_report.0dc265f315ad60373ac2773cf09e4052.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_report.0dc265f315ad60373ac2773cf09e4052.mugqic.done'
mkdir -p report && \
cp /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.macs2_callpeak.md report/ && \
for contrast in POL2_shCRTL-1_Dex POL2_shCTRL-2_Dex POL2_shCRTL_Dex POL2_shCRTL-1_EtOH POL2_shCTRL-2_EtOH POL2_shCTRL_EtOH POL2_shNIPBL-3_Dex POL2_shNIPBL-5_Dex POL2_shNIPBL_Dex POL2_shNIPBL-3_EtOH POL2_shNIPBL-5_EtOH POL2_shNIPBL_EtOH POL2-ser2_shCRTL-1_Dex POL2-ser2_shCTRL-2_Dex POL2-ser2_shCRTL_Dex POL2-ser2_shCRTL-1_EtOH POL2-ser2_shCTRL-2_EtOH POL2-ser2_shCTRL_EtOH POL2-ser2_shNIPBL-3_Dex POL2-ser2_shNIPBL-5_Dex POL2-ser2_shNIPBL_Dex POL2-ser2_shNIPBL-3_EtOH POL2-ser2_shNIPBL-5_EtOH POL2-ser2_shNIPBL_EtOH
do
  cp -a --parents peak_call/$contrast/ report/ && \
  echo -e "* [Peak Calls File for Design $contrast](peak_call/$contrast/${contrast}_peaks.xls)" \
  >> report/ChipSeq.macs2_callpeak.md
done
macs2_callpeak_report.0dc265f315ad60373ac2773cf09e4052.mugqic.done
)
macs2_callpeak_24_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_24_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# Call home with pipeline statistics
#-------------------------------------------------------------------------------
wget "http://mugqic.hpc.mcgill.ca/cgi-bin/pipeline.cgi?hostname=lg-1r17-n03&ip=10.241.129.13&pipeline=ChipSeq&steps=picard_sam_to_fastq,trimmomatic,merge_trimmomatic_stats,bwa_mem_picard_sort_sam,samtools_view_filter,picard_merge_sam_files,picard_mark_duplicates,metrics,homer_make_tag_directory,qc_metrics,homer_make_ucsc_file,macs2_callpeak&samples=30" --quiet --output-document=/dev/null

