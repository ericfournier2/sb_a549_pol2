#!/bin/bash
# Exit immediately on error
set -eu -o pipefail

#-------------------------------------------------------------------------------
# ChipSeq SlurmScheduler Job Submission Bash script
# Version: 3.0.1-beta
# Created on: 2018-03-16T19:56:09
# Steps:
#   macs2_callpeak: 73 jobs
#   TOTAL: 73 jobs
#-------------------------------------------------------------------------------

OUTPUT_DIR=/project/6001942/Working_Directory/Eric/A549_Michele/output/chip-pipeline-GRCh38
JOB_OUTPUT_DIR=$OUTPUT_DIR/job_output
TIMESTAMP=`date +%FT%H.%M.%S`
JOB_LIST=$JOB_OUTPUT_DIR/ChipSeq_job_list_$TIMESTAMP
export CONFIG_FILES="/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini,input/chipseq.numpy.bug.ini"
mkdir -p $OUTPUT_DIR
cd $OUTPUT_DIR


#-------------------------------------------------------------------------------
# STEP: macs2_callpeak
#-------------------------------------------------------------------------------
STEP=macs2_callpeak
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_1_JOB_ID: macs2_callpeak.POL2_shCRTL-1_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shCRTL-1_Dex
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shCRTL-1_Dex.4e9ad9ae997bac7325d57dd4b9c2b0fe.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shCRTL-1_Dex.4e9ad9ae997bac7325d57dd4b9c2b0fe.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shCRTL-1_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shCRTL-1_Dex_Rep1/POL2_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCRTL-1_Dex_Rep1/WCE_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shCRTL-1_Dex/POL2_shCRTL-1_Dex \
  >& peak_call/POL2_shCRTL-1_Dex/POL2_shCRTL-1_Dex.diag.macs.out
macs2_callpeak.POL2_shCRTL-1_Dex.4e9ad9ae997bac7325d57dd4b9c2b0fe.mugqic.done
)
macs2_callpeak_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_2_JOB_ID: macs2_callpeak_bigBed.POL2_shCRTL-1_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2_shCRTL-1_Dex
JOB_DEPENDENCIES=$macs2_callpeak_1_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2_shCRTL-1_Dex.03340ab41f9d0ae47da51c38a56754d7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2_shCRTL-1_Dex.03340ab41f9d0ae47da51c38a56754d7.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2_shCRTL-1_Dex/POL2_shCRTL-1_Dex_peaks.broadPeak > peak_call/POL2_shCRTL-1_Dex/POL2_shCRTL-1_Dex_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2_shCRTL-1_Dex/POL2_shCRTL-1_Dex_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2_shCRTL-1_Dex/POL2_shCRTL-1_Dex_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2_shCRTL-1_Dex.03340ab41f9d0ae47da51c38a56754d7.mugqic.done
)
macs2_callpeak_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_3_JOB_ID: macs2_callpeak.POL2_shCTRL-2_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shCTRL-2_Dex
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shCTRL-2_Dex.071f369289fb142ba2937ec6acbd1de1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shCTRL-2_Dex.071f369289fb142ba2937ec6acbd1de1.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shCTRL-2_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shCTRL-2_Dex_Rep1/POL2_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shCTRL-2_Dex/POL2_shCTRL-2_Dex \
  >& peak_call/POL2_shCTRL-2_Dex/POL2_shCTRL-2_Dex.diag.macs.out
macs2_callpeak.POL2_shCTRL-2_Dex.071f369289fb142ba2937ec6acbd1de1.mugqic.done
)
macs2_callpeak_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_4_JOB_ID: macs2_callpeak_bigBed.POL2_shCTRL-2_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2_shCTRL-2_Dex
JOB_DEPENDENCIES=$macs2_callpeak_3_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2_shCTRL-2_Dex.1843056a2ab358a40fa7a8787637799e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2_shCTRL-2_Dex.1843056a2ab358a40fa7a8787637799e.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2_shCTRL-2_Dex/POL2_shCTRL-2_Dex_peaks.broadPeak > peak_call/POL2_shCTRL-2_Dex/POL2_shCTRL-2_Dex_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2_shCTRL-2_Dex/POL2_shCTRL-2_Dex_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2_shCTRL-2_Dex/POL2_shCTRL-2_Dex_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2_shCTRL-2_Dex.1843056a2ab358a40fa7a8787637799e.mugqic.done
)
macs2_callpeak_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_5_JOB_ID: macs2_callpeak.POL2_shCRTL_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shCRTL_Dex
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shCRTL_Dex.a6e9caf4cc4c51761e9499a7c5a49333.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shCRTL_Dex.a6e9caf4cc4c51761e9499a7c5a49333.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shCRTL_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shCRTL-1_Dex_Rep1/POL2_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  alignment/POL2_shCTRL-2_Dex_Rep1/POL2_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCRTL-1_Dex_Rep1/WCE_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  alignment/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shCRTL_Dex/POL2_shCRTL_Dex \
  >& peak_call/POL2_shCRTL_Dex/POL2_shCRTL_Dex.diag.macs.out
macs2_callpeak.POL2_shCRTL_Dex.a6e9caf4cc4c51761e9499a7c5a49333.mugqic.done
)
macs2_callpeak_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_6_JOB_ID: macs2_callpeak_bigBed.POL2_shCRTL_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2_shCRTL_Dex
JOB_DEPENDENCIES=$macs2_callpeak_5_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2_shCRTL_Dex.447219857ebf0f359d1266ef46f97b75.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2_shCRTL_Dex.447219857ebf0f359d1266ef46f97b75.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2_shCRTL_Dex/POL2_shCRTL_Dex_peaks.broadPeak > peak_call/POL2_shCRTL_Dex/POL2_shCRTL_Dex_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2_shCRTL_Dex/POL2_shCRTL_Dex_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2_shCRTL_Dex/POL2_shCRTL_Dex_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2_shCRTL_Dex.447219857ebf0f359d1266ef46f97b75.mugqic.done
)
macs2_callpeak_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_7_JOB_ID: macs2_callpeak.POL2_shCRTL-1_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shCRTL-1_EtOH
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shCRTL-1_EtOH.c8b7d95b3461b4baebc2a71c07629b2b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shCRTL-1_EtOH.c8b7d95b3461b4baebc2a71c07629b2b.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shCRTL-1_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shCRTL-1_EtOH_Rep1/POL2_shCRTL-1_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCTRL-1_EtOH_Rep1/WCE_shCTRL-1_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shCRTL-1_EtOH/POL2_shCRTL-1_EtOH \
  >& peak_call/POL2_shCRTL-1_EtOH/POL2_shCRTL-1_EtOH.diag.macs.out
macs2_callpeak.POL2_shCRTL-1_EtOH.c8b7d95b3461b4baebc2a71c07629b2b.mugqic.done
)
macs2_callpeak_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_8_JOB_ID: macs2_callpeak_bigBed.POL2_shCRTL-1_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2_shCRTL-1_EtOH
JOB_DEPENDENCIES=$macs2_callpeak_7_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2_shCRTL-1_EtOH.2c4154b6a7a871fe0781815d1800e4e8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2_shCRTL-1_EtOH.2c4154b6a7a871fe0781815d1800e4e8.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2_shCRTL-1_EtOH/POL2_shCRTL-1_EtOH_peaks.broadPeak > peak_call/POL2_shCRTL-1_EtOH/POL2_shCRTL-1_EtOH_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2_shCRTL-1_EtOH/POL2_shCRTL-1_EtOH_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2_shCRTL-1_EtOH/POL2_shCRTL-1_EtOH_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2_shCRTL-1_EtOH.2c4154b6a7a871fe0781815d1800e4e8.mugqic.done
)
macs2_callpeak_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_9_JOB_ID: macs2_callpeak.POL2_shCTRL-2_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shCTRL-2_EtOH
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shCTRL-2_EtOH.633327b44d051872057bf88321c6dad3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shCTRL-2_EtOH.633327b44d051872057bf88321c6dad3.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shCTRL-2_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shCTRL-2_EtOH_Rep1/POL2_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCTRL-2_EtOH_Rep1/WCE_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shCTRL-2_EtOH/POL2_shCTRL-2_EtOH \
  >& peak_call/POL2_shCTRL-2_EtOH/POL2_shCTRL-2_EtOH.diag.macs.out
macs2_callpeak.POL2_shCTRL-2_EtOH.633327b44d051872057bf88321c6dad3.mugqic.done
)
macs2_callpeak_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_10_JOB_ID: macs2_callpeak_bigBed.POL2_shCTRL-2_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2_shCTRL-2_EtOH
JOB_DEPENDENCIES=$macs2_callpeak_9_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2_shCTRL-2_EtOH.66d7241acb75f030c76a34375bb576ac.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2_shCTRL-2_EtOH.66d7241acb75f030c76a34375bb576ac.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2_shCTRL-2_EtOH/POL2_shCTRL-2_EtOH_peaks.broadPeak > peak_call/POL2_shCTRL-2_EtOH/POL2_shCTRL-2_EtOH_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2_shCTRL-2_EtOH/POL2_shCTRL-2_EtOH_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2_shCTRL-2_EtOH/POL2_shCTRL-2_EtOH_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2_shCTRL-2_EtOH.66d7241acb75f030c76a34375bb576ac.mugqic.done
)
macs2_callpeak_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_11_JOB_ID: macs2_callpeak.POL2_shCTRL_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shCTRL_EtOH
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shCTRL_EtOH.38995cbfc190b9e8537d21b40e8906ab.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shCTRL_EtOH.38995cbfc190b9e8537d21b40e8906ab.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shCTRL_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shCRTL-1_EtOH_Rep1/POL2_shCRTL-1_EtOH_Rep1.sorted.dup.bam \
  alignment/POL2_shCTRL-2_EtOH_Rep1/POL2_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCTRL-1_EtOH_Rep1/WCE_shCTRL-1_EtOH_Rep1.sorted.dup.bam \
  alignment/WCE_shCTRL-2_EtOH_Rep1/WCE_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shCTRL_EtOH/POL2_shCTRL_EtOH \
  >& peak_call/POL2_shCTRL_EtOH/POL2_shCTRL_EtOH.diag.macs.out
macs2_callpeak.POL2_shCTRL_EtOH.38995cbfc190b9e8537d21b40e8906ab.mugqic.done
)
macs2_callpeak_11_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_12_JOB_ID: macs2_callpeak_bigBed.POL2_shCTRL_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2_shCTRL_EtOH
JOB_DEPENDENCIES=$macs2_callpeak_11_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2_shCTRL_EtOH.fa750d21f398322f66fa3273228f5c14.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2_shCTRL_EtOH.fa750d21f398322f66fa3273228f5c14.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2_shCTRL_EtOH/POL2_shCTRL_EtOH_peaks.broadPeak > peak_call/POL2_shCTRL_EtOH/POL2_shCTRL_EtOH_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2_shCTRL_EtOH/POL2_shCTRL_EtOH_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2_shCTRL_EtOH/POL2_shCTRL_EtOH_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2_shCTRL_EtOH.fa750d21f398322f66fa3273228f5c14.mugqic.done
)
macs2_callpeak_12_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_13_JOB_ID: macs2_callpeak.POL2_shNIPBL-3_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shNIPBL-3_Dex
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shNIPBL-3_Dex.7abe63199c0b192d0f9672cc270f80e9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shNIPBL-3_Dex.7abe63199c0b192d0f9672cc270f80e9.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shNIPBL-3_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shNIPBL-3_Dex_Rep1/POL2_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_Dex_Rep1/WCE_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shNIPBL-3_Dex/POL2_shNIPBL-3_Dex \
  >& peak_call/POL2_shNIPBL-3_Dex/POL2_shNIPBL-3_Dex.diag.macs.out
macs2_callpeak.POL2_shNIPBL-3_Dex.7abe63199c0b192d0f9672cc270f80e9.mugqic.done
)
macs2_callpeak_13_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_14_JOB_ID: macs2_callpeak_bigBed.POL2_shNIPBL-3_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2_shNIPBL-3_Dex
JOB_DEPENDENCIES=$macs2_callpeak_13_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2_shNIPBL-3_Dex.3f7bb31102bb652d4352c02069291392.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2_shNIPBL-3_Dex.3f7bb31102bb652d4352c02069291392.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2_shNIPBL-3_Dex/POL2_shNIPBL-3_Dex_peaks.broadPeak > peak_call/POL2_shNIPBL-3_Dex/POL2_shNIPBL-3_Dex_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2_shNIPBL-3_Dex/POL2_shNIPBL-3_Dex_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2_shNIPBL-3_Dex/POL2_shNIPBL-3_Dex_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2_shNIPBL-3_Dex.3f7bb31102bb652d4352c02069291392.mugqic.done
)
macs2_callpeak_14_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_15_JOB_ID: macs2_callpeak.POL2_shNIPBL-5_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shNIPBL-5_Dex
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shNIPBL-5_Dex.b9f6d9fa7e924527299600be42c5c101.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shNIPBL-5_Dex.b9f6d9fa7e924527299600be42c5c101.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shNIPBL-5_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shNIPBL-5_Dex_Rep1/POL2_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-5_Dex_Rep1/WCE_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shNIPBL-5_Dex/POL2_shNIPBL-5_Dex \
  >& peak_call/POL2_shNIPBL-5_Dex/POL2_shNIPBL-5_Dex.diag.macs.out
macs2_callpeak.POL2_shNIPBL-5_Dex.b9f6d9fa7e924527299600be42c5c101.mugqic.done
)
macs2_callpeak_15_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_16_JOB_ID: macs2_callpeak_bigBed.POL2_shNIPBL-5_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2_shNIPBL-5_Dex
JOB_DEPENDENCIES=$macs2_callpeak_15_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2_shNIPBL-5_Dex.19edd8c0ef22e481f9ff1bde81dbdcd5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2_shNIPBL-5_Dex.19edd8c0ef22e481f9ff1bde81dbdcd5.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2_shNIPBL-5_Dex/POL2_shNIPBL-5_Dex_peaks.broadPeak > peak_call/POL2_shNIPBL-5_Dex/POL2_shNIPBL-5_Dex_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2_shNIPBL-5_Dex/POL2_shNIPBL-5_Dex_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2_shNIPBL-5_Dex/POL2_shNIPBL-5_Dex_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2_shNIPBL-5_Dex.19edd8c0ef22e481f9ff1bde81dbdcd5.mugqic.done
)
macs2_callpeak_16_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_17_JOB_ID: macs2_callpeak.POL2_shNIPBL_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shNIPBL_Dex
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shNIPBL_Dex.6ff1dd312e710202d653e794ab0af235.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shNIPBL_Dex.6ff1dd312e710202d653e794ab0af235.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shNIPBL_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shNIPBL-3_Dex_Rep1/POL2_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  alignment/POL2_shNIPBL-5_Dex_Rep1/POL2_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_Dex_Rep1/WCE_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  alignment/WCE_shNIPBL-5_Dex_Rep1/WCE_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shNIPBL_Dex/POL2_shNIPBL_Dex \
  >& peak_call/POL2_shNIPBL_Dex/POL2_shNIPBL_Dex.diag.macs.out
macs2_callpeak.POL2_shNIPBL_Dex.6ff1dd312e710202d653e794ab0af235.mugqic.done
)
macs2_callpeak_17_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_18_JOB_ID: macs2_callpeak_bigBed.POL2_shNIPBL_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2_shNIPBL_Dex
JOB_DEPENDENCIES=$macs2_callpeak_17_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2_shNIPBL_Dex.dce0c270c8ed51047818808c0833171e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2_shNIPBL_Dex.dce0c270c8ed51047818808c0833171e.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2_shNIPBL_Dex/POL2_shNIPBL_Dex_peaks.broadPeak > peak_call/POL2_shNIPBL_Dex/POL2_shNIPBL_Dex_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2_shNIPBL_Dex/POL2_shNIPBL_Dex_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2_shNIPBL_Dex/POL2_shNIPBL_Dex_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2_shNIPBL_Dex.dce0c270c8ed51047818808c0833171e.mugqic.done
)
macs2_callpeak_18_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_19_JOB_ID: macs2_callpeak.POL2_shNIPBL-3_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shNIPBL-3_EtOH
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shNIPBL-3_EtOH.e3282327fd5295fa1a0b90ea1ab0316f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shNIPBL-3_EtOH.e3282327fd5295fa1a0b90ea1ab0316f.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shNIPBL-3_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shNIPBL-3_EtOH_Rep1/POL2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shNIPBL-3_EtOH/POL2_shNIPBL-3_EtOH \
  >& peak_call/POL2_shNIPBL-3_EtOH/POL2_shNIPBL-3_EtOH.diag.macs.out
macs2_callpeak.POL2_shNIPBL-3_EtOH.e3282327fd5295fa1a0b90ea1ab0316f.mugqic.done
)
macs2_callpeak_19_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_20_JOB_ID: macs2_callpeak_bigBed.POL2_shNIPBL-3_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2_shNIPBL-3_EtOH
JOB_DEPENDENCIES=$macs2_callpeak_19_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2_shNIPBL-3_EtOH.f5302b8c220a93ed27bd9e41b7a2d4f8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2_shNIPBL-3_EtOH.f5302b8c220a93ed27bd9e41b7a2d4f8.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2_shNIPBL-3_EtOH/POL2_shNIPBL-3_EtOH_peaks.broadPeak > peak_call/POL2_shNIPBL-3_EtOH/POL2_shNIPBL-3_EtOH_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2_shNIPBL-3_EtOH/POL2_shNIPBL-3_EtOH_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2_shNIPBL-3_EtOH/POL2_shNIPBL-3_EtOH_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2_shNIPBL-3_EtOH.f5302b8c220a93ed27bd9e41b7a2d4f8.mugqic.done
)
macs2_callpeak_20_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_21_JOB_ID: macs2_callpeak.POL2_shNIPBL-5_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shNIPBL-5_EtOH
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shNIPBL-5_EtOH.45b7afc853a64769c89324c21fd5159e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shNIPBL-5_EtOH.45b7afc853a64769c89324c21fd5159e.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shNIPBL-5_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shNIPBL-5_EtOH_Rep1/POL2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shNIPBL-5_EtOH/POL2_shNIPBL-5_EtOH \
  >& peak_call/POL2_shNIPBL-5_EtOH/POL2_shNIPBL-5_EtOH.diag.macs.out
macs2_callpeak.POL2_shNIPBL-5_EtOH.45b7afc853a64769c89324c21fd5159e.mugqic.done
)
macs2_callpeak_21_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_21_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_22_JOB_ID: macs2_callpeak_bigBed.POL2_shNIPBL-5_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2_shNIPBL-5_EtOH
JOB_DEPENDENCIES=$macs2_callpeak_21_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2_shNIPBL-5_EtOH.71ef1d8173ffb3dfa255e00052a15933.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2_shNIPBL-5_EtOH.71ef1d8173ffb3dfa255e00052a15933.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2_shNIPBL-5_EtOH/POL2_shNIPBL-5_EtOH_peaks.broadPeak > peak_call/POL2_shNIPBL-5_EtOH/POL2_shNIPBL-5_EtOH_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2_shNIPBL-5_EtOH/POL2_shNIPBL-5_EtOH_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2_shNIPBL-5_EtOH/POL2_shNIPBL-5_EtOH_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2_shNIPBL-5_EtOH.71ef1d8173ffb3dfa255e00052a15933.mugqic.done
)
macs2_callpeak_22_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_22_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_23_JOB_ID: macs2_callpeak.POL2_shNIPBL_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2_shNIPBL_EtOH
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2_shNIPBL_EtOH.21748b442c553eb3bf636fd0ce8305a2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2_shNIPBL_EtOH.21748b442c553eb3bf636fd0ce8305a2.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2_shNIPBL_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2_shNIPBL-3_EtOH_Rep1/POL2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  alignment/POL2_shNIPBL-5_EtOH_Rep1/POL2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  alignment/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2_shNIPBL_EtOH/POL2_shNIPBL_EtOH \
  >& peak_call/POL2_shNIPBL_EtOH/POL2_shNIPBL_EtOH.diag.macs.out
macs2_callpeak.POL2_shNIPBL_EtOH.21748b442c553eb3bf636fd0ce8305a2.mugqic.done
)
macs2_callpeak_23_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_23_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_24_JOB_ID: macs2_callpeak_bigBed.POL2_shNIPBL_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2_shNIPBL_EtOH
JOB_DEPENDENCIES=$macs2_callpeak_23_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2_shNIPBL_EtOH.64698f068a96ea65bbe95758a02aefb9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2_shNIPBL_EtOH.64698f068a96ea65bbe95758a02aefb9.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2_shNIPBL_EtOH/POL2_shNIPBL_EtOH_peaks.broadPeak > peak_call/POL2_shNIPBL_EtOH/POL2_shNIPBL_EtOH_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2_shNIPBL_EtOH/POL2_shNIPBL_EtOH_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2_shNIPBL_EtOH/POL2_shNIPBL_EtOH_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2_shNIPBL_EtOH.64698f068a96ea65bbe95758a02aefb9.mugqic.done
)
macs2_callpeak_24_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_24_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_25_JOB_ID: macs2_callpeak.POL2-ser2_shCRTL-1_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shCRTL-1_Dex
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shCRTL-1_Dex.bd1c38cffd959a6dc370c5c6a362cd08.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shCRTL-1_Dex.bd1c38cffd959a6dc370c5c6a362cd08.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shCRTL-1_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shCRTL-1_Dex_Rep1/POL2-ser2_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCRTL-1_Dex_Rep1/WCE_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shCRTL-1_Dex/POL2-ser2_shCRTL-1_Dex \
  >& peak_call/POL2-ser2_shCRTL-1_Dex/POL2-ser2_shCRTL-1_Dex.diag.macs.out
macs2_callpeak.POL2-ser2_shCRTL-1_Dex.bd1c38cffd959a6dc370c5c6a362cd08.mugqic.done
)
macs2_callpeak_25_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_25_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_26_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shCRTL-1_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shCRTL-1_Dex
JOB_DEPENDENCIES=$macs2_callpeak_25_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shCRTL-1_Dex.a2e43be62cc8ff94d04f0fa7df615caf.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shCRTL-1_Dex.a2e43be62cc8ff94d04f0fa7df615caf.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shCRTL-1_Dex/POL2-ser2_shCRTL-1_Dex_peaks.broadPeak > peak_call/POL2-ser2_shCRTL-1_Dex/POL2-ser2_shCRTL-1_Dex_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shCRTL-1_Dex/POL2-ser2_shCRTL-1_Dex_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shCRTL-1_Dex/POL2-ser2_shCRTL-1_Dex_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shCRTL-1_Dex.a2e43be62cc8ff94d04f0fa7df615caf.mugqic.done
)
macs2_callpeak_26_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_26_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_27_JOB_ID: macs2_callpeak.POL2-ser2_shCTRL-2_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shCTRL-2_Dex
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shCTRL-2_Dex.01f9c0eac73d6564bd7a2164c5ef549b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shCTRL-2_Dex.01f9c0eac73d6564bd7a2164c5ef549b.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shCTRL-2_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shCTRL-2_Dex_Rep1/POL2-ser2_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shCTRL-2_Dex/POL2-ser2_shCTRL-2_Dex \
  >& peak_call/POL2-ser2_shCTRL-2_Dex/POL2-ser2_shCTRL-2_Dex.diag.macs.out
macs2_callpeak.POL2-ser2_shCTRL-2_Dex.01f9c0eac73d6564bd7a2164c5ef549b.mugqic.done
)
macs2_callpeak_27_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_27_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_28_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shCTRL-2_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shCTRL-2_Dex
JOB_DEPENDENCIES=$macs2_callpeak_27_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shCTRL-2_Dex.ca6e501bb307f9476026b85d50d1ccda.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shCTRL-2_Dex.ca6e501bb307f9476026b85d50d1ccda.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shCTRL-2_Dex/POL2-ser2_shCTRL-2_Dex_peaks.broadPeak > peak_call/POL2-ser2_shCTRL-2_Dex/POL2-ser2_shCTRL-2_Dex_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shCTRL-2_Dex/POL2-ser2_shCTRL-2_Dex_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shCTRL-2_Dex/POL2-ser2_shCTRL-2_Dex_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shCTRL-2_Dex.ca6e501bb307f9476026b85d50d1ccda.mugqic.done
)
macs2_callpeak_28_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_28_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_29_JOB_ID: macs2_callpeak.POL2-ser2_shCRTL_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shCRTL_Dex
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shCRTL_Dex.06fac6046ec038ce6732bae14c61bfd0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shCRTL_Dex.06fac6046ec038ce6732bae14c61bfd0.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shCRTL_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shCRTL-1_Dex_Rep1/POL2-ser2_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  alignment/POL2-ser2_shCTRL-2_Dex_Rep1/POL2-ser2_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCRTL-1_Dex_Rep1/WCE_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  alignment/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shCRTL_Dex/POL2-ser2_shCRTL_Dex \
  >& peak_call/POL2-ser2_shCRTL_Dex/POL2-ser2_shCRTL_Dex.diag.macs.out
macs2_callpeak.POL2-ser2_shCRTL_Dex.06fac6046ec038ce6732bae14c61bfd0.mugqic.done
)
macs2_callpeak_29_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_29_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_30_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shCRTL_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shCRTL_Dex
JOB_DEPENDENCIES=$macs2_callpeak_29_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shCRTL_Dex.d6fdd1f781adea53a792b74f7ee9f4ee.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shCRTL_Dex.d6fdd1f781adea53a792b74f7ee9f4ee.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shCRTL_Dex/POL2-ser2_shCRTL_Dex_peaks.broadPeak > peak_call/POL2-ser2_shCRTL_Dex/POL2-ser2_shCRTL_Dex_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shCRTL_Dex/POL2-ser2_shCRTL_Dex_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shCRTL_Dex/POL2-ser2_shCRTL_Dex_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shCRTL_Dex.d6fdd1f781adea53a792b74f7ee9f4ee.mugqic.done
)
macs2_callpeak_30_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_30_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_31_JOB_ID: macs2_callpeak.POL2-ser2_shCRTL-1_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shCRTL-1_EtOH
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shCRTL-1_EtOH.c52dfe945a02d00108554b727fa45bd6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shCRTL-1_EtOH.c52dfe945a02d00108554b727fa45bd6.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shCRTL-1_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shCRTL-1_EtOH_Rep1/POL2-ser2_shCRTL-1_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCTRL-1_EtOH_Rep1/WCE_shCTRL-1_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shCRTL-1_EtOH/POL2-ser2_shCRTL-1_EtOH \
  >& peak_call/POL2-ser2_shCRTL-1_EtOH/POL2-ser2_shCRTL-1_EtOH.diag.macs.out
macs2_callpeak.POL2-ser2_shCRTL-1_EtOH.c52dfe945a02d00108554b727fa45bd6.mugqic.done
)
macs2_callpeak_31_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_31_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_32_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shCRTL-1_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shCRTL-1_EtOH
JOB_DEPENDENCIES=$macs2_callpeak_31_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shCRTL-1_EtOH.b2b9055e67dce71174bcd6e85dac2583.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shCRTL-1_EtOH.b2b9055e67dce71174bcd6e85dac2583.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shCRTL-1_EtOH/POL2-ser2_shCRTL-1_EtOH_peaks.broadPeak > peak_call/POL2-ser2_shCRTL-1_EtOH/POL2-ser2_shCRTL-1_EtOH_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shCRTL-1_EtOH/POL2-ser2_shCRTL-1_EtOH_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shCRTL-1_EtOH/POL2-ser2_shCRTL-1_EtOH_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shCRTL-1_EtOH.b2b9055e67dce71174bcd6e85dac2583.mugqic.done
)
macs2_callpeak_32_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_32_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_33_JOB_ID: macs2_callpeak.POL2-ser2_shCTRL-2_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shCTRL-2_EtOH
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shCTRL-2_EtOH.a9e223a125e1c9f772344b3c59a97002.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shCTRL-2_EtOH.a9e223a125e1c9f772344b3c59a97002.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shCTRL-2_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shCTRL-2_EtOH_Rep1/POL2-ser2_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCTRL-2_EtOH_Rep1/WCE_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shCTRL-2_EtOH/POL2-ser2_shCTRL-2_EtOH \
  >& peak_call/POL2-ser2_shCTRL-2_EtOH/POL2-ser2_shCTRL-2_EtOH.diag.macs.out
macs2_callpeak.POL2-ser2_shCTRL-2_EtOH.a9e223a125e1c9f772344b3c59a97002.mugqic.done
)
macs2_callpeak_33_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_33_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_34_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shCTRL-2_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shCTRL-2_EtOH
JOB_DEPENDENCIES=$macs2_callpeak_33_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shCTRL-2_EtOH.e1ab35e93fc35c15380422663518906b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shCTRL-2_EtOH.e1ab35e93fc35c15380422663518906b.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shCTRL-2_EtOH/POL2-ser2_shCTRL-2_EtOH_peaks.broadPeak > peak_call/POL2-ser2_shCTRL-2_EtOH/POL2-ser2_shCTRL-2_EtOH_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shCTRL-2_EtOH/POL2-ser2_shCTRL-2_EtOH_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shCTRL-2_EtOH/POL2-ser2_shCTRL-2_EtOH_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shCTRL-2_EtOH.e1ab35e93fc35c15380422663518906b.mugqic.done
)
macs2_callpeak_34_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_34_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_35_JOB_ID: macs2_callpeak.POL2-ser2_shCTRL_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shCTRL_EtOH
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shCTRL_EtOH.364ca4d6d32c3aac468be9b9097ff2a2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shCTRL_EtOH.364ca4d6d32c3aac468be9b9097ff2a2.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shCTRL_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shCRTL-1_EtOH_Rep1/POL2-ser2_shCRTL-1_EtOH_Rep1.sorted.dup.bam \
  alignment/POL2-ser2_shCTRL-2_EtOH_Rep1/POL2-ser2_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCTRL-1_EtOH_Rep1/WCE_shCTRL-1_EtOH_Rep1.sorted.dup.bam \
  alignment/WCE_shCTRL-2_EtOH_Rep1/WCE_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shCTRL_EtOH/POL2-ser2_shCTRL_EtOH \
  >& peak_call/POL2-ser2_shCTRL_EtOH/POL2-ser2_shCTRL_EtOH.diag.macs.out
macs2_callpeak.POL2-ser2_shCTRL_EtOH.364ca4d6d32c3aac468be9b9097ff2a2.mugqic.done
)
macs2_callpeak_35_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_35_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_36_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shCTRL_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shCTRL_EtOH
JOB_DEPENDENCIES=$macs2_callpeak_35_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shCTRL_EtOH.994ec08ac349536ff7ecf27dfb1ef81b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shCTRL_EtOH.994ec08ac349536ff7ecf27dfb1ef81b.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shCTRL_EtOH/POL2-ser2_shCTRL_EtOH_peaks.broadPeak > peak_call/POL2-ser2_shCTRL_EtOH/POL2-ser2_shCTRL_EtOH_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shCTRL_EtOH/POL2-ser2_shCTRL_EtOH_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shCTRL_EtOH/POL2-ser2_shCTRL_EtOH_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shCTRL_EtOH.994ec08ac349536ff7ecf27dfb1ef81b.mugqic.done
)
macs2_callpeak_36_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_36_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_37_JOB_ID: macs2_callpeak.POL2-ser2_shNIPBL-3_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shNIPBL-3_Dex
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shNIPBL-3_Dex.a202f1b6db6dc6b43dde4646c2c8d3ba.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shNIPBL-3_Dex.a202f1b6db6dc6b43dde4646c2c8d3ba.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shNIPBL-3_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shNIPBL-3_Dex_Rep1/POL2-ser2_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_Dex_Rep1/WCE_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shNIPBL-3_Dex/POL2-ser2_shNIPBL-3_Dex \
  >& peak_call/POL2-ser2_shNIPBL-3_Dex/POL2-ser2_shNIPBL-3_Dex.diag.macs.out
macs2_callpeak.POL2-ser2_shNIPBL-3_Dex.a202f1b6db6dc6b43dde4646c2c8d3ba.mugqic.done
)
macs2_callpeak_37_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_37_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_38_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shNIPBL-3_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shNIPBL-3_Dex
JOB_DEPENDENCIES=$macs2_callpeak_37_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shNIPBL-3_Dex.6a549478c71e10befcc96051d6b3c6f1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shNIPBL-3_Dex.6a549478c71e10befcc96051d6b3c6f1.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shNIPBL-3_Dex/POL2-ser2_shNIPBL-3_Dex_peaks.broadPeak > peak_call/POL2-ser2_shNIPBL-3_Dex/POL2-ser2_shNIPBL-3_Dex_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shNIPBL-3_Dex/POL2-ser2_shNIPBL-3_Dex_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shNIPBL-3_Dex/POL2-ser2_shNIPBL-3_Dex_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shNIPBL-3_Dex.6a549478c71e10befcc96051d6b3c6f1.mugqic.done
)
macs2_callpeak_38_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_38_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_39_JOB_ID: macs2_callpeak.POL2-ser2_shNIPBL-5_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shNIPBL-5_Dex
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shNIPBL-5_Dex.82decd9798f6fb38a3705cc0cdf91153.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shNIPBL-5_Dex.82decd9798f6fb38a3705cc0cdf91153.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shNIPBL-5_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shNIPBL-5_Dex_Rep1/POL2-ser2_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-5_Dex_Rep1/WCE_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shNIPBL-5_Dex/POL2-ser2_shNIPBL-5_Dex \
  >& peak_call/POL2-ser2_shNIPBL-5_Dex/POL2-ser2_shNIPBL-5_Dex.diag.macs.out
macs2_callpeak.POL2-ser2_shNIPBL-5_Dex.82decd9798f6fb38a3705cc0cdf91153.mugqic.done
)
macs2_callpeak_39_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_39_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_40_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shNIPBL-5_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shNIPBL-5_Dex
JOB_DEPENDENCIES=$macs2_callpeak_39_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shNIPBL-5_Dex.8f0b6ea4d160935efbe455a683b7c0b5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shNIPBL-5_Dex.8f0b6ea4d160935efbe455a683b7c0b5.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shNIPBL-5_Dex/POL2-ser2_shNIPBL-5_Dex_peaks.broadPeak > peak_call/POL2-ser2_shNIPBL-5_Dex/POL2-ser2_shNIPBL-5_Dex_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shNIPBL-5_Dex/POL2-ser2_shNIPBL-5_Dex_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shNIPBL-5_Dex/POL2-ser2_shNIPBL-5_Dex_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shNIPBL-5_Dex.8f0b6ea4d160935efbe455a683b7c0b5.mugqic.done
)
macs2_callpeak_40_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_40_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_41_JOB_ID: macs2_callpeak.POL2-ser2_shNIPBL_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shNIPBL_Dex
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shNIPBL_Dex.1db4078d2657686c63ec5df94659fe9b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shNIPBL_Dex.1db4078d2657686c63ec5df94659fe9b.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shNIPBL_Dex && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shNIPBL-3_Dex_Rep1/POL2-ser2_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  alignment/POL2-ser2_shNIPBL-5_Dex_Rep1/POL2-ser2_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_Dex_Rep1/WCE_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  alignment/WCE_shNIPBL-5_Dex_Rep1/WCE_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shNIPBL_Dex/POL2-ser2_shNIPBL_Dex \
  >& peak_call/POL2-ser2_shNIPBL_Dex/POL2-ser2_shNIPBL_Dex.diag.macs.out
macs2_callpeak.POL2-ser2_shNIPBL_Dex.1db4078d2657686c63ec5df94659fe9b.mugqic.done
)
macs2_callpeak_41_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_41_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_42_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shNIPBL_Dex
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shNIPBL_Dex
JOB_DEPENDENCIES=$macs2_callpeak_41_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shNIPBL_Dex.49614cf4a6a756649c4a6a3c706c0e1e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shNIPBL_Dex.49614cf4a6a756649c4a6a3c706c0e1e.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shNIPBL_Dex/POL2-ser2_shNIPBL_Dex_peaks.broadPeak > peak_call/POL2-ser2_shNIPBL_Dex/POL2-ser2_shNIPBL_Dex_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shNIPBL_Dex/POL2-ser2_shNIPBL_Dex_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shNIPBL_Dex/POL2-ser2_shNIPBL_Dex_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shNIPBL_Dex.49614cf4a6a756649c4a6a3c706c0e1e.mugqic.done
)
macs2_callpeak_42_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_42_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_43_JOB_ID: macs2_callpeak.POL2-ser2_shNIPBL-3_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shNIPBL-3_EtOH
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shNIPBL-3_EtOH.1a959fbc4896cf16e8c48df0e665ba81.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shNIPBL-3_EtOH.1a959fbc4896cf16e8c48df0e665ba81.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shNIPBL-3_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shNIPBL-3_EtOH_Rep1/POL2-ser2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shNIPBL-3_EtOH/POL2-ser2_shNIPBL-3_EtOH \
  >& peak_call/POL2-ser2_shNIPBL-3_EtOH/POL2-ser2_shNIPBL-3_EtOH.diag.macs.out
macs2_callpeak.POL2-ser2_shNIPBL-3_EtOH.1a959fbc4896cf16e8c48df0e665ba81.mugqic.done
)
macs2_callpeak_43_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_43_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_44_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shNIPBL-3_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shNIPBL-3_EtOH
JOB_DEPENDENCIES=$macs2_callpeak_43_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shNIPBL-3_EtOH.e23a922a06ad6dba8147c20d97150aa2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shNIPBL-3_EtOH.e23a922a06ad6dba8147c20d97150aa2.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shNIPBL-3_EtOH/POL2-ser2_shNIPBL-3_EtOH_peaks.broadPeak > peak_call/POL2-ser2_shNIPBL-3_EtOH/POL2-ser2_shNIPBL-3_EtOH_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shNIPBL-3_EtOH/POL2-ser2_shNIPBL-3_EtOH_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shNIPBL-3_EtOH/POL2-ser2_shNIPBL-3_EtOH_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shNIPBL-3_EtOH.e23a922a06ad6dba8147c20d97150aa2.mugqic.done
)
macs2_callpeak_44_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_44_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_45_JOB_ID: macs2_callpeak.POL2-ser2_shNIPBL-5_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shNIPBL-5_EtOH
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shNIPBL-5_EtOH.311cfa583c5f3e6edca455e9b46a4f20.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shNIPBL-5_EtOH.311cfa583c5f3e6edca455e9b46a4f20.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shNIPBL-5_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shNIPBL-5_EtOH_Rep1/POL2-ser2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shNIPBL-5_EtOH/POL2-ser2_shNIPBL-5_EtOH \
  >& peak_call/POL2-ser2_shNIPBL-5_EtOH/POL2-ser2_shNIPBL-5_EtOH.diag.macs.out
macs2_callpeak.POL2-ser2_shNIPBL-5_EtOH.311cfa583c5f3e6edca455e9b46a4f20.mugqic.done
)
macs2_callpeak_45_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_45_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_46_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shNIPBL-5_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shNIPBL-5_EtOH
JOB_DEPENDENCIES=$macs2_callpeak_45_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shNIPBL-5_EtOH.2a16ae5fc2e4b9888b8888b625d8d851.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shNIPBL-5_EtOH.2a16ae5fc2e4b9888b8888b625d8d851.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shNIPBL-5_EtOH/POL2-ser2_shNIPBL-5_EtOH_peaks.broadPeak > peak_call/POL2-ser2_shNIPBL-5_EtOH/POL2-ser2_shNIPBL-5_EtOH_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shNIPBL-5_EtOH/POL2-ser2_shNIPBL-5_EtOH_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shNIPBL-5_EtOH/POL2-ser2_shNIPBL-5_EtOH_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shNIPBL-5_EtOH.2a16ae5fc2e4b9888b8888b625d8d851.mugqic.done
)
macs2_callpeak_46_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_46_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_47_JOB_ID: macs2_callpeak.POL2-ser2_shNIPBL_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shNIPBL_EtOH
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shNIPBL_EtOH.d8ab9b8cf1b40e650f4c1cffc1f86d76.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shNIPBL_EtOH.d8ab9b8cf1b40e650f4c1cffc1f86d76.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shNIPBL_EtOH && \
macs2 callpeak --format BAM --broad --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shNIPBL-3_EtOH_Rep1/POL2-ser2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  alignment/POL2-ser2_shNIPBL-5_EtOH_Rep1/POL2-ser2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  alignment/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shNIPBL_EtOH/POL2-ser2_shNIPBL_EtOH \
  >& peak_call/POL2-ser2_shNIPBL_EtOH/POL2-ser2_shNIPBL_EtOH.diag.macs.out
macs2_callpeak.POL2-ser2_shNIPBL_EtOH.d8ab9b8cf1b40e650f4c1cffc1f86d76.mugqic.done
)
macs2_callpeak_47_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_47_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_48_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shNIPBL_EtOH
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shNIPBL_EtOH
JOB_DEPENDENCIES=$macs2_callpeak_47_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shNIPBL_EtOH.eebaa832504857991acaee7b3cb49359.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shNIPBL_EtOH.eebaa832504857991acaee7b3cb49359.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shNIPBL_EtOH/POL2-ser2_shNIPBL_EtOH_peaks.broadPeak > peak_call/POL2-ser2_shNIPBL_EtOH/POL2-ser2_shNIPBL_EtOH_peaks.broadPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shNIPBL_EtOH/POL2-ser2_shNIPBL_EtOH_peaks.broadPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shNIPBL_EtOH/POL2-ser2_shNIPBL_EtOH_peaks.broadPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shNIPBL_EtOH.eebaa832504857991acaee7b3cb49359.mugqic.done
)
macs2_callpeak_48_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_48_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_49_JOB_ID: macs2_callpeak.POL2-ser2_shCRTL-1_DexN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shCRTL-1_DexN
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shCRTL-1_DexN.e96da877d996986277702a1f959d0f6d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shCRTL-1_DexN.e96da877d996986277702a1f959d0f6d.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shCRTL-1_DexN && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shCRTL-1_Dex_Rep1/POL2-ser2_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCRTL-1_Dex_Rep1/WCE_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shCRTL-1_DexN/POL2-ser2_shCRTL-1_DexN \
  >& peak_call/POL2-ser2_shCRTL-1_DexN/POL2-ser2_shCRTL-1_DexN.diag.macs.out
macs2_callpeak.POL2-ser2_shCRTL-1_DexN.e96da877d996986277702a1f959d0f6d.mugqic.done
)
macs2_callpeak_49_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_49_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_50_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shCRTL-1_DexN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shCRTL-1_DexN
JOB_DEPENDENCIES=$macs2_callpeak_49_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shCRTL-1_DexN.ef3bb33dfd768f8ad234b8a3abc229c7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shCRTL-1_DexN.ef3bb33dfd768f8ad234b8a3abc229c7.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shCRTL-1_DexN/POL2-ser2_shCRTL-1_DexN_peaks.narrowPeak > peak_call/POL2-ser2_shCRTL-1_DexN/POL2-ser2_shCRTL-1_DexN_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shCRTL-1_DexN/POL2-ser2_shCRTL-1_DexN_peaks.narrowPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shCRTL-1_DexN/POL2-ser2_shCRTL-1_DexN_peaks.narrowPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shCRTL-1_DexN.ef3bb33dfd768f8ad234b8a3abc229c7.mugqic.done
)
macs2_callpeak_50_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_50_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_51_JOB_ID: macs2_callpeak.POL2-ser2_shCTRL-2_DexN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shCTRL-2_DexN
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shCTRL-2_DexN.dbce6929f9f4a15fe24724d6eefd64bc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shCTRL-2_DexN.dbce6929f9f4a15fe24724d6eefd64bc.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shCTRL-2_DexN && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shCTRL-2_Dex_Rep1/POL2-ser2_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shCTRL-2_DexN/POL2-ser2_shCTRL-2_DexN \
  >& peak_call/POL2-ser2_shCTRL-2_DexN/POL2-ser2_shCTRL-2_DexN.diag.macs.out
macs2_callpeak.POL2-ser2_shCTRL-2_DexN.dbce6929f9f4a15fe24724d6eefd64bc.mugqic.done
)
macs2_callpeak_51_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_51_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_52_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shCTRL-2_DexN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shCTRL-2_DexN
JOB_DEPENDENCIES=$macs2_callpeak_51_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shCTRL-2_DexN.a91055ebc69a02280763b1d2beb825d3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shCTRL-2_DexN.a91055ebc69a02280763b1d2beb825d3.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shCTRL-2_DexN/POL2-ser2_shCTRL-2_DexN_peaks.narrowPeak > peak_call/POL2-ser2_shCTRL-2_DexN/POL2-ser2_shCTRL-2_DexN_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shCTRL-2_DexN/POL2-ser2_shCTRL-2_DexN_peaks.narrowPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shCTRL-2_DexN/POL2-ser2_shCTRL-2_DexN_peaks.narrowPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shCTRL-2_DexN.a91055ebc69a02280763b1d2beb825d3.mugqic.done
)
macs2_callpeak_52_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_52_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_53_JOB_ID: macs2_callpeak.POL2-ser2_shCRTL_DexN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shCRTL_DexN
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shCRTL_DexN.d1c05ae8c02d5c4592058c0c87748904.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shCRTL_DexN.d1c05ae8c02d5c4592058c0c87748904.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shCRTL_DexN && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shCRTL-1_Dex_Rep1/POL2-ser2_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  alignment/POL2-ser2_shCTRL-2_Dex_Rep1/POL2-ser2_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCRTL-1_Dex_Rep1/WCE_shCRTL-1_Dex_Rep1.sorted.dup.bam \
  alignment/WCE_shCTRL-2_Dex_Rep1/WCE_shCTRL-2_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shCRTL_DexN/POL2-ser2_shCRTL_DexN \
  >& peak_call/POL2-ser2_shCRTL_DexN/POL2-ser2_shCRTL_DexN.diag.macs.out
macs2_callpeak.POL2-ser2_shCRTL_DexN.d1c05ae8c02d5c4592058c0c87748904.mugqic.done
)
macs2_callpeak_53_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_53_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_54_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shCRTL_DexN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shCRTL_DexN
JOB_DEPENDENCIES=$macs2_callpeak_53_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shCRTL_DexN.1cf3b924eaaf61215948f83bc9a61f49.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shCRTL_DexN.1cf3b924eaaf61215948f83bc9a61f49.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shCRTL_DexN/POL2-ser2_shCRTL_DexN_peaks.narrowPeak > peak_call/POL2-ser2_shCRTL_DexN/POL2-ser2_shCRTL_DexN_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shCRTL_DexN/POL2-ser2_shCRTL_DexN_peaks.narrowPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shCRTL_DexN/POL2-ser2_shCRTL_DexN_peaks.narrowPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shCRTL_DexN.1cf3b924eaaf61215948f83bc9a61f49.mugqic.done
)
macs2_callpeak_54_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_54_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_55_JOB_ID: macs2_callpeak.POL2-ser2_shCRTL-1_EtOHN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shCRTL-1_EtOHN
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shCRTL-1_EtOHN.aaffae6e9213ce09f3d3b7c65f6ecb4b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shCRTL-1_EtOHN.aaffae6e9213ce09f3d3b7c65f6ecb4b.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shCRTL-1_EtOHN && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shCRTL-1_EtOH_Rep1/POL2-ser2_shCRTL-1_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCTRL-1_EtOH_Rep1/WCE_shCTRL-1_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shCRTL-1_EtOHN/POL2-ser2_shCRTL-1_EtOHN \
  >& peak_call/POL2-ser2_shCRTL-1_EtOHN/POL2-ser2_shCRTL-1_EtOHN.diag.macs.out
macs2_callpeak.POL2-ser2_shCRTL-1_EtOHN.aaffae6e9213ce09f3d3b7c65f6ecb4b.mugqic.done
)
macs2_callpeak_55_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_55_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_56_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shCRTL-1_EtOHN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shCRTL-1_EtOHN
JOB_DEPENDENCIES=$macs2_callpeak_55_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shCRTL-1_EtOHN.c6cf82007fcc6a128a621fc91704929f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shCRTL-1_EtOHN.c6cf82007fcc6a128a621fc91704929f.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shCRTL-1_EtOHN/POL2-ser2_shCRTL-1_EtOHN_peaks.narrowPeak > peak_call/POL2-ser2_shCRTL-1_EtOHN/POL2-ser2_shCRTL-1_EtOHN_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shCRTL-1_EtOHN/POL2-ser2_shCRTL-1_EtOHN_peaks.narrowPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shCRTL-1_EtOHN/POL2-ser2_shCRTL-1_EtOHN_peaks.narrowPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shCRTL-1_EtOHN.c6cf82007fcc6a128a621fc91704929f.mugqic.done
)
macs2_callpeak_56_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_56_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_57_JOB_ID: macs2_callpeak.POL2-ser2_shCTRL-2_EtOHN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shCTRL-2_EtOHN
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shCTRL-2_EtOHN.ec1bb7bd3c72e5c8f086cc425faecfad.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shCTRL-2_EtOHN.ec1bb7bd3c72e5c8f086cc425faecfad.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shCTRL-2_EtOHN && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shCTRL-2_EtOH_Rep1/POL2-ser2_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCTRL-2_EtOH_Rep1/WCE_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shCTRL-2_EtOHN/POL2-ser2_shCTRL-2_EtOHN \
  >& peak_call/POL2-ser2_shCTRL-2_EtOHN/POL2-ser2_shCTRL-2_EtOHN.diag.macs.out
macs2_callpeak.POL2-ser2_shCTRL-2_EtOHN.ec1bb7bd3c72e5c8f086cc425faecfad.mugqic.done
)
macs2_callpeak_57_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_57_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_58_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shCTRL-2_EtOHN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shCTRL-2_EtOHN
JOB_DEPENDENCIES=$macs2_callpeak_57_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shCTRL-2_EtOHN.8c2e1f3d69f1a1d540f0be0416758147.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shCTRL-2_EtOHN.8c2e1f3d69f1a1d540f0be0416758147.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shCTRL-2_EtOHN/POL2-ser2_shCTRL-2_EtOHN_peaks.narrowPeak > peak_call/POL2-ser2_shCTRL-2_EtOHN/POL2-ser2_shCTRL-2_EtOHN_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shCTRL-2_EtOHN/POL2-ser2_shCTRL-2_EtOHN_peaks.narrowPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shCTRL-2_EtOHN/POL2-ser2_shCTRL-2_EtOHN_peaks.narrowPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shCTRL-2_EtOHN.8c2e1f3d69f1a1d540f0be0416758147.mugqic.done
)
macs2_callpeak_58_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_58_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_59_JOB_ID: macs2_callpeak.POL2-ser2_shCTRL_EtOHN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shCTRL_EtOHN
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shCTRL_EtOHN.b5afc8d0980ccaeffed57354a2b29f89.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shCTRL_EtOHN.b5afc8d0980ccaeffed57354a2b29f89.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shCTRL_EtOHN && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shCRTL-1_EtOH_Rep1/POL2-ser2_shCRTL-1_EtOH_Rep1.sorted.dup.bam \
  alignment/POL2-ser2_shCTRL-2_EtOH_Rep1/POL2-ser2_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shCTRL-1_EtOH_Rep1/WCE_shCTRL-1_EtOH_Rep1.sorted.dup.bam \
  alignment/WCE_shCTRL-2_EtOH_Rep1/WCE_shCTRL-2_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shCTRL_EtOHN/POL2-ser2_shCTRL_EtOHN \
  >& peak_call/POL2-ser2_shCTRL_EtOHN/POL2-ser2_shCTRL_EtOHN.diag.macs.out
macs2_callpeak.POL2-ser2_shCTRL_EtOHN.b5afc8d0980ccaeffed57354a2b29f89.mugqic.done
)
macs2_callpeak_59_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_59_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_60_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shCTRL_EtOHN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shCTRL_EtOHN
JOB_DEPENDENCIES=$macs2_callpeak_59_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shCTRL_EtOHN.fa5204a9f51f1a69679cd6808f7e071b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shCTRL_EtOHN.fa5204a9f51f1a69679cd6808f7e071b.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shCTRL_EtOHN/POL2-ser2_shCTRL_EtOHN_peaks.narrowPeak > peak_call/POL2-ser2_shCTRL_EtOHN/POL2-ser2_shCTRL_EtOHN_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shCTRL_EtOHN/POL2-ser2_shCTRL_EtOHN_peaks.narrowPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shCTRL_EtOHN/POL2-ser2_shCTRL_EtOHN_peaks.narrowPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shCTRL_EtOHN.fa5204a9f51f1a69679cd6808f7e071b.mugqic.done
)
macs2_callpeak_60_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_60_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_61_JOB_ID: macs2_callpeak.POL2-ser2_shNIPBL-3_DexN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shNIPBL-3_DexN
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shNIPBL-3_DexN.7318ebfa61ff70db03a8209b3e25ded6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shNIPBL-3_DexN.7318ebfa61ff70db03a8209b3e25ded6.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shNIPBL-3_DexN && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shNIPBL-3_Dex_Rep1/POL2-ser2_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_Dex_Rep1/WCE_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shNIPBL-3_DexN/POL2-ser2_shNIPBL-3_DexN \
  >& peak_call/POL2-ser2_shNIPBL-3_DexN/POL2-ser2_shNIPBL-3_DexN.diag.macs.out
macs2_callpeak.POL2-ser2_shNIPBL-3_DexN.7318ebfa61ff70db03a8209b3e25ded6.mugqic.done
)
macs2_callpeak_61_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_61_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_62_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shNIPBL-3_DexN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shNIPBL-3_DexN
JOB_DEPENDENCIES=$macs2_callpeak_61_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shNIPBL-3_DexN.75c7c8bfd7329a3193d428265581a5c8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shNIPBL-3_DexN.75c7c8bfd7329a3193d428265581a5c8.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shNIPBL-3_DexN/POL2-ser2_shNIPBL-3_DexN_peaks.narrowPeak > peak_call/POL2-ser2_shNIPBL-3_DexN/POL2-ser2_shNIPBL-3_DexN_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shNIPBL-3_DexN/POL2-ser2_shNIPBL-3_DexN_peaks.narrowPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shNIPBL-3_DexN/POL2-ser2_shNIPBL-3_DexN_peaks.narrowPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shNIPBL-3_DexN.75c7c8bfd7329a3193d428265581a5c8.mugqic.done
)
macs2_callpeak_62_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_62_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_63_JOB_ID: macs2_callpeak.POL2-ser2_shNIPBL-5_DexN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shNIPBL-5_DexN
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shNIPBL-5_DexN.c0bbf5c884920857e2b7614c12faee07.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shNIPBL-5_DexN.c0bbf5c884920857e2b7614c12faee07.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shNIPBL-5_DexN && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shNIPBL-5_Dex_Rep1/POL2-ser2_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-5_Dex_Rep1/WCE_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shNIPBL-5_DexN/POL2-ser2_shNIPBL-5_DexN \
  >& peak_call/POL2-ser2_shNIPBL-5_DexN/POL2-ser2_shNIPBL-5_DexN.diag.macs.out
macs2_callpeak.POL2-ser2_shNIPBL-5_DexN.c0bbf5c884920857e2b7614c12faee07.mugqic.done
)
macs2_callpeak_63_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_63_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_64_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shNIPBL-5_DexN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shNIPBL-5_DexN
JOB_DEPENDENCIES=$macs2_callpeak_63_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shNIPBL-5_DexN.2b5e274a7b9fbd0824b99f770ecf7f14.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shNIPBL-5_DexN.2b5e274a7b9fbd0824b99f770ecf7f14.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shNIPBL-5_DexN/POL2-ser2_shNIPBL-5_DexN_peaks.narrowPeak > peak_call/POL2-ser2_shNIPBL-5_DexN/POL2-ser2_shNIPBL-5_DexN_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shNIPBL-5_DexN/POL2-ser2_shNIPBL-5_DexN_peaks.narrowPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shNIPBL-5_DexN/POL2-ser2_shNIPBL-5_DexN_peaks.narrowPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shNIPBL-5_DexN.2b5e274a7b9fbd0824b99f770ecf7f14.mugqic.done
)
macs2_callpeak_64_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_64_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_65_JOB_ID: macs2_callpeak.POL2-ser2_shNIPBL_DexN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shNIPBL_DexN
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shNIPBL_DexN.ddd64261157c09159d467db218cf07a2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shNIPBL_DexN.ddd64261157c09159d467db218cf07a2.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shNIPBL_DexN && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shNIPBL-3_Dex_Rep1/POL2-ser2_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  alignment/POL2-ser2_shNIPBL-5_Dex_Rep1/POL2-ser2_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_Dex_Rep1/WCE_shNIPBL-3_Dex_Rep1.sorted.dup.bam \
  alignment/WCE_shNIPBL-5_Dex_Rep1/WCE_shNIPBL-5_Dex_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shNIPBL_DexN/POL2-ser2_shNIPBL_DexN \
  >& peak_call/POL2-ser2_shNIPBL_DexN/POL2-ser2_shNIPBL_DexN.diag.macs.out
macs2_callpeak.POL2-ser2_shNIPBL_DexN.ddd64261157c09159d467db218cf07a2.mugqic.done
)
macs2_callpeak_65_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_65_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_66_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shNIPBL_DexN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shNIPBL_DexN
JOB_DEPENDENCIES=$macs2_callpeak_65_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shNIPBL_DexN.4438db742c05439e222da2a4092d3dd8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shNIPBL_DexN.4438db742c05439e222da2a4092d3dd8.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shNIPBL_DexN/POL2-ser2_shNIPBL_DexN_peaks.narrowPeak > peak_call/POL2-ser2_shNIPBL_DexN/POL2-ser2_shNIPBL_DexN_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shNIPBL_DexN/POL2-ser2_shNIPBL_DexN_peaks.narrowPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shNIPBL_DexN/POL2-ser2_shNIPBL_DexN_peaks.narrowPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shNIPBL_DexN.4438db742c05439e222da2a4092d3dd8.mugqic.done
)
macs2_callpeak_66_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_66_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_67_JOB_ID: macs2_callpeak.POL2-ser2_shNIPBL-3_EtOHN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shNIPBL-3_EtOHN
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shNIPBL-3_EtOHN.b549ea265e190a60f9553c3b0d630c59.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shNIPBL-3_EtOHN.b549ea265e190a60f9553c3b0d630c59.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shNIPBL-3_EtOHN && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shNIPBL-3_EtOH_Rep1/POL2-ser2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shNIPBL-3_EtOHN/POL2-ser2_shNIPBL-3_EtOHN \
  >& peak_call/POL2-ser2_shNIPBL-3_EtOHN/POL2-ser2_shNIPBL-3_EtOHN.diag.macs.out
macs2_callpeak.POL2-ser2_shNIPBL-3_EtOHN.b549ea265e190a60f9553c3b0d630c59.mugqic.done
)
macs2_callpeak_67_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_67_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_68_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shNIPBL-3_EtOHN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shNIPBL-3_EtOHN
JOB_DEPENDENCIES=$macs2_callpeak_67_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shNIPBL-3_EtOHN.048e9045dcefc627429b9a5abb989781.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shNIPBL-3_EtOHN.048e9045dcefc627429b9a5abb989781.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shNIPBL-3_EtOHN/POL2-ser2_shNIPBL-3_EtOHN_peaks.narrowPeak > peak_call/POL2-ser2_shNIPBL-3_EtOHN/POL2-ser2_shNIPBL-3_EtOHN_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shNIPBL-3_EtOHN/POL2-ser2_shNIPBL-3_EtOHN_peaks.narrowPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shNIPBL-3_EtOHN/POL2-ser2_shNIPBL-3_EtOHN_peaks.narrowPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shNIPBL-3_EtOHN.048e9045dcefc627429b9a5abb989781.mugqic.done
)
macs2_callpeak_68_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_68_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_69_JOB_ID: macs2_callpeak.POL2-ser2_shNIPBL-5_EtOHN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shNIPBL-5_EtOHN
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shNIPBL-5_EtOHN.858f235ac7feac9ce60b16826b73c6ea.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shNIPBL-5_EtOHN.858f235ac7feac9ce60b16826b73c6ea.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shNIPBL-5_EtOHN && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shNIPBL-5_EtOH_Rep1/POL2-ser2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shNIPBL-5_EtOHN/POL2-ser2_shNIPBL-5_EtOHN \
  >& peak_call/POL2-ser2_shNIPBL-5_EtOHN/POL2-ser2_shNIPBL-5_EtOHN.diag.macs.out
macs2_callpeak.POL2-ser2_shNIPBL-5_EtOHN.858f235ac7feac9ce60b16826b73c6ea.mugqic.done
)
macs2_callpeak_69_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_69_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_70_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shNIPBL-5_EtOHN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shNIPBL-5_EtOHN
JOB_DEPENDENCIES=$macs2_callpeak_69_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shNIPBL-5_EtOHN.7e8869539b9ce1054890afd4b685c04d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shNIPBL-5_EtOHN.7e8869539b9ce1054890afd4b685c04d.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shNIPBL-5_EtOHN/POL2-ser2_shNIPBL-5_EtOHN_peaks.narrowPeak > peak_call/POL2-ser2_shNIPBL-5_EtOHN/POL2-ser2_shNIPBL-5_EtOHN_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shNIPBL-5_EtOHN/POL2-ser2_shNIPBL-5_EtOHN_peaks.narrowPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shNIPBL-5_EtOHN/POL2-ser2_shNIPBL-5_EtOHN_peaks.narrowPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shNIPBL-5_EtOHN.7e8869539b9ce1054890afd4b685c04d.mugqic.done
)
macs2_callpeak_70_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_70_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_71_JOB_ID: macs2_callpeak.POL2-ser2_shNIPBL_EtOHN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.POL2-ser2_shNIPBL_EtOHN
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.POL2-ser2_shNIPBL_EtOHN.346e7239ae0c707fd0916d0b0f01fa27.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.POL2-ser2_shNIPBL_EtOHN.346e7239ae0c707fd0916d0b0f01fa27.mugqic.done'
module load mugqic/python/2.7.8 mugqic/MACS2/2.1.0.20140616 && \
mkdir -p peak_call/POL2-ser2_shNIPBL_EtOHN && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/POL2-ser2_shNIPBL-3_EtOH_Rep1/POL2-ser2_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  alignment/POL2-ser2_shNIPBL-5_EtOH_Rep1/POL2-ser2_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --control \
  alignment/WCE_shNIPBL-3_EtOH_Rep1/WCE_shNIPBL-3_EtOH_Rep1.sorted.dup.bam \
  alignment/WCE_shNIPBL-5_EtOH_Rep1/WCE_shNIPBL-5_EtOH_Rep1.sorted.dup.bam \
  --name peak_call/POL2-ser2_shNIPBL_EtOHN/POL2-ser2_shNIPBL_EtOHN \
  >& peak_call/POL2-ser2_shNIPBL_EtOHN/POL2-ser2_shNIPBL_EtOHN.diag.macs.out
macs2_callpeak.POL2-ser2_shNIPBL_EtOHN.346e7239ae0c707fd0916d0b0f01fa27.mugqic.done
)
macs2_callpeak_71_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_71_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_72_JOB_ID: macs2_callpeak_bigBed.POL2-ser2_shNIPBL_EtOHN
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.POL2-ser2_shNIPBL_EtOHN
JOB_DEPENDENCIES=$macs2_callpeak_71_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.POL2-ser2_shNIPBL_EtOHN.4ccf5e3402c805a6c0fc904b1049d3b1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.POL2-ser2_shNIPBL_EtOHN.4ccf5e3402c805a6c0fc904b1049d3b1.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/POL2-ser2_shNIPBL_EtOHN/POL2-ser2_shNIPBL_EtOHN_peaks.narrowPeak > peak_call/POL2-ser2_shNIPBL_EtOHN/POL2-ser2_shNIPBL_EtOHN_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/POL2-ser2_shNIPBL_EtOHN/POL2-ser2_shNIPBL_EtOHN_peaks.narrowPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/POL2-ser2_shNIPBL_EtOHN/POL2-ser2_shNIPBL_EtOHN_peaks.narrowPeak.bb
macs2_callpeak_bigBed.POL2-ser2_shNIPBL_EtOHN.4ccf5e3402c805a6c0fc904b1049d3b1.mugqic.done
)
macs2_callpeak_72_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_72_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_73_JOB_ID: macs2_callpeak_report
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_report
JOB_DEPENDENCIES=$macs2_callpeak_1_JOB_ID:$macs2_callpeak_3_JOB_ID:$macs2_callpeak_5_JOB_ID:$macs2_callpeak_7_JOB_ID:$macs2_callpeak_9_JOB_ID:$macs2_callpeak_11_JOB_ID:$macs2_callpeak_13_JOB_ID:$macs2_callpeak_15_JOB_ID:$macs2_callpeak_17_JOB_ID:$macs2_callpeak_19_JOB_ID:$macs2_callpeak_21_JOB_ID:$macs2_callpeak_23_JOB_ID:$macs2_callpeak_25_JOB_ID:$macs2_callpeak_27_JOB_ID:$macs2_callpeak_29_JOB_ID:$macs2_callpeak_31_JOB_ID:$macs2_callpeak_33_JOB_ID:$macs2_callpeak_35_JOB_ID:$macs2_callpeak_37_JOB_ID:$macs2_callpeak_39_JOB_ID:$macs2_callpeak_41_JOB_ID:$macs2_callpeak_43_JOB_ID:$macs2_callpeak_45_JOB_ID:$macs2_callpeak_47_JOB_ID:$macs2_callpeak_49_JOB_ID:$macs2_callpeak_51_JOB_ID:$macs2_callpeak_53_JOB_ID:$macs2_callpeak_55_JOB_ID:$macs2_callpeak_57_JOB_ID:$macs2_callpeak_59_JOB_ID:$macs2_callpeak_61_JOB_ID:$macs2_callpeak_63_JOB_ID:$macs2_callpeak_65_JOB_ID:$macs2_callpeak_67_JOB_ID:$macs2_callpeak_69_JOB_ID:$macs2_callpeak_71_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_report.8c78328698af406234c4b836457fc6c4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_report.8c78328698af406234c4b836457fc6c4.mugqic.done'
mkdir -p report && \
cp /home/efournie/genpipes/bfx/report/ChipSeq.macs2_callpeak.md report/ && \
for contrast in POL2_shCRTL-1_Dex POL2_shCTRL-2_Dex POL2_shCRTL_Dex POL2_shCRTL-1_EtOH POL2_shCTRL-2_EtOH POL2_shCTRL_EtOH POL2_shNIPBL-3_Dex POL2_shNIPBL-5_Dex POL2_shNIPBL_Dex POL2_shNIPBL-3_EtOH POL2_shNIPBL-5_EtOH POL2_shNIPBL_EtOH POL2-ser2_shCRTL-1_Dex POL2-ser2_shCTRL-2_Dex POL2-ser2_shCRTL_Dex POL2-ser2_shCRTL-1_EtOH POL2-ser2_shCTRL-2_EtOH POL2-ser2_shCTRL_EtOH POL2-ser2_shNIPBL-3_Dex POL2-ser2_shNIPBL-5_Dex POL2-ser2_shNIPBL_Dex POL2-ser2_shNIPBL-3_EtOH POL2-ser2_shNIPBL-5_EtOH POL2-ser2_shNIPBL_EtOH POL2-ser2_shCRTL-1_DexN POL2-ser2_shCTRL-2_DexN POL2-ser2_shCRTL_DexN POL2-ser2_shCRTL-1_EtOHN POL2-ser2_shCTRL-2_EtOHN POL2-ser2_shCTRL_EtOHN POL2-ser2_shNIPBL-3_DexN POL2-ser2_shNIPBL-5_DexN POL2-ser2_shNIPBL_DexN POL2-ser2_shNIPBL-3_EtOHN POL2-ser2_shNIPBL-5_EtOHN POL2-ser2_shNIPBL_EtOHN
do
  cp -a --parents peak_call/$contrast/ report/ && \
  echo -e "* [Peak Calls File for Design $contrast](peak_call/$contrast/${contrast}_peaks.xls)" \
  >> report/ChipSeq.macs2_callpeak.md
done
macs2_callpeak_report.8c78328698af406234c4b836457fc6c4.mugqic.done
)
macs2_callpeak_73_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_73_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# Call home with pipeline statistics
#-------------------------------------------------------------------------------
wget "http://mugqic.hpc.mcgill.ca/cgi-bin/pipeline.cgi?hostname=cedar5.cedar.computecanada.ca&ip=206.12.124.6&pipeline=ChipSeq&steps=macs2_callpeak&samples=24&AnonymizedList=2d4543656832a0f7ce64645ba607de8b,67e4977aa74515d9fa8d4f929b0480af,1fe9f6d1d22aef19d7d5601840729b74,7f7a32015cb6b46ab4076205db1c03fb,f0b68df75f6af8b9cbd1b1aa3a28a338,6fa3428227c0adb8be7ed85e8b8eb020,b5662f698f424a4bea73103163b9c09e,6157f5ab88ec30bfd195f253f3e1e44b,b54b42c2a9d7138c26fee793d5f9ebc1,7d288f0ad84f4abc2a60a2174447686c,f1040c317da2b4e18d7524355a8d4b3a,b23db1ac7431c6334784b48c180cfc4f,f420a3e0707626f7c7ba76adc7c1dab6,8b69211c545725ef9d396aad51602c03,0c986e135aa9bbc1aa9528f40406a6e9,5812b89d9599b8a376e2e52db39ac4e3,97b188a1f2462f9cab8d816f53c80baa,53fd961e68fe1a8e1ffa26cae47be580,f5ae1c0e518dba94b26685364315e11a,fb13b7d9eb381c66b005619d5001ca91,1b93ab0c6d05bc954dc30fd339e27262,6baf8b9cb388e734b92f46b1a2e6a26a,1913f07324c7355f4bd983788a9a1743,b07c86fdd58c53a001b692385b93807d" --quiet --output-document=/dev/null

